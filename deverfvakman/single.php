<?php
get_header();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>

	<article class="pt-5">
		<div class="container ">

			<div class="row justify-content-center">
				<div class="col-lg-10 col-xl-8">

					<h1 class="text-uppercase mb-0 text-center">
						<?php echo get_the_title(); ?>
					</h1>

					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs" class="mb-0 text-center">','</p>' );
					}; ?>

					<div class="content mt-3 pt-4">
					
						<div class="sticky post-meta bg-white mb-0 d-flex justify-content-center">

							<div class="shares d-flex align-items-center justify-content-end">
								<div class="social-share-icons-list d-flex align-items-center mb-0">
									<?php 
									$linkedin_count = get_click_count($post->ID, 'linkedin');
									$twitter_count = get_click_count($post->ID, 'twitter');
									$facebook_count = get_click_count($post->ID, 'facebook');
									$whatsapp_count = get_click_count($post->ID, 'whatsapp');
									$count = $facebook_count + $twitter_count + $linkedin_count + $whatsapp_count + $link_count; ?>

									<div class="social-icons-list list-inline list-unstyled mb-0">
										<span class="position-relative mr-1 d-inline"><?php if($linkedin_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $linkedin_count; ?></span><?php }; ?>
											<a class="share list-inline-item social linkedin" target="_blank" href="<?php echo esc_url('https://www.linkedin.com/shareArticle?mini=true&amp;url='.get_permalink().'&amp;title='.urlencode(get_the_title())); ?>" data-click-count="linkedin">
												<i class="dvv dvv-linkedin"></i>
											</a>
										</span>
										<span class="position-relative mr-1 d-inline"><?php if($twitter_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $twitter_count; ?></span><?php }; ?>
											<a class="share list-inline-item social twitter" target="_blank" href="<?php echo esc_url('https://twitter.com/home?status='.(urlencode(__('Check this out:', 'maatwerkonline'))).' '.wp_get_shortlink()); ?>" data-click-count="twitter">
												<i class="dvv dvv-twitter"></i>
											</a>
										</span>
										<span class="position-relative mr-1 d-inline"><?php if($facebook_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $facebook_count; ?></span><?php }; ?>
											<a class="share list-inline-item social facebook" target="_blank" href="<?php echo esc_url('https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink()); ?>" data-click-count="facebook">
												<i class="dvv dvv-facebook"></i>
											</a>
										</span>
										<span class="position-relative mr-1 d-inline"><?php if($whatsapp_count){ ; ?><span class="badge badge-pill badge-danger share-counter"><?php echo $whatsapp_count; ?></span><?php }; ?>
											<a class="share list-inline-item social whatsapp" target="_blank" href="<?php echo esc_url('https://api.whatsapp.com/send?phone=whatsappphonenumber&text='.__('Check this out:', 'maatwerkonline').' '.wp_get_shortlink()); ?>" data-action="share/whatsapp/share" data-click-count="whatsapp">
												<i class="dvv dvv-whatsapp"></i>
											</a>
										</span>
									</div>

								</div>
							</div>

						</div>

					
						<div class="row">
							<?php the_content();?>
						</div>
					</div>
					
				</div>
			</div>

			<?php if(comments_open()): ?>
				<div id="comments" class="row justify-content-md-center">
					<div class="col-lg-10 col-xl-8">
						<?php comments_template('/template-parts/comments.php'); ?>
					</div>
				</div>
			<?php endif; ?>

		</div>
	</article>

<?php endwhile; endif;?>

<?php
	get_footer();
?>
