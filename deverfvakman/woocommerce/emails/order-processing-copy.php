<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Request Emails Controller
 *
 * WooCommerce-RFQ Emails Class which handles the sending on request emails and email templates. This class loads in available emails.
 *
 * @class       WC_Request_Emails
 * @version     2.3.0
 * @category    Class
 * @author      OnlineMarketing.nl
 */
class Processing_Order_Email_Admin extends WC_Email {

    public $form_data;
    /**
     * Set email defaults
     *
     * @since 0.1
     */
    public function __construct() {

        // set ID, this simply needs to be a unique name
        $this->id = 'processing_order_email_copy';

        // this is the title in WooCommerce Email settings
        $this->title = __('Processing order Admin', 'firecom');

        // this is the description in WooCommerce email settings
        $this->description =  __('Email which will sent to the admin when someone make a request.', 'woocommerce-rfq');

        // these are the default heading and subject lines that can be overridden using the settings
        $this->heading = __('Processing Order Copy', 'firecom');
        $this->subject = __('Processing Order Copy', 'firecom');

        // these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
        $this->template_html  = 'admin-processing-order.php';
        $this->template_plain = 'admin-processing-order.php';

        // Trigger on new paid orders
        add_action( 'send_order_processing_email', array( $this, 'trigger' ), 10, 2  );

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // this sets the recipient to the settings defined below in init_form_fields()
        $this->recipient = $this->get_option( 'recipient' );

        // if none was entered, just use the WP admin email as a fallback
        if ( ! $this->recipient )
            $this->recipient = get_option( 'admin_email' );
    }


    /**
     * Determine if the email should actually be sent and setup email merge variables
     *
     * @since 0.1
     * @param int $order_id
     */
    public function trigger($order_id, $order = false ) {

      /*  $this->form_data = $form_data;
        if ( ! $this->is_enabled() || ! $this->get_recipient() )
            return;
        $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );*/

       /* if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
            $order = new WC_Order( $order_id );
        }*/

        if ( is_a( $order, 'WC_Order' ) ) {
            $this->object       = $order;
            $this->order       = $order;
            $this->find['order-date']      = '{order_date}';
            $this->find['order-number']    = '{order_number}';

            $this->replace['order-date']   = wc_format_datetime( $this->object->get_date_created() );
            $this->replace['order-number'] = $this->object->get_order_number();
        }



        if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
            return;
        }

        $this->setup_locale();
        $this->send($this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        $this->restore_locale();
    }


    /**
     * get_content_html function.
     *
     * @since 0.1
     * @return string
     */
    public function get_content_html() {
       $template_path = get_stylesheet_directory() . '/woocommerce/emails/';
        return wc_get_template_html( $this->template_html, array(
            'email_heading' => $this->get_heading(),
            'sent_to_admin' => false,
            'plain_text'    => false,
            'email'         => $this,
            'order'         => $this->order
        ),
        $template_path,
        $template_path);
    }


    /**
     * get_content_plain function.
     *
     * @since 0.1
     * @return string
     */
    public function get_content_plain() {
        return wc_get_template_html( $this->template_plain, array(
            'email_heading' => $this->get_heading(),
            'sent_to_admin' => false,
            'plain_text'    => false,
            'email'         => $this
        ),
        '',
        woocommerce_rfq_plugin_path() . "/templates/" );
    }


    /**
     * Initialize Settings Form Fields
     *
     * @since 2.0
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => __('Enable/Disable', 'firecom' ),
                'type'    => 'checkbox',
                'label'   => __('Enable this email notification', 'firecom' ),
                'default' => 'yes'
            ),
            'recipient'  => array(
                'title'       => __('Recipient(s)', 'firecom' ),
                'type'        => 'text',
                'description' => sprintf( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', esc_attr( get_option( 'admin_email' ) ) ),
                'placeholder' => '',
                'default'     => ''
            ),
            'subject'    => array(
                'title'       => __('Subject', 'firecom' ),
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
                'placeholder' => '',
                'default'     => ''
            ),
            'heading'    => array(
                'title'       => __('Email Heading', 'firecom' ),
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
                'placeholder' => '',
                'default'     => ''
            ),
            'email_type' => array(
                'title'       => __('Email type', 'firecom' ),
                'type'        => 'select',
                'description' => __('Choose which format of email to send', 'woocommerce-rfq' ),
                'default'     => 'html',
                'class'       => 'email_type',
                'options'     => array(
                    'plain'     => __( 'Plain text', 'firecom' ),
                    'html'      => __( 'HTML', 'firecom' ),
                    'multipart' => __( 'Multipart', 'firecom' ),
                )
            )
        );
    }


} // end \WC_Request_Email class
/*
,'emailrawdata'    => array(
                'title'       => __('Email Data', 'woocommerce-rfq' ),
                'type'        => 'textarea',
                'description' => sprintf( __( 'Its area where we have add the string that will be used for the email: <code>%s</code>.' ), $this->heading ),
                'placeholder' => '',
                'default'     => ''
            )*/