<?php
/**
 * Customer completed order email
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
		
<div class="top_heading">
	<?php echo get_option( 'ec_vanilla_customer_completed_order_heading' ); ?>
</div>

<?php //echo get_option( 'ec_vanilla_customer_completed_order_main_text' ); ?>

<?php
	foreach( $order->get_items( 'shipping' ) as $item_id => $item ){
		$shipping_method = $item->get_method_title();
		$pickup_data = wc_local_pickup_plus()->get_orders_instance()->get_order_items_instance()->get_order_item_pickup_location_address( $item_id, 'array' );
	}
?>

<?php if($shipping_method == "Afhalen"):

	$text_send = 	"<p>";
	$text_send .= 	__('$order-completed-local-pickup-sentence-1', 'maatwerkonline');
	$text_send .=	"</p><p>";
	$text_send .=	__('$order-completed-local-pickup-sentence-2','maatwerkonline');
	$text_send .=	"</p>";

else: 

	$text_send = 	"<p>";
	$text_send .= 	__('$order-completed-sentence-1', 'maatwerkonline');
	$text_send .=	"</p><p>";
	$text_send .=	__('$order-completed-sentence-2','maatwerkonline');
	$text_send .=	"</p>";

?>

<?php endif; 
	$return = str_replace("[ec_order]", "<a href='https://www.deverfvakman.nl/mijn-account/bestelling-bekijken/".$order->id."'>#".$order->id."</a>", $text_send);
	$return = str_replace("[empty]", "", $return);
	echo $return;
?>


<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additonal content - this is set in each email's settings.
 */
if ( isset( $additional_content ) && $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );

