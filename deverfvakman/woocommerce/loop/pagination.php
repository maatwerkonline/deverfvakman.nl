<?php
/**
 * Pagination - Show numbered pagination for catalog pages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/pagination.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wp_query;

if ( $wp_query->max_num_pages <= 1 ) {
	return;
}
?>

<?php $args = array(
		'previous_string' => __( 'Previous', 'onlinemarketingnl' ),
		'next_string'     => __( 'Next', 'onlinemarketingnl' ),
		'before_output'   => '<section id="pagination" class="pb-0"><div class="container"><div class="row"><div class="col-12"><nav class="woocommerce-pagination" aria-label="Navigation"><ul class="pagination justify-content-end">',
		'after_output'    => '</ul></nav></div></div></div></section>'
);
wp_bootstrap_pagination( $args ); ?>
