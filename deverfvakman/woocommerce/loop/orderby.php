<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<form class="woocommerce-ordering hidden-sm-down" method="get">
	<select name="orderby" class="orderby custom-select m-1">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit' ) ); ?>
</form>

<!--<div id="show-ordering" class="hidden-md-up btn btn-link float-right"><i class="dvv dvv-sort-numeric-asc mr-2 mb-0"></i> <?php _e('Sort by', 'maatwerkonline'); ?></div>-->

<?php $term_id = get_queried_object()->term_id;
$category_filter = get_term_meta($term_id, 'category_filter', true); ?>

<?php if( $category_filter != 'hide' ) : ?>
	<div id="show-filter" class="d-md-none btn btn-link float-right"><i class="dvv dvv-funnel mr-2 mb-0"></i> <?php _e('Filter', 'maatwerkonline'); ?></div>
<?php endif; ?>
