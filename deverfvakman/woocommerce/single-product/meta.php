<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

$contact_page = get_theme_mod( 'contact_page' );
?>

<div class="product_meta mt-4">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php
	$late_delivery = get_post_meta( get_the_ID(), '_late_delivery', true );
	$customer_service_points = get_theme_mod( 'customer_service_points' );
	$customer_service_page = get_theme_mod( 'customer_service_page' );
	?>
	<?php if ( $customer_service_points != '' || $late_delivery != '') : ?>
		<?php foreach ( $customer_service_points as $customer_service_point ) : ?>
			<li class="list-icon"><i class="dvv dvv-checkmark-circle" style="color:#65bc25;"></i> <a href="<?php echo get_page_link($customer_service_page); ?>" class="text-normal"><?php echo $customer_service_point['point']; ?></a></li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
