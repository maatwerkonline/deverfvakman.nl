<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<li <?php comment_class('media d-flex'); ?> id="comment-<?php comment_ID() ?>">

	<?php
	$avatar_size = 60;
	if ( '0' != $comment->comment_parent ){
		$avatar_size = 60;
	}
	?>
	<div class="comment-image mr-3">
		<?php echo get_avatar( $comment, $avatar_size ); ?>
	</div>

	<div class="media-body">
		<div class="comment-author">
			<?php printf( __( '%1$s  %2$s %3$s', 'onlinemarketingnl' ),
				sprintf( '<span>%s</span>', get_comment_author_link() ),
				sprintf( $badgeauthor ),
				sprintf( _x( '<time pubdate class="small text-muted ml-2">%s ago</time>', '%s = human-readable time difference', 'onlinemarketingnl' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) )
			); ?>
		</div>
		<div class="comment-text">
			<?php
				/**
				 * The woocommerce_review_before_comment_meta hook.
				 *
				 * @hooked woocommerce_review_display_rating - 10
				 */
				do_action( 'woocommerce_review_before_comment_meta', $comment );
			?>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<p class="comment-awaiting-moderation small font-italic text-muted"><?php _e( 'Your comment is awaiting moderation.', 'onlinemarketingnl' ); ?></p>
			<?php else: ?>
				<?php /**
				 * The woocommerce_review_comment_text hook
				 *
				 * @hooked woocommerce_review_display_comment_text - 10
				 */
				 do_action( 'woocommerce_review_before_comment_text', $comment );

				 do_action( 'woocommerce_review_comment_text', $comment );

				 do_action( 'woocommerce_review_after_comment_text', $comment ); ?>
			<?php endif; ?>
		</div>
		<div class="comment-reply">
			<?php edit_comment_link(  __( 'Edit', 'maatwerkonline' ), '<span class="edit-link small">', '</span>' ); ?>
		</div>
	</div>

</li>
