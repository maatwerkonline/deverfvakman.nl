<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;

$attachment_ids = $product->get_gallery_image_ids();

$product_video 		= get_field('product_video');
$video_locations 	= $product_video['video_locations'];
$video_enabled 		= $product_video['video_enabled'];

$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 5 );

if ( $attachment_ids && has_post_thumbnail() || $video_enabled[0] == 'enabled' && has_post_thumbnail() ) { ?>

	<div class="thumbnails">

		<?php /*
		<button type="button" class="slick-prev slick-prev-slider-nav dvv dvv-arrow-left slick-arrow" aria-disabled="false"></button>
		*/ ?>

		<?php /*
		<div class="carousel slider-nav" data-asnavfor=".slider-for" data-infinite="true" data-centermode="true" data-focusonselect="1" data-autoplay="false" data-autoplayspeed="7000" data-arrows="0" data-prevarrow="dvv dvv-arrow-left" data-nextarrow="dvv dvv-arrow-right" data-focusonselect="1" data-swipe="true" data-easing="linear" data-initialslide="1" data-speed="300" data-slidestoscroll="1" data-slidestoshow="<?php echo $columns; ?>" data-responsive="1" data-slidestoscroll_1190="1" data-slidestoshow_1190="3" data-slidestoscroll_768="1" data-slidestoshow_768="3" data-slidestoscroll_480="1" data-slidestoshow_480="2">
		*/ ?>

		<div class="carousel slider-nav"
			data-asnavfor=".slider-for"
			<?php /*
			data-prevarrow="dvv dvv-arrow-left",
			data-nextarrow="dvv dvv-arrow-right",
			*/ ?>
			data-infinite="true"
			data-arrows = "false",
			data-responsive="1"
			data-centermode="false",
			data-swipe="true",
			data-focusonselect="true",
			data-slidestoshow="5"
			data-slidestoscroll_1190="1"
			data-slidestoshow_1190="3"
			data-slidestoscroll_768="1"
			data-slidestoshow_768="3"
			data-slidestoscroll_480="1"
			data-slidestoshow_480="2"
		>

			<?php
			$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
			$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );

			if ( has_post_thumbnail() ) {
				$html  = '<div class="woocommerce-product-gallery__image">';
				$html .= get_the_post_thumbnail( $post->ID, 'shop_thumbnail' );
				$html .= '</div>';
			}

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

			foreach ( $attachment_ids as $attachment_id ) {

				$thumbnail = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );

				$html  = '<div class="woocommerce-product-gallery__image">';
				$html .= wp_get_attachment_image( $attachment_id, 'shop_thumbnail', false );
	 			$html .= '</div>';

				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
			} ?>

			<?php
				// Product video gallery item
				if($video_enabled[0] == 'enabled' && in_array('gallery', $video_locations)):
					$video_type 				= $product_video['video_type'];
					$video_thumbnail 			= $product_video['video_thumbnail'];
					$video_thumbnail_youtube	= $product_video['video_thumbnail_youtube'];
					$video_file 				= $product_video['video_file'];
					$video_youtube_id 			= $product_video['video_youtube_id'];

					$html  = '<div class="woocommerce-product-gallery__image">';
					
					if($video_thumbnail):
						$html .= '<img src="'. $video_thumbnail .'">';
					else:
						if($video_type == 'youtube' && in_array('use_youtube_thumbnail', $video_thumbnail_youtube)):
							$html .= '<img src="https://img.youtube.com/vi/'. $video_youtube_id .'/0.jpg">';
						else:
							$html .= '<img src="https://verfvakman-nl.dev.serv10.wpbouwlocatie.nl/wp-content/uploads/2021/10/vid_thumb_default-2.png">';
						endif;
					endif;
					
					$html .= '</div>';

					echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html);

				endif;
				
			?>

		</div>

		<?php /*
		<button type="button" class="slick-next slick-next-slider-nav dvv dvv-arrow-right slick-arrow" aria-disabled="false"></button>
		*/ ?>
	</div>

<?php }
