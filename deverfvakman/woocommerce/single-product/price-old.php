<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<?php $price_html = $product->get_price_html(); ?>
<p class="price"><?php echo preg_replace('/,00/', ',-', $price_html); ?></p>

<?php $bulkdiscount_enabled = get_post_meta( get_the_ID(), '_bulkdiscount_enabled', true ); ?>
<?php $bulkdiscount_minimum_quantity = get_post_meta( get_the_ID(), '_bulkdiscount_quantity_1', true ); ?>

<?php if($bulkdiscount_enabled == 'yes' && $bulkdiscount_minimum_quantity) : ?>
	<p class="text-success h5"><?php _e('This product will be <strong>cheaper</strong> if you buy more', 'maatwerkonline'); ?></p>
<?php endif; ?>
