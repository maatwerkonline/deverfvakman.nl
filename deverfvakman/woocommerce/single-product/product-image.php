<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;

$product_video 		= get_field('product_video');
$video_locations 	= $product_video['video_locations'];
$video_enabled 		= $product_video['video_enabled'];

$message 		   = get_post_meta($post->ID, 'custom_message', true);
$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$thumbnail_full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size );
$attachment_ids    = $product->get_gallery_image_ids();
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . $placeholder,
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
	'col-md-12',
	'col-lg-7',
) );
?>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>">

	<?php $sale = $product->get_attribute('pa_aanbieding'); ?>

	<?php if( ! empty( $sale ) ) : ?>

		<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

	<?php endif; ?>

	<figure class="woocommerce-product-gallery__wrapper">

		<?php if(!empty($message)): ?>

			<div class="product-message"><?php echo $message ?></div>

		<?php endif; ?>
			<?php /*
			<div class="carousel slider-for" data-asnavfor=".slider-nav" data-infinite="true" data-effect="fade" data-autoplay="false" data-autoplayspeed="7000" data-swipe="1" data-easing="linear" data-initialslide="1" data-speed="0" data-slidestoscroll="1" data-slidestoshow="1">
			*/ ?>

			<div class="carousel slider-for"
				data-asnavfor=".slider-nav"
				data-infinite="false"
				data-swipe="1"
				data-initialslide="1"
				data-slidestoscroll="1"
				data-slidestoshow="1"
			>

			<?php
			$thumbnail_attributes = array(
				'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
				'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
				'data-src'                => $thumbnail_full_size_image[0],
				'data-large_image'        => $thumbnail_full_size_image[0],
				'data-large_image_width'  => $thumbnail_full_size_image[1],
				'data-large_image_height' => $thumbnail_full_size_image[2],
			);

			if ( has_post_thumbnail() ) {
				$html  = '<div data-thumb="' . get_the_post_thumbnail_url( $post->ID, 'woocommerce_thumbnail' ) . '" class="woocommerce-product-gallery__image">';
				$html .= get_the_post_thumbnail( $post->ID, 'woocommerce_thumbnail', $thumbnail_attributes );
				$html .= '</div>';
			} else {
				$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
				$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
				$html .= '</div>';
			}

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

			if ( $attachment_ids && has_post_thumbnail() || $video_enabled[0] == 'enabled' && has_post_thumbnail()) {

				foreach ( $attachment_ids as $attachment_id ) {

					$attachment_full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
					$attachment_thumbnail       = wp_get_attachment_image_src( $attachment_id, 'woocommerce_thumbnail' );
					$attachment_attributes      = array(
						'title'                   => get_post_field( 'post_title', $attachment_id ),
						'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
						'data-src'                => $attachment_full_size_image[0],
						'data-large_image'        => $attachment_full_size_image[0],
						'data-large_image_width'  => $attachment_full_size_image[1],
						'data-large_image_height' => $attachment_full_size_image[2],
					);

					$html  = '<div data-thumb="' . esc_url( $attachment_thumbnail[0] ) . '" class="woocommerce-product-gallery__image">';
					$html .= wp_get_attachment_image( $attachment_id, 'woocommerce_thumbnail', false, $attachment_attributes );
		 			$html .= '</div>';

					echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
				}
				
				// Product video
				if($video_enabled[0] == 'enabled' && in_array('gallery', $video_locations)):
				
					$video_type 		= $product_video['video_type'];
					$video_thumbnail 	= $product_video['video_thumbnail'];
					$video_file 		= $product_video['video_file'];
					$video_youtube_id 	= $product_video['video_youtube_id'];

					switch($video_type):
						case "youtube":
							
							$html  = '<div data-thumb="https://img.youtube.com/vi/'. $video_youtube_id .'/0.jpg" class="woocommerce-product-gallery__image">';
							$html .= '<div class="product-video-featured-container">';
							$html .= '<iframe class="product-video-yt-featured" src="https://www.youtube.com/embed/'. $video_youtube_id .'" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
							$html .= '</div>';
							$html .= '</div>';

						break;
						case "file":

							// $html .= '<div class="product-video-featured-container">';
							// $html .= '<iframe class="product-video-file-featured" src="'.$video_file['url'].'" autoplay="false"></iframe>';
							// $html .= '</div>';
							// $html .= '</div>';

							$ext = pathinfo($video_file['url'], PATHINFO_EXTENSION);

							if($video_thumbnail):
								$html  = '<div data-thumb="'.$video_thumbnail.'" class="woocommerce-product-gallery__image">';
							else:
								$html  = '<div data-thumb="https://verfvakman-nl.dev.serv10.wpbouwlocatie.nl/wp-content/uploads/2021/10/vid_thumb_default-2.png" class="woocommerce-product-gallery__image">';
							endif;

							$html .= '<div class="product-video-featured-container">';
							$html .= '<video class="product-video-file" controls>';
							$html .= '<source src="' . $video_file['url'] . '" type="video/' . $ext . '">';
							$html .= '</video>';
							$html .= '</div>';

							$html .= '</div>';
							
						break;
					endswitch;

					echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html );

				endif;
			}?>

			</div>

		<?php do_action( 'woocommerce_product_thumbnails' ); ?>
	</figure>
</div>
