<?php
/**
 * Related Products tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $bundleproductIds;
$bundleproductIds		=	array();
$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Accessories', 'onlinemarketingnl' ) ) );
$product_id=get_the_ID();
global $wpdb;

$result = $wpdb->get_results ( "SELECT * FROM wp_woocommerce_bundled_items WHERE bundle_id = '$product_id'" );

foreach ( $result as $page )
{

	$bundleproductIds[]		=	$page->product_id;
}
// print_r($bundleproductIds);

?>

<?php if ( $heading ) : ?>
  <h2 class="h4 text-initial mt-5 strong"><?php echo $heading; ?></h2>
<?php endif; ?>

<?php $terms = get_the_terms( $post->ID, 'product_tag' ); ?>

<?php foreach ( $terms as $term ) : ?>
	<?php $term_array[] = $term->slug;?>
<?php endforeach; ?>
<?php /*$space_terms = wp_get_post_terms( $post->ID, 'space' );
if( $space_terms ) {
  $space_terms = array();
  foreach( $space_terms as $term ) {
   $space_terms[] = $term->slug;
  }
} */?>
<?php //$terms_out = explode(',', $term_array); echo $terms_out;?>

<?php

	$attributeConnectorIdsArray		=	array();
	$attributeAccessoireTagIdsArray	=	array();
	global $attributProductIds;
	global $attributProductIdsresult;
	global $catArr;
	$catArr	 = 	array();
	$attributProductIdsresult						=		array();
	if($term_array){
		foreach($term_array as $term){
				$attributeTag		=	get_term_by( 'slug', $term, 'pa_connector','OBJECT' );
				if($attributeTag){
						$attributeConnectorIdsArray[]		=	$attributeTag->slug;
				}

				$attributeTag		=	get_term_by( 'slug', $term, 'pa_accessoire-voor','OBJECT' );
				if($attributeTag){
						$attributeAccessoireTagIdsArray[]		=	$attributeTag->slug;
				}
		}
	}
	getProductByAttributeTaxnomy('pa_connector',$attributeConnectorIdsArray);
	getProductByAttributeTaxnomy('pa_accessoire-voor',$attributeAccessoireTagIdsArray);


	function getProductByAttributeTaxnomy($taxonomy_name,$taxonmy_slug_array){
		global $attributProductIds;
		global $attributProductIdsresult;
		global $catArr;
		$args = array (
				'post_type'  => 'product',
				'posts_per_page'  => -1,
				'tax_query' => array(
					array(
							'taxonomy' => $taxonomy_name,
							'field'    => 'slug',
							'terms'    => $taxonmy_slug_array,
							'operator' => 'IN',
					)
				),
			);
		$query = new WP_Query( $args );

		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) :
					$query->the_post();
				$attributProductIdsresult[]		=		get_the_ID();

				$categories 				=	 get_the_terms( get_the_ID(), 'product_cat' );

				if($categories){
					foreach($categories as $category){
						if(!in_array($category->term_id,$catArr)){
								$catArr[]		=	$category->term_id ;
						}
					}
				}

			endwhile;
		endif;
		wp_reset_postdata();

	}
	$result = array_merge($attributProductIdsresult,$bundleproductIds);
	 //print_r($result);
	 $attributProductIds= array_unique($result);
	 //print_r($result1);
	//$attributProductIds = array_diff($attributProductIdsresult, $bundleproductIds);
	//print_r($attributProductIdsresult);echo "r";
?>



<div class="row">
		<div class="col-md-3">
		<?php if(count($catArr) > 0):?>
			<ul class="list-group" role="tablist">
				<?php
						$counter		=	1;
						foreach($catArr as $category_id) :?>
							<a class="list-group-item <?php if($counter==1):?>active<?php endif; ?> " data-toggle="tab" href="#one<?php echo $counter; ?>" role="tab">
							<?php
								$getname = get_term( $category_id,'product_cat' );

								echo $getname->name;

								$counter++;
							?>
						</a>
				<?php endforeach;?>
			</ul>
		<?php endif; ?>
		</div>
	<div class="col-md-9">

		<div class="tab-content">
			<?php
				$counter		=	1;

				foreach($catArr as $category_id) : ?>

					<div class="tab-pane px-5 <?php if ($counter==1): ?>active<?php endif; ?>" id="one<?php echo $counter; ?>" role="tabpanel">
					<div class="products carousel mx-5" data-accessibility="0" data-adaptiveheight="0" data-autoplay="1" data-autoplayspeed="7000" data-arrows="1" data-prevarrow="dvv dvv-arrow-left" data-nextarrow="dvv dvv-arrow-right" data-centermode="0" data-dots="0" data-effect="scroll" data-swipe="1" data-easing="linear" data-infinite="0" data-initialslide="1" data-pauseonhover="0" data-speed="300" data-variablewidth="0" data-slidestoscroll="1" data-slidestoshow="3" data-responsive="1" data-slidestoscroll_1190="1" data-slidestoshow_1190="2" data-slidestoscroll_768="1" data-slidestoshow_768="2" data-slidestoscroll_480="1" data-slidestoshow_480="1">
	    			<?php
							$getcatname = get_term( $category_id,'product_cat' );
							$slug = $getcatname->slug;
							/*
							 $term_id = $getcatname->term_id;
							$args = array (
									'post_type'  => 'product',
									'posts_per_page'  => -1,
									'tax_query'=> array(
								array(
									'taxonomy' => 'product_type',
									'field'    => 'slug',
									'terms'    => 'bundle',
								),
							),
							);
																		$filteredFoo = array_diff($foo, $bar);
							$cateProductIds		=	array();
							$cateQuery = new WP_Query( $args );
							if ( $cateQuery->have_posts() ) :
								while ( $cateQuery->have_posts() ) :
									$cateQuery->the_post();
									$cateProductIds[]		=	 get_the_ID();
								endwhile;
							endif;
							wp_reset_postdata();
							//print_r($cateProductIds);

							$findCateproductIds = array_intersect($attributProductIds, $cateProductIds);
							*/
							$args = array( 'post_type' => 'product', 'posts_per_page' => 9999,'product_cat' => $slug,  'orderby' => 'DESC','post__in'=>$attributProductIds,'post__not_in' => array(7021322,7021219));
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) :
								while ( $loop->have_posts() ) : $loop->the_post();
										global $product;

									?>
									<?php wc_get_template_part( 'content', 'product' ); ?>
								<?php endwhile; ?>
							<?php endif;?>
	    			<?php wp_reset_postdata(); ?>
					</div>
					<?php $counter++; ?>
				</div>
			<?php endforeach; ?>

		</div>

	</div>
</div>
