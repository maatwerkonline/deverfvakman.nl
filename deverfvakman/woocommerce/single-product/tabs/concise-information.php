<?php
/**
 * Additional Information tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/additional-information.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$attributes = $product->get_attributes();

$heading = esc_html( apply_filters( 'woocommerce_product_additional_information_heading', __( 'Concise information', 'onlinemarketingnl' ) ) );

?>

<div class="row">

	<div class="col-md-6">

		<?php if ( $heading ) : ?>
			<h2 class="h4 text-initial strong"><?php echo $heading; ?></h2>
		<?php endif; ?>

		<table class="table table-striped">

			<?php $i = 0; foreach ( $attributes as $attribute ) : ?>
				<?php if ($i < 5) : ?>
				<tr>
					<th><?php echo wc_attribute_label( $attribute->get_name() ); ?></th>
					<td><?php
						$values = array();

						if ( $attribute->is_taxonomy() ) {
							$attribute_taxonomy = $attribute->get_taxonomy_object();
							$attribute_values = wc_get_product_terms( $product->get_id(), $attribute->get_name(), array( 'fields' => 'all' ) );

							foreach ( $attribute_values as $attribute_value ) {
								$value_name = esc_html( $attribute_value->name );

								if ( $attribute_taxonomy->attribute_public ) {
									$values[] = '<a href="' . esc_url( get_term_link( $attribute_value->term_id, $attribute->get_name() ) ) . '" rel="tag">' . $value_name . '</a>';
								} else {
									$values[] = $value_name;
								}
							}
						} else {
							$values = $attribute->get_options();

							foreach ( $values as &$value ) {
								$value = make_clickable( esc_html( $value ) );
							}
						}

						echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );
					?></td>
				</tr>
			<?php endif; $i++; ?>
			<?php endforeach; ?>
		</table>

		<a href="#panel-additional_information" class="btn btn-link btn-primary px-0"><?php _e('View Extra Information', 'onlinemarketingnl'); ?> <i class="dvv dvv-arrow-right"></i></a>

		<?php
		$benefits_field = get_post_meta($product->get_id(), '_benefit_field', true);
		$disadvantages_field = get_post_meta($product->get_id(), '_disadvantage_field', true);
		$delivered_with_field = get_post_meta($product->get_id(), '_delivered_with_field', true);
		?>

		<?php if ( $benefits_field ) : ?>

			<h2 class="h4 text-initial mt-5 strong"><?php _e('Benefits and Disadvantages', 'onlinemarketingnl'); ?></h2>

			<ul class="benefits">
				<?php foreach ( $benefits_field as $field ) { ?>
					<li class="list-unstyled list-icon"><i>+</i> <?php echo $field['benefit']; ?> </li>
				<?php } ?>
			</ul>

		<?php endif; ?>

		<?php if ( $disadvantages_field ) : ?>

			<ul class="disadvantages">
				<?php foreach ( $disadvantages_field as $field ) { ?>
					<li class="list-unstyled list-icon"><i>-</i> <?php echo $field['disadvantage']; ?> </li>
				<?php } ?>
			</ul>

		<?php endif; ?>

		<?php if ( $delivered_with_field ) : ?>

			<h2 class="h4 text-initial mt-5 strong"><?php _e('Delivered with', 'onlinemarketingnl'); ?></h2>

			<ul class="delivered-with mb-0">
				<?php foreach ( $delivered_with_field as $field ) { ?>
					<li><?php echo $field['delivered_with']; ?> </li>
				<?php } ?>
			</ul>

		<?php endif; ?>

	</div>

	<div class="col-md-5 offset-md-1">

		<?php $bundle_products_ids = get_bundle_products($product->get_id(), 2); ?>

		<?php if ( $bundle_products_ids ) : ?>

			<div class="sticky">

				<h2 class="h4 text-initial strong"><?php _e('Recommended combination', 'onlinemarketingnl'); ?></h2>

				<ul class="products list-unstyled">

					<?php foreach ( $bundle_products_ids as $bundle_product ) : ?>

						<?php global $product; ?>

						<?php $classes = 'card mb-3'; ?>

						<li <?php post_class($classes, $bundle_product); ?>>
							<ul class="list-group list-group-flush">

								<div class="card-header text-muted">
									<p class="strong mb-0"><?php _e('Recommended by the experts', 'onlinemarketingnl'); ?></p>
									<?php if($product->get_type()!="bundle"){ ?>
									
								
									<p class="small mb-0"><?php _e('This product bundle is recommended by the experts from Firecom. One of the benefits of this product bundle is that is cheaper in this bundle.', 'onlinemarketingnl'); ?></p>

									<?php } else{ ?>
										<p class="small mb-0"><?php _e('This single product is part of this bundle and can also be bought alone. One of the benefits of the product bundle is that you receive discount in the bundle.', 'onlinemarketingnl'); ?></p>
									<?php } ?>
								</div>

								<a href="<?php echo get_the_permalink( $bundle_product );?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">

									<div class="list-group-item row">
										<div class="col-md-3">
											<?php if ( $product->is_on_sale( $bundle_product ) ) : ?>
												<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>
											<?php endif; ?>
											<?php echo get_the_post_thumbnail( $bundle_product, 'shop_thumbnail' ); ?>
										</div>
										<div class="col-md-9">
										<h2 class="h5 fw-600 text-initial text-normal"><?php echo get_the_title( $bundle_product ); ?></h2>
									</div>
									</div>

								</a>

								<div class="list-group-item justify-content-between">

									<?php if ( $price_html = wc_get_product( $bundle_product )->get_price_html() ) : ?>
										<span class="price text-muted"><?php echo $price_html; ?></span>
									<?php endif; ?>

									<?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
										sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
											esc_url( wc_get_product( $bundle_product )->add_to_cart_url() ),
											esc_attr( isset( $quantity ) ? $quantity : 1 ),
											esc_attr( wc_get_product( $bundle_product )->get_id() ),
											esc_attr( wc_get_product( $bundle_product )->get_sku() ),
											esc_attr( isset( $class ) ? $class .' btn btn-sm btn-success' : ' btn btn-sm btn-success' ),
											__( '<i class="dvv dvv-cart h2"></i>' )
										),
									$product ); ?>

								</div>

							</ul>
						</li>

					<?php endforeach; ?>
				</ul>

			</div>

		<?php endif; ?>

	</div>

</div>
