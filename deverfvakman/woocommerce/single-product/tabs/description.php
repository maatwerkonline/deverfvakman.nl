<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) ) );

?>

<?php if ( $heading ) : ?>
  <h2 class="h4 text-initial pt-5 strong"><?php echo $heading; ?></h2>
<?php endif; ?>

<?php the_content(); ?>

<?php

  // Product video
  $product_video    = get_field('product_video');
  $video_locations 	= $product_video['video_locations'];
  $video_enabled    = $product_video['video_enabled'];

  if($video_enabled[0] == 'enabled' && in_array('description', $video_locations)):
    $video_type       = $product_video['video_type'];
    $video_file       = $product_video['video_file'];
    $video_youtube_id = $product_video['video_youtube_id'];
    ?>
    <div class="product-video-container">
      <?php if($video_type == 'youtube'): ?>
        
        <iframe class="product-video-yt" src="https://www.youtube.com/embed/<?php echo $video_youtube_id ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      
      <?php elseif($video_type == 'file'): ?>

        <video class="product-video-file" controls>
          <?php $ext = pathinfo($video_file['url'], PATHINFO_EXTENSION); ?>

          <source src="<?php echo $video_file['url'] ?>" type="video/<?php echo $ext ?>">

          <?php _e('[Your browser does not support the video tag]', 'maatwerkonline'); ?>
        </video>

      <?php endif; ?>
    </div>
    <?php
  endif;
?>

<div class="product "></div>