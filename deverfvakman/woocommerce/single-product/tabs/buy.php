<?php
/**
 * Related Products tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<div class="d-md-flex px-5 py-2 mt-5 float-left w-100 shadow">

	<?php
	$post_thumbnail_id = get_post_thumbnail_id( $product->ID );
	$thumbnail_attributes = array(
		'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
	);

	if ( has_post_thumbnail() ) {
		$html  = '<div class="woocommerce-product-gallery__image float-left mr-5">';
		$html .= get_the_post_thumbnail( $product->ID, 'shop_thumbnail', $thumbnail_attributes );
		$html .= '</div>';
	} else {
		$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
		$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
		$html .= '</div>';
	}

	echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $product->ID ) );
	?>

	<div class="float-left mr-auto">
		<h2 class="h4 fw-600 text-initial text-normal"><?php echo get_the_title(); ?></h2>

		<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
	</div>

	<div class="align-self-center">
		<?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		    sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="btn btn-success float-right %s product_type_%s">%s</a>',
		        esc_url( $product->add_to_cart_url() ),
		        esc_attr( $product->id ),
		        esc_attr( $product->get_sku() ),
		        $product->is_purchasable() ? 'add_to_cart_button' : '',
		        esc_attr( $product->product_type ),
		        '<i class="dvv dvv-cart"></i>'
		    ),
		$product ); ?>
	</div>

</div>
