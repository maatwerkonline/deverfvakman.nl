<?php
/**
 * Related Products tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Accessories', 'onlinemarketingnl' ) ) );

?>

<?php if ( $heading ) : ?>
  <h2 class="h4 text-initial strong"><?php echo $heading; ?></h2>
<?php endif; ?>

<?php $terms = get_the_terms( $product->ID, 'product_tag' ); ?>

<?php foreach ( $terms as $term ) : ?>
	<?php $term_array[] = $term->slug; ?>
<?php endforeach; ?>

<?php $terms_out = implode(' ', $term_array); ?>

<?php
$tax_query = array('relation' => 'AND');
$tax_query[] =  array(
	'taxonomy' => 'pa_connector',
	'field' => 'slug',
	'terms' => $terms_out,
	'operator' => 'IN',
);

$args = array (
	'post_type'  => 'product',
	'posts_per_page'  => -1,
	'tax_query' => $tax_query,
); ?>

<?php $query = new WP_Query( $args ); ?>

<?php if ( $query->have_posts() ) : ?>

	<ul class="row d-flex flex-wrap list-unstyled">

	<?php while ( $query->have_posts() ) : ?>
		<?php $query->the_post(); ?>
		<li class="col-md-4"><a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a></li>
	<?php endwhile; ?>

	</ul>

<?php endif; ?>

<?php wp_reset_postdata(); ?>

<div class="row">
	<div class="col-md-3">

		<ul class="list-group" role="tablist">
		  <a class="list-group-item active" data-toggle="tab" href="#one" role="tab">Active</a>
		  <a class="list-group-item" data-toggle="tab" href="#two" role="tab">Link</a>
		  <a class="list-group-item" data-toggle="tab" href="#three" role="tab">Link</a>
		  <a class="list-group-item" data-toggle="tab" href="#four" role="tab">Disabled</a>
		</ul>

	</div>
	<div class="col-md-9">

		<div class="tab-content">
		  <div class="tab-pane active" id="one" role="tabpanel">One</div>
		  <div class="tab-pane" id="two" role="tabpanel">Two</div>
		  <div class="tab-pane" id="three" role="tabpanel">Three</div>
		  <div class="tab-pane" id="four" role="tabpanel">Four</div>
		</div>

	</div>
</div>
