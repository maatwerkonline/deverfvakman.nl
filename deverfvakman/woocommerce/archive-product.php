<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');

$category_filter_class = 'col-md-9 order-md-12 mb-4';
if ($category_filter == 'hide') :
    $category_filter_class = 'col-md-12 mb-4';
endif;
?>

<section id="shop">
    <div class="container">
        <div class="row">
            <?php
            $term_id = get_queried_object()->term_id;
            $category_filter = get_term_meta($term_id, 'category_filter', true);
            ?>
            <div class="<?php echo $category_filter_class; ?>">
                <?php
                /**
                 * woocommerce_before_main_content hook.
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 * @hooked WC_Structured_Data::generate_website_data() - 30
                 */
                do_action('woocommerce_before_main_content');
                ?>

                <div class="woocommerce-products-header">

                <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>

                    <?php if ( is_search() ) : ?>
                         <h1 class="search-form-title mt-5 mb-5" >
                            <?php printf( esc_html__( 'Showing search results for: %s', 'maatwerkonline' ), '<strong>' . esc_html( get_search_query() ) . '</strong>' ); ?>
                        </h1>
                    <?php else : ?>
                         <h1 class="woocommerce-products-header__title page-title mt-4"><?php woocommerce_page_title(); ?></h1>

                    <?php endif; ?>
                <?php endif; ?>



                    <?php
                    $description = term_description();
                    $category_text = get_term_meta($term_id, 'category_text', true);

                    if ($description) :
                        ?>
                        <?php if ($category_text == 'top') : ?>
                            <div id="term-description" class="bg-gray mt-5 p-4 mb-4">

                                <?php
                                /**
                                 * woocommerce_archive_description hook.
                                 *
                                 * @hooked woocommerce_taxonomy_archive_description - 10
                                 * @hooked woocommerce_product_archive_description - 10
                                 */
                                do_action('woocommerce_archive_description');
                                ?>

                            </div>
                        <?php else : ?>
                            <a href="#term-description" class="btn btn-link btn-primary px-0 mb-4"><?php _e('Read more about', 'onlinemarketingnl'); ?> <?php woocommerce_page_title(); ?> <i class="dvv dvv-arrow-right"></i></a>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>

                <?php if (have_posts()) : ?>

                    <div class="products-archive">

                        <div class="d-flex bg-gray w-100 justify-content-between align-items-center">
                            <?php
                            /**
                             * woocommerce_before_shop_loop hook.
                             *
                             * @hooked wc_print_notices - 10
                             * @hooked woocommerce_result_count - 20
                             * @hooked woocommerce_catalog_ordering - 30
                             */
                            do_action('woocommerce_before_shop_loop');
                            ?>
                        </div>

                        <?php woocommerce_product_loop_start(); ?>

                        <?php woocommerce_product_subcategories(); ?>

                        <?php while (have_posts()) : the_post(); ?>

                            <?php
                            /**
                             * woocommerce_shop_loop hook.
                             *
                             * @hooked WC_Structured_Data::generate_product_data() - 10
                             */
                            do_action('woocommerce_shop_loop');
                            ?>

                            <?php wc_get_template_part('content', 'product'); ?>

                        <?php endwhile; // end of the loop.  ?>

                        <?php woocommerce_product_loop_end(); ?>

                        <?php
                        /**
                         * woocommerce_after_shop_loop hook.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action('woocommerce_after_shop_loop');
                        ?>

                    </div>

                <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

                    <?php
                    /**
                     * woocommerce_no_products_found hook.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action('woocommerce_no_products_found');
                    ?>

                <?php endif; ?>

                <?php
                $description = term_description();
                $category_text = get_term_meta($term_id, 'category_text', true);

                if ($description) :
                    ?>
                    <?php if ($category_text != 'top') : ?>
                        <div id="term-description" class="bg-gray mt-5 pt-4 pr-4 pb-1 pl-4">

                            <?php
                            /**
                             * woocommerce_archive_description hook.
                             *
                             * @hooked woocommerce_taxonomy_archive_description - 10
                             * @hooked woocommerce_product_archive_description - 10
                             */
                            do_action('woocommerce_archive_description');
                            ?>

                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            </div>

            <?php if ($category_filter != 'hide') : ?>
                <aside id="filters" class="col-md-3 float-md-left pt-5 mt-md-0 sidebar hidden-sm-down">
                    <?php dynamic_sidebar(__('Categories', 'onlinemarketingnl')); ?>
                </aside>
            <?php endif; ?>

            <?php
            /**
             * woocommerce_after_main_content hook.
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            do_action('woocommerce_after_main_content');
            ?>

        </div>

    </div>
</section>

<?php get_footer('shop'); ?>
