<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div id="customer_login" class="row no-gutters d-flex justify-content-center">

	<div class="col-md-4 text-center">

<?php endif; ?>

		<h2 class="h5 text-normal"><?php _e( 'Login', 'woocommerce' ); ?></h2>

		<form class="woocomerce-form woocommerce-form-login login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="form-group">
				<label for="username" class="sr-only"><?php _e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="form-control py-2" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" placeholder="<?php _e( 'Username or email address', 'woocommerce' ); ?>" />
			</p>
			<p class="form-group">
				<label for="password" class="sr-only"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="form-control py-2" type="password" name="password" id="password" placeholder="<?php _e( 'Password', 'woocommerce' ); ?>" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
			<input type="submit" class="btn btn-primary mr-3 py-2" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />

			<label class="custom-control custom-checkbox">
				<input class="custom-control-input" name="rememberme" type="checkbox" id="rememberme" value="forever" />
				<span class="custom-control-indicator"></span>
				<span class="custom-control-description"><?php _e( 'Remember me', 'woocommerce' ); ?></span>
			</label>

			<p class="woocommerce-LostPassword lost_password mt-3 d-block">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="text-muted"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="col-md-2 d-flex align-items-center justify-content-center">
		<span class="or"><?php _e( 'or', 'maatwerkonline' ); ?></span>
	</div>

	<div class="col-md-4 d-flex align-items-center text-center">
		
		<div class="row w-100">
			<div class="col-md-12">

				<h2 class="h5 text-normal"><?php _e( 'Register', 'maatwerkonline' ); ?></h2>

				<form method="post" class="register">

					<?php do_action( 'woocommerce_register_form_start' ); ?>

					<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

						<p class="form-group">
							<label for="reg_username" class="sr-only"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="text" class="form-control py-2" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" placeholder="<?php _e( 'Username', 'woocommerce' ); ?>" />
						</p>

					<?php endif; ?>

					<p class="form-group">
						<label for="reg_email" class="sr-only"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
						<input type="email" class="form-control py-2" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( $_POST['email'] ) : ''; ?>" placeholder="<?php _e( 'Email address', 'woocommerce' ); ?>" />
					</p>

					<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

						<p class="form-group">
							<label for="reg_password" class="sr-only"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="password" class="form-control" name="password" id="reg_password" placeholder="<?php _e( 'Password', 'woocommerce' ); ?>" />
						</p>

					<?php endif; ?>

					<!-- Spam Trap -->
					<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

					<?php do_action( 'woocommerce_register_form' ); ?>

					<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
					<input type="submit" class="btn btn-primary py-2" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" />

                    <?php do_action( 'woocommerce_register_form_end' ); ?>
                    
                    <a href="https://www.deverfvakman.nl/zakelijk-bestellen-nederland/" class="btn btn-outline-primary py-2 ml-1"><?php esc_attr_e( 'Zakelijk Aanvragen', 'woocommerce' ); ?></a>

				</form>

			</div>
		</div>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
