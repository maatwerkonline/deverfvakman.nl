<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices(); ?>

<form method="post" class="woocommerce-ResetPassword lost_reset_password">

	<h2><?php _e( 'Lost your password?', 'woocommerce' ); ?></h2>

	<div class="alert alert-warning" role="alert">
		<?php echo apply_filters( 'woocommerce_lost_password_message', __( 'Please enter your username or email address. You will receive a link to create a new password via email.', 'maatwerkonline' ) ); ?>
	</div>

	<p class="form-group">
		<label for="user_login" class="form-control-label sr-only"><?php _e( 'Username or email', 'woocommerce' ); ?></label>
		<input class="form-control" type="text" name="user_login" id="user_login" placeholder="<?php _e( 'Username or email', 'woocommerce' ); ?>" />
	</p>

	<?php do_action( 'woocommerce_lostpassword_form' ); ?>

	<input type="hidden" name="wc_reset_password" value="true" />
	<input type="submit" class="btn btn-primary" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>" />

	<?php wp_nonce_field( 'lost_password' ); ?>

</form>
