<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

		<h2 class="text-normal"><?php
			/* translators: 1 */
			printf(
				__( 'Hello %1$s', 'maatwerkonline' ),
				'<strong>' . esc_html( $current_user->first_name ) . '</strong>'
			); ?>
		</h2>

		<p class="mb-5"><?php
		printf(
			__( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a> and <a href="%3$s">edit your password and account details</a>.', 'maatwerkonline' ),
			esc_url( wc_get_endpoint_url( 'orders' ) ),
			esc_url( wc_get_endpoint_url( 'edit-address' ) ),
			esc_url( wc_get_endpoint_url( 'edit-account' ) )
		);
		?></p>

		<div class="row">
			<div class="col-md-4">

				<a href="<?php echo esc_url( wc_get_endpoint_url( 'orders' ) ); ?>" class="card mb-3 orders">
					<div class="card-body">
						<i class="dvv dvv-list3 d-block mb-3 fs-4 display-2 text-normal"></i>
						<h3 class="card-title text-normal"><?php _e('Recent Orders', 'maatwerkonline'); ?></h3>
						<p class="card-text text-normal"><?php _e('View your recent orders and contact us if you have any question.', 'maatwerkonline'); ?></p>
    				</div>
    				<div class="card-footer bg-white">
    					<span href="<?php echo esc_url( wc_get_endpoint_url( 'orders' ) ); ?>" class="btn btn-primary p-4 btn-block rounded"><?php _e('Your Orders', 'maatwerkonline'); ?> <i class="dvv dvv-arrow-right"></i></span>
    				</div>
				</a>

			</div>

			<div class="col-md-4">

				<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address' ) ); ?>" class="card mb-3 edit-address">
					<div class="card-body">
						<i class="dvv dvv-home4 d-block mb-3 fs-4 display-2 text-normal"></i>
						<h3 class="card-title text-normal"><?php _e('Edit Address', 'maatwerkonline'); ?></h3>
						<p class="card-text text-normal"><?php _e('Edit your Billing Address, this will be used on new orders.', 'maatwerkonline'); ?></p>
    				</div>
    				<div class="card-footer bg-white">
    					<span class="btn btn-primary p-4 btn-block rounded"><?php _e('Edit Address', 'maatwerkonline'); ?> <i class="dvv dvv-arrow-right"></i></span>
    				</div>
				</a>

			</div>
			<div class="col-md-4">

				<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account' ) ); ?>" class="card mb-3 edit-account">
					<div class="card-body">
						<i class="dvv dvv-user d-block mb-3 fs-4 display-2 text-normal"></i>
						<h3 class="card-title text-normal"><?php _e('Edit Account', 'maatwerkonline'); ?></h3>
						<p class="card-text text-normal"><?php _e('Edit your Account Settings, this will be used on your certificate.', 'maatwerkonline'); ?></p>
    				</div>
    				<div class="card-footer bg-white">
    					<span class="btn btn-primary p-4 btn-block rounded"><?php _e('Edit Account', 'maatwerkonline'); ?> <i class="dvv dvv-arrow-right"></i></span>
    				</div>
				</a>

			</div>
		</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
