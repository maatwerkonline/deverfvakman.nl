<html>
<title><?php echo $post->post_name; ?>.pdf</title>
 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    @page { margin: 100px 0px 135px 0px; }
/*    header { left: 0px; right: 0px; height: 50px; }*/
 header { position: fixed; top: -100px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    body{padding: 0px; margin: 0px; width: 820px; font-size: 12px;}
    main{ margin-top: 0px; padding: 0px 40px 0px 40px;}

  @import url('https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i');
  body{font-family: 'Lato', sans-serif;}
    .column_left{ padding: 10px; }

    ul li{list-style: none;}
    .attachment_list{background-color: #f2f2f2; padding:20px 20px 10px; margin-top:30px; margin-bottom: 45px; text-align: center;  }
     .attachment_list ul{padding: 0;margin: 0;}
    .attachment_list li { display: inline-block; vertical-align: top; margin: 10px; }

    .BlankResumePrint_row {
        position: relative;
        width: 100%;
    }
    .BlankResumePrint_row_left {
        display: block;
        width: 40%;
        margin-right: -20%;
        float: left;
        page-break-inside: avoid;


    }

    .BlankResumePrint_row_right {
        display: block;
        float: right;
        margin-left: 20%;
        padding-left: 12px;
        width: 55%;
    }

	.BlankResumePrint_row_right .product_description ul li {  list-style-type:disc; }
	.BlankResumePrint_row_right .product_video ul li img  {margin-right:8px; }
	.BlankResumePrint_row_right h1 {margin:50px 0 15px; font-size:22px;}
	.BlankResumePrint_row_right h3 {font-size:16px;}
    footer { position: fixed; bottom: -135px; left: 0px; right: 0px; background-color:#000; height:110px; color:#fff; }
    .more_info_table{}
    .more_info_heading{}

/*    .bottom_block{ width:230px; padding-left:40px; color:#fff; }

    .bottom_block .phone-email span { vertical-align:top; }
    .bottom_block .phone-email img { margin:3px 3px 0 0;  }*/

/*.product_description  { font-size:14px !important; }
.product_description p  { font-size:14px !important; }*/


    .bottom_address p { display:inline-block; width: 230px; vertical-align:top; /*font-size:16px;*/}
    .bottom_address {padding: 2px 2px 3px 87px; font-size:14px;}
    .bottom_address{ background: #080B1E; width: 100%; font-size:14px; /*height: 100px;*/ color:#fff;}
    .grey_background li {  margin:0 0 10px; /*font-size:16px;*/ }
    .grey_background{background-color: #f2f2f2; padding:20px; margin-top:10px; margin-bottom: 45px; }
    .attach_table{ background-color:#f2f2f2; margin-top: 10px;}
    .attach_table td{ padding:10px; text-align: center;  }
    .benifits_disadvantage{ margin:0px;padding:0;text-align:left;color:#000;font-weight:bold;   font-size:15px  }


    .product_video li { margin:0 0 10px; /*font-size:16px;*/   }
    .imgBox{ margin-top: 55px;  }
  </style>
<body>

 <header>
   <?php if(strlen($this->options['logo_img_url'])>0){ ?>

  <img src="<?php echo $this->options['logo_img_url']; ?>" width="820px;">
  <?php }else{ ?>
  <img src="<?php echo WooCommerce_shopcart_path."images/template-logo.jpg"; ?>" width="820px;">
  <?php } ?>
  </header>
  <footer><?php echo woocommerce_cart_get_contact_info1(); ?></footer>

  <main>
  <strong>From theme overrided</strong>

 <?php
if (isset ( $this->options ['CustomCSS_option'] )) {
        $html = '<style>' . $this->options ['Customcss'] . '</style>';
      }
      $html .= "<body>";
      if (isset ( $this->options ['authorDetail'] ) and ! $this->options ['authorDetail'] == '') {
        $author_id = $post->post_author;
        $author_meta_key = $this->options ['authorDetail'];
        $author = get_user_meta ( $author_id );
        $html .= '<p><strong>Author : </strong>' . $author [$author_meta_key] [0] . '</p>';
      }

      if (isset ( $this->options ['postCategories'] )) {
        $categories = get_the_category ( $post->ID );
        if ($categories) {
          $html .= '<p><strong>Categories : </strong>' . $categories [0]->cat_name . '</p>';
        }
      }
      // Display tag list is set in config
      if (isset ( $this->options ['postTags'] )) {
        $tags = get_the_tags ( $post->the_tags );
        if ($tags) {
          $html .= '<p><strong>Tagged as : </strong>';
          foreach ( $tags as $tag ) {
            $tag_link = get_tag_link ( $tag->term_id );
            $html .= '<a href="' . $tag_link . '">' . $tag->name . '</a>';
            if (next ( $tags )) {
              $html .= ', ';
            }
          }
          $html .= '</p>';
        }
      }
      // Display date if set in config
      if (isset ( $this->options ['postDate'] )) {
        $newDate = date ( "d-m-Y", strtotime ( $post->post_date ) );
        $html .= '<p><strong>Date : </strong>' . $newDate . '</p>';
      }

      // Set some content to print
      // Display feachered image if set in config on page/post
      if (isset ( $this->options ['show_feachered_image'] )) {
        if (has_post_thumbnail ( $post->ID )) {}
      }
      $post_content = $post->post_content;
      if (empty ( $post->post_content )) {}

      $imgd=$this->options ['logo_img_url'];
      $file_text=preg_replace("|<a *href=\"(.*)\">(.*)</a>|","\\2",$post->post_content);



 ?>

<div class="BlankResumePrint_row">

  <div class="BlankResumePrint_row_left">
      <?php if (has_post_thumbnail ( $post->ID )) {
        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array(600,400));
$url = $src[0];
       ?>
        <?php //echo get_the_post_thumbnail ( $post->ID , array(300,400));?>
        <div class="imgBox" style="text-align:center; ">
        <!-- <img style="display:block; margin-top:50px; margin-bottom:40px; " src="<?php echo $url; ?>" /> -->
        <?php echo get_the_post_thumbnail ( $post->ID , 'thumbnail'); ?>
	  </div>
      <?php } ?>


          <?php
      //get the product advanages & benifites
        $product = wc_get_product( $post->ID );
          $benefits_field = get_post_meta($product->get_id(), '_benefit_field', true);
        $disadvantages_field = get_post_meta($product->get_id(), '_disadvantage_field', true);
        $delivered_with_field = get_post_meta($product->get_id(), '_delivered_with_field', true);
        $space="&nbsp;&nbsp;&nbsp;";
      ?>
      <div class="grey_background">
      <?php  if ( $benefits_field ) { ?>
      <div class="benifits_disadvantage">
      <?php    _e( 'Benefits & Disadvantages', 'woocommerce-pdf-productsheet' ); ?>
      </div>
      <ul style="margin:25px 0 0; padding: 0;">
      <?php foreach ( $benefits_field as $field ) { ?>
          <li><img src="<?php echo PTPDF_URL; ?>/asset/images/green_dot.jpg" width="12" height="12" alt="" style="margin-top: 2px;" /><?php echo $space; ?>
            <?php echo $field["benefit"]?>
            </li>
      <?php } echo "</ul>"; } ?>
      <?php if ( $disadvantages_field ) {
          echo '<ul style="margin:0 0 10px; padding: 0;">';
          foreach ( $disadvantages_field as $field ) { ?>
          <li><img src="<?php echo PTPDF_URL;?>/asset/images/red_dot.jpg" width="12" height="12" alt="" style="margin-top: 2px;" /> <?php echo $space; ?><?php echo $field["disadvantage"]?>
          </li>
      <?php    } echo "</ul>"; }?>

    <?php
        if ( $delivered_with_field ) {
        echo '<div class="benifits_disadvantage" style="margin-top:20px;">';
      _e( 'Delivered with', 'woocommerce-pdf-productsheet' );
      echo '</div>';
      echo "<ul style='margin:25px 0px 0 0px; padding: 0;'>";
      foreach ( $delivered_with_field as $field ) {?>
          <li> <img src="<?php echo PTPDF_URL; ?>/asset/images/dot.jpg" width="6" height="6" alt=""/><?php echo $space."&nbsp;"; ?> <?php echo $field["delivered_with"]; ?></li>
      <?php   } echo "</ul>";   } ?>
      </div>
  </div>

  <div class="BlankResumePrint_row_right">
    <h1><?php echo $post->post_title; ?></h1>
    <div class="product_description"><?php echo $file_text; ?>
    </div>
         <div class="product_video">
          <ul style="page-break-inside:avoid; margin:0; padding:0 0 60px; ">
        <?php

            $late_delivery = get_post_meta( get_the_ID(), '_late_delivery', true );
            $customer_service_points = get_theme_mod( 'customer_service_points' );
            $customer_service_page = get_theme_mod( 'customer_service_page' );
            $grntck = PTPDF_URL . '/asset/images/green_tick.jpg';
            if ( $customer_service_points != '' || $late_delivery != '') {
               if ( $late_delivery != '') {
              $service .='<li><img src="'.$grntck.'" width="15" height="15" alt="" style="margin-top: 4px;" /> Delivered on workdays within %s days</li>';
               }else {
                $twenty_four_hour = __("Delivered on workdays within 24 hours","woocommerce-pdf-productsheet");
              $service .='<li><img src="'.$grntck.'" width="15" height="15" alt="" style="margin-top: 4px;" />'.$twenty_four_hour.'</li>';

               }
              foreach ( $customer_service_points as $customer_service_point ) {
                $service .='<li><img src="'.$grntck.'" width="15" height="15" alt="" style="margin-top: 4px;" /> '.$customer_service_point['point'].'</li>';
              }


            }

            echo $service;
         ?>
         </ul>
         </div>
  </div>

</div>
<div style="clear: both"></div>
<h3 class="more_info_heading"><?php  _e( 'Extra Information', 'woocommerce-pdf-productsheet' ); ?></h3>
  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="more_info_table">
      <tbody>
        <?php
        $product = wc_get_product( $post->ID );
        if ($product->get_sku() ) {
          ?>
        <tr>
          <td style="text-align:left;color:#000;background-color: #f2f2f2;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;">Sku</td>
          <td style="text-align:left;color:#000;background-color: #f2f2f2;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> <?php echo $product->get_sku(); ?></td>
          </tr>
          <?php }else{ ?>
          <tr>
          <td style="text-align:left;color:#000;background-color: #f2f2f2;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;">Sku</td>
          <td style="text-align:left;color:#000;background-color: #f2f2f2;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> N/A</td>
          </tr>
        <?php }
        if( has_term( '', 'product_cat' ) ){ ?>
          <tr>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;">Categories</td>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"><style>style="color:#f8981d;text-decoration:none"</style> <?php echo strip_tags($product->get_categories( ', ', '<span>' . _n( '', '', sizeof( get_the_terms( $post->ID, 'product_cat' ) ), 'woocommerce' ) . ' ', '</span>' )); ?></td>
          </tr>
        <?php }
        if( has_term( '', 'product_tag' ) ){ ?>

          <tr>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0;background-color: #f2f2f2; border-bottom:1px solid #edeef0;">Tags</td>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0;background-color: #f2f2f2; border-bottom:1px solid #edeef0;"><style>style="color:#f8981d;text-decoration:none"</style> <?php echo  strip_tags(wc_get_product_tag_list( $product->get_id(), ', ' )); ?></td>
          </tr>
          <?php   }

        if ($product->get_weight() ) { ?>

          <tr>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;">Weight</td>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> <?php echo $product->get_weight(); ?></td>
          </tr>
        <?php } if( $product->has_dimensions() ){ ?>

        <tr>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0;background-color: #f2f2f2; border-bottom:1px solid #edeef0;">Dimensions</td>
          <td style="text-align:left;color:#000;border-top:1px solid #edeef0;background-color: #f2f2f2; border-bottom:1px solid #edeef0;"><style>style="color:#f8981d;text-decoration:none"</style> <?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?></td>
          </tr>
          <?php   }

        if( $product->has_attributes() ){
          $wpp_attributes = $product->get_attributes();
          if(!empty($wpp_attributes)){
            $y=0;
            foreach( $wpp_attributes as $attribute){
             if( !$attribute['is_visible'] ) continue;
              if( $attribute['is_taxonomy'] ){
                $attribute_taxonomy = $attribute->get_taxonomy_object();
                $product_terms = wp_get_post_terms( $post->ID, $attribute['name'] ); $p_terms = array();
                $attribute_name = ucwords(str_replace('attribute_','',str_replace('pa_','',$attribute['name'])));


                foreach($product_terms as $p_term){
                $p_terms[] = $p_term->name;
                }
                if ($i % 2 == 0 ){
                  echo '<tr><td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;">'.$attribute_name.'</td><td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> '.implode(', ', $p_terms)."\r\n</td></tr>";
                  }
              else
              {
                echo  '<tr><td style="text-align:left;color:#000;border-top:1px solid #edeef0; background-color: #f2f2f2;border-bottom:1px solid #edeef0;">'.$attribute_name.'</td><td style="background-color: #f2f2f2;text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> '.implode(', ', $p_terms)."\r\n</td></tr>";
                }
              $i++;
                  }
              else{
                if ($i % 2 == 0 ){
                echo  '<tr><td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;">'.$attribute['name'].'</td><td style="background-color: #f2f2f2;text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> '.$attribute['value']."\r\n</td></tr>";
                }else{ echo '<tr><td style="text-align:left;color:#000;border-top:1px solid #edeef0;background-color: #f2f2f2; border-bottom:1px solid #edeef0;">'.$attribute['name'].'</td><td style="text-align:left;color:#000;border-top:1px solid #edeef0; border-bottom:1px solid #edeef0;"> '.$attribute['value']."\r\n</td></tr>";
                }
                $i++;
              }
            }

          }
        } ?>
      </tbody>
      </table>

<?php

    $product_id = $post->ID;
    $productimg = new WC_product($product_id);
    $attachment_ids = $productimg->get_gallery_attachment_ids();
    if(count($attachment_ids)>0){
 ?>

  <!-- <div class="attachment_list">
  <ul> -->
  <table cellpadding="0" cellspacing="0" class="attach_table">
  <?php

    $i=1;
    foreach( $attachment_ids as $attachment_id ) {



        if ($i == 1) {
            echo "<tr>";
        }

      ?>
 <td><img src="<?php echo wp_get_attachment_image_src($attachment_id,'thumbnail')[0];  ?>"/></td>

      <?php

        if ($i % 4 == 0) {
            echo "</tr>";
        }
        if ($i % 4 == 0) {
            echo "<tr>";
        }
   $i++;
    ?>


    <?php } ?>
<!--   </ul>
  </div> -->
  <?php } ?>
</table>
  </main>

</body>
</html>

<?php
function woocommerce_cart_get_contact_info1(){
  $return="";
  $visiting_address = get_theme_mod( 'visiting_address' );
  $visiting_zipcode = get_theme_mod( 'visiting_zipcode' );
  $visiting_city = get_theme_mod( 'visiting_city' );
  $post_address = get_theme_mod( 'post_address' );
  $post_zipcode = get_theme_mod( 'post_zipcode' );
  $post_city = get_theme_mod( 'post_city' );
  $phone_number = get_theme_mod( 'phone_number' );
  $mobile_number = get_theme_mod( 'mobile_number' );
  $email = get_theme_mod( 'email' );
  $vat = get_theme_mod( 'vat' );
  $coc = get_theme_mod( 'coc' );
  $bank_account = get_theme_mod( 'bank_account' );

  $return = '<div class="bottom_address text">';
  $return .= ($visiting_address != '' || $visiting_zipcode != '' || $visiting_city != '' ) ?'<p class="visiting-address"><strong>'. __('Visiting Address', 'onlinemarketingnl').'</strong><br/><span><span><span class="street-address">'.esc_attr($visiting_address).'</span>,<br/><span class="postal-code">'.esc_attr($visiting_zipcode).'</span> <span class="locality">'.esc_attr($visiting_city).'</span></span></span></p>' : '';
  $return .= ($post_address != '' || $post_zipcode != '' || $post_city != '' ) ?'<p class="post-address"><strong>'. __('Post Address', 'onlinemarketingnl').'</strong></br><span><span class="street-address">'.esc_attr($post_address).'</span>,<br/><span class="postal-code">'.esc_attr($post_zipcode).'</span> <span class="locality">'.esc_attr($post_city).'</span></span></p>' : '';

  $return .= ($phone_number != '' || $mobile_number != '' || $email != '' ) ?'<p class="phone-email">' : '';
  $return .= ($phone_number != '' ) ?'<img src="'.plugins_url( 'woocommerce-pdf-productsheet/asset/images/phone.png').'" style="margin-top:5px;">  <span> '.esc_attr($phone_number).'</span>' : '';
  $return .= ($mobile_number != '' ) ?'<br/><i class="dvv dvv-phone" style="height: 13px;"></i> <span>'.esc_attr($mobile_number).'</span>' : '';
  $return .= ($email != '' ) ?'<br/><img src="'.plugins_url( 'woocommerce-pdf-productsheet/asset/images/envalop.png').'" style="margin-top:4px;"> <span> '.str_replace('@','<i>@</i>',$email).'</span>' : '';
  $return .= ($phone_number != '' || $mobile_number != '' || $email != '' ) ?'</p>' : '';

  $return .= ($vat != '' || $coc != '' || $bank_account != '' ) ?'<p class="registration-numbers">' : '';
  $return .= ($vat != '' ) ?'' .__('VAT', 'onlinemarketingnl').' : '.esc_attr($vat) : '';
  $return .= ($coc != '' ) ?'<br/>'.__('COC', 'onlinemarketingnl'). ' : ' .esc_attr($coc) : '';
  $return .= ($bank_account != '' ) ?'<br/>'. __('Bank Account', 'onlinemarketingnl'). ' : ' .esc_attr($bank_account) : '';
  $return .= ($vat != '' || $coc != '' || $bank_account != '' ) ?'</p>' : '';
  $return .= '</div>';
  return $return;
}
/*function woocommerce_cart_get_contact_info(){
  $return="";
  $visiting_address = get_theme_mod( 'visiting_address' );
  $visiting_zipcode = get_theme_mod( 'visiting_zipcode' );
  $visiting_city = get_theme_mod( 'visiting_city' );
  $post_address = get_theme_mod( 'post_address' );
  $post_zipcode = get_theme_mod( 'post_zipcode' );
  $post_city = get_theme_mod( 'post_city' );
  $phone_number = get_theme_mod( 'phone_number' );
  $mobile_number = get_theme_mod( 'mobile_number' );
  $email = get_theme_mod( 'email' );
  $vat = get_theme_mod( 'vat' );
  $coc = get_theme_mod( 'coc' );
  $bank_account = get_theme_mod( 'bank_account' );

  $return = '<table>';
  $return .='<tr><td class="bottom_block">';
  $return .= ($visiting_address != '' || $visiting_zipcode != '' || $visiting_city != '' ) ?'<p class="visiting-address"><strong>'. __('Visiting Address', 'onlinemarketingnl').'</strong><br/><span><span><span class="street-address">'.esc_attr($visiting_address).'</span>,<br/><span class="postal-code">'.esc_attr($visiting_zipcode).'</span> <span class="locality">'.esc_attr($visiting_city).'</span></span></span></p>' : '';
  $return .= ($post_address != '' || $post_zipcode != '' || $post_city != '' ) ?'<p class="post-address"><strong>'. __('Post Address', 'onlinemarketingnl').'</strong></br><span><span class="street-address">'.esc_attr($post_address).'</span>,<br/><span class="postal-code">'.esc_attr($post_zipcode).'</span> <span class="locality">'.esc_attr($post_city).'</span></span></p>' : '';
  $return .='</td>';
  $return .='<td class="bottom_block ">';
  $return .= ($phone_number != '' || $mobile_number != '' || $email != '' ) ?'<p class="phone-email">' : '';
  $return .= ($phone_number != '' ) ?'<img src="'.plugins_url( 'woocommerce-pdf-productsheet/asset/images/phone.png').'"><span>'.esc_attr($phone_number).'</span>' : '';
  $return .= ($mobile_number != '' ) ?'<br/><i class="dvv dvv-phone" style="height: 13px;"></i> <span>'.esc_attr($mobile_number).'</span>' : '';
  $return .= ($email != '' ) ?'<br/><img src="'.plugins_url( 'woocommerce-pdf-productsheet/asset/images/envalop.png').'">  <span>'.str_replace('@','<i>@</i>',$email).'</span>' : '';
  $return .= ($phone_number != '' || $mobile_number != '' || $email != '' ) ?'</p>' : '';
   $return .='</td>';
   $return .='<td class="bottom_block">';
  $return .= ($vat != '' || $coc != '' || $bank_account != '' ) ?'<p class="registration-numbers">' : '';
  $return .= ($vat != '' ) ?'' .__('VAT', 'onlinemarketingnl').' : '.esc_attr($vat) : '';
  $return .= ($coc != '' ) ?'<br/>'.__('COC', 'onlinemarketingnl'). ' : ' .esc_attr($coc) : '';
  $return .= ($bank_account != '' ) ?'<br/>'. __('Bank Account', 'onlinemarketingnl'). ' : ' .esc_attr($bank_account) : '';
  $return .= ($vat != '' || $coc != '' || $bank_account != '' ) ?'</p>' : '';
   $return .='</td>';
  $return .= '</table>';
  return $return;
}
*/
?>
