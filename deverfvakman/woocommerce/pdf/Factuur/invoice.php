<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<?php
foreach( $order->get_items( 'shipping' ) as $item_id => $item ){
	$shipping_method = $item->get_method_title();
	$pickup_data = wc_local_pickup_plus()->get_orders_instance()->get_order_items_instance()->get_order_item_pickup_location_address( $item_id, 'array' );
}

function getBetween($content,$start,$end){
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}
?>

<table class="head container">
	<tr>
		<td class="shop-info">
			<?php do_action( 'wpo_wcpdf_before_shop_name', $this->type, $this->order ); ?>
			<div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div>
			<?php do_action( 'wpo_wcpdf_after_shop_name', $this->type, $this->order ); ?>
			<?php do_action( 'wpo_wcpdf_before_shop_address', $this->type, $this->order ); ?>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
			<?php do_action( 'wpo_wcpdf_after_shop_address', $this->type, $this->order ); ?>
			<table class="shop-data">
			<tr>
				<td>Tel</td>
				<td>: 010 30 70 650</td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td>: klantenservice@deverfvakman.nl</td>
			</tr>
			<tr>
				<td>Bank</td>
				<td>: NL59 ABNA 0883 0825 51</td>
			</tr>
			<tr>
				<td>KvK</td>
				<td>: 784.610.73</td>
			</tr>
			<tr>
				<td>BTW</td>
				<td>: NL861408901B01</td>
			</tr>
			</table>
		</td>
		<td class="header">
		<?php
		if( $this->has_header_logo() ) {
			$this->header_logo();
		} else {
			echo $this->get_title();
		}
		?>
		</td>
	</tr>
</table>

<h1 class="document-type-label">
	Factuur
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<!-- <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
			<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
			<?php $this->billing_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
			<?php if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<?php } ?>
			<?php if ( isset($this->settings['display_phone']) ) { ?>
			<div class="billing-phone"><?php $this->billing_phone(); ?></div>
			<?php } ?>
		</td>
		<td class="address shipping-address">
			<?php if ($shipping_method == "Afhalen") {
				echo "<b class='color-red'>AFHALEN</b>" . "<br>";
				echo $pickup_data['address_1'] . "<br>";
				$postcode_numbers = substr($pickup_data['postcode'], 0, 4);
				$postcode_letters = substr($pickup_data['postcode'], -2);
				echo $postcode_numbers . " " . $postcode_letters . " " . $pickup_data['city'];

			} else { ?>

				<?php if ( !empty($this->settings['display_shipping_address']) && ( $this->ships_to_different_address() || $this->settings['display_shipping_address'] == 'always' ) ) { ?>
				<h3><?php _e( 'Ship To:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
				<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
				<?php $this->shipping_address(); ?>
				<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
				<?php } ?>

			<?php } ?>

		</td>
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
				<?php if ( isset($this->settings['display_number']) ) { ?>
				<tr class="invoice-number">
					<th><?php _e( 'Invoice Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->invoice_number(); ?></td>
				</tr>
				<?php } ?>
				<?php if ( isset($this->settings['display_date']) ) { ?>
				<tr class="invoice-date">
					<th><?php _e( 'Invoice Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->invoice_date(); ?></td>
				</tr>
				<?php } ?>
				<tr class="order-number">
					<th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_number(); ?></td>
				</tr>
				<tr class="order-date">
					<th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_date(); ?></td>
				</tr>
				<tr class="payment-method">
					<th><?php _e( 'Payment method', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->payment_method(); ?></td>
				</tr>
				<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
			</table>
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="afb"></th>
			<th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="meta">Eigenschappen</th>
			<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="price"><?php _e('Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
			<th class="afb">
				<span class="item-image"><?php echo $item['thumbnail'] ?></span>
			</th>
			<td class="product">
				<div class="info-container">
					<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
					<span class="item-name"><?php echo $item['name']; ?></span><br>
					<?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
				</div>
			</td>
			<td>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="price">
				<?php echo $item['line_total']; ?>
			</td>
		</tr>
		<?php endforeach; endif; ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders"></td>
			<td class="no-borders">
				<div class="document-notes">
					<?php do_action( 'wpo_wcpdf_before_document_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_document_notes() ) : ?>
						<h3><?php _e( 'Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->document_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_document_notes', $this->type, $this->order ); ?>
				</div>
				<div class="customer-notes">
					<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_shipping_notes() ) : ?>
						<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->shipping_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
				</div>
			</td>
			<td class="no-borders" colspan="3">
				<table class="totals">
					<tfoot>
						<?php
						//get ['line_total'] of every item
						//clean up the values
						//Add the values
						//print the final value in the correct format

						$totals = $this->get_woocommerce_totals(); ?>
						<tr class="subtotal">
							<th class="description">
								Subtotaal exclusief BTW
							</th>
							<td class="price">
								<span class="totals-price">
									<?php
									$taxless_prices = [];

									$items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) :
										// $content = $item['line_total'];
										// $start = "inclusief";
										// $end = ")";
										// $output = getBetween($content,$start,$end);
										$price = $item['line_total'];
										$res = $str = preg_replace('[\D]', '', $price);
										//$res = str_replace('€', '', $res);
										$taxless_price = (int) $res;
										array_push($taxless_prices, $taxless_price);
									endforeach; endif;
									$total_taxless = 0;
									foreach ($taxless_prices as $price) {
										$total_taxless += $price;
									}
									$tax_generated = round($total_taxless / 1.21);
									$total_taxless_cents = substr($tax_generated, -2);
									$total_taxless_euros = substr_replace($tax_generated, "", -2);
									if(empty($total_taxless_euros)){
										$total_taxless_euros = 0;
									}
									if(empty($total_taxless_cents)){
										$total_taxless_cents = "00";
									}
									$subtotal = "€ " . number_format($total_taxless_euros) . "," . $total_taxless_cents;

									echo $subtotal;
									?>
								</span>
							</td>
						</tr>

						
						<?php
						//Manage tax component
						
						/*
						$content = $totals['order_total']['value'];
						$start = "inclusief";
						$end = "btw";
						$output = getBetween($content,$start,$end);
						?>
						<?php if(!empty($output)): ?>
						<tr class="btw">
							<th class="description">
								BTW 21%
							</th>
							<td class="price">
								<span class="totals-price">
									<?php echo $output ?>
								</span>
							</td>
						</tr>
						<?php endif; ?>
						*/

						?>
						<tr class="btw">
							<th class="description">
								BTW 21%
							</th>
							<td class="price">
								<span class="totals-price">
									<?php
										$taxless_prices = [];

										$items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) :
											// $content = $item['line_total'];
											// $start = "inclusief";
											// $end = ")";
											// $output = getBetween($content,$start,$end);
											$price = $item['line_total'];
											$res = $str = preg_replace('[\D]', '', $price);
											//$res = str_replace('€', '', $res);
											$taxless_price = (int) $res;
											array_push($taxless_prices, $taxless_price);
										endforeach; endif;
										$total_taxless = 0;
										foreach ($taxless_prices as $price) {
											$total_taxless += $price;
										}
										$tax_generated = round($total_taxless / 1.21);
										$tax_generated = $total_taxless - $tax_generated;
										
										if($tax_generated != 0){
											$total_taxless_generated_cents = substr($tax_generated, -2);
											$total_taxless_generated_euros = substr_replace($tax_generated, "", -2);
										} else {
											$total_taxless_generated_cents = "00";
											$total_taxless_generated_euros = "0";
										}
										$total_tax_generated = "€ " . $total_taxless_generated_euros . "," . $total_taxless_generated_cents;
									 ?>
									<?php echo $total_tax_generated; ?>
								</span>
							</td>
						</tr>

						<?php if(!empty($totals['discount']['value'])): ?>
						<tr class="description">
							<th class="description">Korting</th>
							<td class="price">
								<span class="totals-price">
									<?php echo $totals['discount']['value'] ?>
								</span>
							</td>
						</tr>
					<?php endif; ?>
						<tr class="postage">
							<th class="description">
								Verzending
							</th>
							<td class="price">
								<span class="totals-price">
									<?php
									if($total_shipping = $totals['shipping']['value'] != "Gratis verzending"):

										$total_shipping = str_replace('via Bezorgen', '', $totals['shipping']['value']);
										$total_shipping = str_replace(' ', '', $total_shipping);
										
									else:
										$total_shipping = $totals['shipping']['value'];
									endif;

									echo $total_shipping;

									?>
								</span>
							</td>
						</tr>
						<tr class="order_total">
							<th class="description">
								Totaal
							</th>
							<td class="price">
								<?php
								$price_full = $totals['order_total']['value'];
								$text_un = getBetween($price_full, "(", ")");
								$price_full = str_replace($text_un, '', $price_full);
								$price_full = str_replace('()' ,"", $price_full);
								?>
								<span class="totals-price">
									<?php echo $price_full ?>
								</span>
							</td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<?php $this->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
