<html>
<title><?php echo  $pdf_file_title; ?></title>
 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/icons/icons.css' ?>">
  <style>
    @page { margin: 70px 0 70px 0;}
    header { left: 0px; top:-70px; right: 0px; height: 50px; position: fixed; }
    body{padding: 0px; margin: 0px; width: 820px;}
    main{ margin-top: 40px; }
    .table { width: 820px; margin-bottom: 1rem;}
  .table thead th { vertical-align: bottom; border-bottom: 2px solid #eceeef;}
  .table td, .table th { padding: .75rem; border-top: 1px solid #eceeef;  }
  .cart_total_table thead th { vertical-align: bottom; }
  .cart_total_table td, .cart_total_table th { padding: .75rem;}
  .cart_total_table{ width:400px; float: right;}
  #shipping_method{ padding-left: 0px; }
  #shipping_method li{ list-style: none; }


	@import url('https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i');
	body{font-family: 'Lato', sans-serif;}
		.table td, .table th{font-family: 'Lato', sans-serif;}
		.cart_total_table {border:1px;border-radius: 4px;}
		.cart-subtotal th {text-align: left !important;}
		.cart_total_table h3{ margin: 0px !important;}
		.cart_totals{border:1px !important; border-radius:4px;}
		.cart_total_table td, .cart_total_table th {padding: .50rem !important;}
		.cart_total_table{border-radius: 4px; margin: 20px 0;}
		.list-unstyled li{ list-style: none; margin-bottom: 5px; }
		.list-unstyled li i.fa{ margin-right: 5px; color: #65bc25;}
		.service_icon{ margin: 60px 0px 0px 10px;}
		.bottom_address p { vertical-align: top; display:inline-block;  width: 230px;}
		.bottom_address {padding: 2px 2px 3px 87px;}
	    .bottom_address{position: absolute; bottom:0px; background: #080B1E; width: 100%; color: #fff;}
	    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 100px; background-color: green;}
	    /*.bottom_address{position: absolute; bottom:0px; background: #5f5f5f; width: 100%; height: 100px; color: #fff;}*/
	    /*.dvv dvv-checkmark-circle:before { content: "\e87f"; background-color: #65bc25; color: #fff; border-radius:5px 5px 5px 0px; padding-bottom: .6px; padding-left: 1.3px; }*/
	    .service_table{width:400px; float: left;}

  </style>
<body>
  <header>
   <?php if(strlen(get_option( 'WooCommerce_shopcart_pdf_headerlogo_id' ))>0){ ?>

	<img src="<?php echo wp_get_attachment_url(get_option( 'WooCommerce_shopcart_pdf_headerlogo_id' )); ?>" width="820px;">
  <?php }else{ ?>
  <img src="<?php echo WooCommerce_shopcart_path."images/template-logo.jpg"; ?>" width="820px;">
  <?php } ?>
  </header>
    <footer>
<?php echo woocommerce_cart_get_contact_info(); ?>
  </footer>
  <main>
<?php
global $woocommerce;
?>

		<table cellpadding="3" cellspacing="2" border="0" style="margin-bottom: 20px; margin-left: 40px;">
			<tr><th><?php _e('Date','woocommerce-pdf-cart'); ?> : </th><td><?php echo date_i18n(get_option('date_format'), strtotime(date("Y-m-d"))); ?></td></tr>
		</table>
		<table class="table" cellspacing="0">
		<thead>
			<tr>

				<th class="product-thumbnail hidden-sm-down">&nbsp;</th>
				<th class="product-name"><?php _e('Product','woocommerce-pdf-cart'); ?></th>
				<th class="product-price hidden-sm-down"><?php _e('Price','woocommerce-pdf-cart'); ?></th>
				<th class="product-quantity"><?php _e('Quantity','woocommerce-pdf-cart'); ?></th>
				<th class="product-subtotal"><?php _e('Total','woocommerce-pdf-cart'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($woocommerce->session->cart as $products){
			$product = wc_get_product($products['product_id']);
			?>
		<tr class="woocommerce-cart-form__cart-item cart_item">
			<td class="product-thumbnail hidden-sm-down">
				<?php echo get_the_post_thumbnail($products['product_id'],array( 80, 80));  ?>
			</td>
			<td class="product-name"><?php echo $product->get_title(); ?></td>
			<td class="product-price hidden-sm-down">
				<span class="woocommerce-Price-amount amount">
				<?php 
					echo  wc_price(get_post_meta($products['product_id'] , '_price', true),array());
					?></span>
			</td>
			<td class="product-quantity" data-title="Quantity">
				<div class="quantity"><?php echo $products['quantity']; ?></div>
			</td>
			<td class="product-subtotal" data-title="Total">
				<span class="woocommerce-Price-amount amount">
					<?php echo wc_price($products['line_total'],array()); ?>
				</span>
			</td>
			</tr>
			<?php } ?>


		</tbody>
	</table>
	<div class="cart-total" style="margin-top: 40px;">
		<div class="service_table">
			<?php
	$customer_service_points = get_theme_mod( 'customer_service_points' );
	?>
	<?php if ( $customer_service_points != '') : ?>
		<ul class="list-unstyled service_icon">
		<?php foreach ( $customer_service_points as $customer_service_point ) : ?>
			<li class="list-icon">
			<img src="<?php echo WooCommerce_shopcart_path."images/check--icon.jpg"; ?>" style="margin-top:3px;"> &nbsp;
			<span class="text-normal"><?php echo $customer_service_point['point']; ?></span></li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
		</div>
		<table cellspacing="0" class="cart_total_table" style="page-break-inside: avoid;">
		<tr>
			<td colspan="2"><h3><?php _e('Cart Totals','woocommerce-pdf-cart'); ?></h3><hr/></td>
		</tr>
		<tbody>

			<tr class="cart-subtotal">
				<th><?php _e('Subtotal','woocommerce-pdf-cart'); ?></th>
				<td><?php echo wc_price($woocommerce->session->cart_totals['subtotal'],array()); ?></td>
			</tr>
			<tr>
				<th><?php _e('VAT','woocommerce-pdf-cart'); ?></th>
				<td><?php echo wc_price( $woocommerce->session->cart_totals['total_tax'], $args ); ?>
						
					</td>
			</tr>
			<tr> <td colspan="2"><hr/></td></tr>
			<tr class="order-total">
				<th><?php _e('Total','woocommerce-pdf-cart'); ?></th>
				<td><strong><?php 
				  echo wc_price($woocommerce->session->cart_totals['total'], $args );  
				?></strong> </td>
			</tr>

			</tbody>
		</table>
		<table style="clear:both; margin-left: 40px;">
			<tr><td>
			<?php _e('No rights can be derived from this automatic quotation.','woocommerce-pdf-cart'); ?>
			</td></tr>
		</table>

	</div>
	<div style="height:2px; background: #fff; clear:both;"></div>
	
	

  </main>

</body>
</html>
<?php


function woocommerce_cart_get_contact_info(){
	$return="";
	$visiting_address = get_theme_mod( 'visiting_address' );
	$visiting_zipcode = get_theme_mod( 'visiting_zipcode' );
	$visiting_city = get_theme_mod( 'visiting_city' );
	$post_address = get_theme_mod( 'post_address' );
	$post_zipcode = get_theme_mod( 'post_zipcode' );
	$post_city = get_theme_mod( 'post_city' );
	$phone_number = get_theme_mod( 'phone_number' );
	$mobile_number = get_theme_mod( 'mobile_number' );
	$email = get_theme_mod( 'email' );
	$vat = get_theme_mod( 'vat' );
	$coc = get_theme_mod( 'coc' );
	$bank_account = get_theme_mod( 'bank_account' );

	$return = '<div class="bottom_address text">';
	$return .= ($visiting_address != '' || $visiting_zipcode != '' || $visiting_city != '' ) ?'<p class="visiting-address"><strong>'. __('Visiting Address', 'woocommerce-pdf-cart').'</strong><br/><span><span><span class="street-address">'.esc_attr($visiting_address).'</span>,<br/><span class="postal-code">'.esc_attr($visiting_zipcode).'</span> <span class="locality">'.esc_attr($visiting_city).'</span></span></span></p>' : '';
	$return .= ($post_address != '' || $post_zipcode != '' || $post_city != '' ) ?'<p class="post-address"><strong>'. __('Post Address', 'woocommerce-pdf-cart').'</strong></br><span><span class="street-address">'.esc_attr($post_address).'</span>,<br/><span class="postal-code">'.esc_attr($post_zipcode).'</span> <span class="locality">'.esc_attr($post_city).'</span></span></p>' : '';

	$return .= ($phone_number != '' || $mobile_number != '' || $email != '' ) ?'<p class="phone-email">' : '';
	$return .= ($phone_number != '' ) ?'<i class="dvv dvv-phone" style="height: 13px;"></i> <span>'.esc_attr($phone_number).'</span>' : '';
	$return .= ($mobile_number != '' ) ?'<br/><i class="dvv dvv-phone" style="height: 13px;"></i> <span>'.esc_attr($mobile_number).'</span>' : '';
	$return .= ($email != '' ) ?'<br/><i class="dvv dvv-envelope" style="height: 13px;"></i> <span>'.str_replace('@','<i>@</i>',$email).'</span>' : '';
	$return .= ($phone_number != '' || $mobile_number != '' || $email != '' ) ?'</p>' : '';

	$return .= ($vat != '' || $coc != '' || $bank_account != '' ) ?'<p class="registration-numbers">' : '';
	$return .= ($vat != '' ) ?'' .__('VAT', 'woocommerce-pdf-cart').' : '.esc_attr($vat) : '';
	$return .= ($coc != '' ) ?'<br/>'.__('COC', 'woocommerce-pdf-cart'). ' : ' .esc_attr($coc) : '';
	$return .= ($bank_account != '' ) ?'<br/>'. __('Bank Account', 'woocommerce-pdf-cart'). ' : ' .esc_attr($bank_account) : '';
	$return .= ($vat != '' || $coc != '' || $bank_account != '' ) ?'</p>' : '';
	$return .= '</div>';
	return $return;
}