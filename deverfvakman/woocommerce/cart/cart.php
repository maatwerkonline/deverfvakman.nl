<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */
if (!defined('ABSPATH')) {
    exit;
}

wc_print_notices();
?>
<form class="woocommerce-cart-form row" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">

    <div class="col-12">
        <?php do_action('woocommerce_before_cart'); ?>
    </div>

    <div class="col-md-8 order-2 order-md-1  mt-5 mb-5">
        <div class="mb-5 d-md-flex justify-content-between">
            <h1 class="h5 text-normal fw-600"><?php _e('Your Cart', 'maatwerkonline'); ?></h1>
        </div>

        <?php do_action('woocommerce_before_cart_table'); ?>

        <div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">

            <?php do_action('woocommerce_before_cart_contents'); ?>

            <?php
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->get_permalink($cart_item), $cart_item, $cart_item_key);
                    $color = 'text-normal';
                    ?>

                    <div class="woocommerce-cart-form__cart-item cart_item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?> mb-3">
                        

                            <div class="card border-0" <?php /* echo $style; */ ?>>

                                <div class="card-body row text-normal <?php echo $color; ?>">

                                    <div class="product-thumbnail col-md-2">
                                        <?php
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image('thumbnail'), $cart_item, $cart_item_key);

                                        echo $thumbnail;
                                        ?>
                                    </div>

                                    <div class="product-name-quantitiy d-flex mt-2 col-md-5">
                                        <div class="product-name font-weight-bold w-100" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                                        <a href="<?php echo esc_url($product_permalink); ?>" class="d-block">
                                            <?php
                                            echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;';
                                            echo WC()->cart->get_item_data($cart_item);
                                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                                echo '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>';
                                            }
                                            ?>
                                        </a>
                                        </div>
                                    </div>
                                    <div class="product-qty-input d-flex col-md-3">
                                        <div class="product-quantity d-inline" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                                            <?php
                                            if ($_product->is_sold_individually()) {
                                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                            } else {
                                                $product_quantity = woocommerce_quantity_input(array(
                                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                                    'input_value' => $cart_item['quantity'],
                                                    'max_value' => $_product->get_max_purchase_quantity(),
                                                    'min_value' => '0',
                                                        ), $_product, false);
                                            }

                                            echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="product-price col-md-2" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                                        <?php
                                            echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                        ?>
                                    </div>

                                </div>
                                <div class="card-footer border-0 py-4">
                                </div>
                            </div>

                        <div class="product-remove-edit p-2">
                            <span class="product-remove">
                                <?php
                                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                                '<a href="%s" class="btn btn-sm btn-link %s" aria-label="%s" data-product_id="%s" data-product_sku="%s">%s %s</a>',
                                                esc_url(WC()->cart->get_remove_url($cart_item_key)),
                                                $color,
                                                __('Remove', 'maatwerkonline'),
                                                esc_attr($product_id),
                                                esc_attr($_product->get_sku()),
                                                '<i class="dvv dvv-trash2"></i>',
                                                __('Remove', 'maatwerkonline')
                                        ), $cart_item_key);
                                ?>
                            </span>                         
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <?php do_action('woocommerce_cart_contents'); ?>

            <div>
                <div class="actions">

                    <input type="submit" class="d-none update-cart btn btn-secondary button float-right" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>">

                    <?php do_action('woocommerce_cart_actions'); ?>

                    <?php wp_nonce_field('woocommerce-cart'); ?>
                    <a class=" btn btn-primary btn-sharp   waves-effect waves-light mt-4" style="" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" target="_self"><?php esc_attr_e('Continue shopping', 'maatwerkonline'); ?></a>
                </div>
            </div>

            <?php do_action('woocommerce_after_cart_contents'); ?>

        </div>
        <?php do_action('woocommerce_after_cart_table'); ?>

    </div>

    <?php
    /**
     * woocommerce_cart_collaterals hook.
     *
     * @hooked woocommerce_cross_sell_display
     * @hooked woocommerce_cart_totals - 10
     */
    do_action('woocommerce_cart_collaterals');
    ?>

</form>
<?php do_action('woocommerce_after_cart'); ?>