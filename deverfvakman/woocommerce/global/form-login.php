<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() ) {
	return;
}

?>
<form class="woocomerce-form woocommerce-form-login login" method="post" <?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>

	<h2 class="border"><?php _e( 'Login', 'woocommerce' ); ?></h2>
	
	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<?php echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; ?>

	<div class="row">
		<div class="col-md-6">
			<p class="form-group">
				<label for="username" class="form-control-label sr-only"><?php _e( 'Username or email', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="form-control" name="username" id="username" placeholder="<?php _e( 'Username or email', 'woocommerce' ); ?>" />
			</p>
		</div>
		<div class="col-md-6">
			<p class="form-group">
				<label for="password" class="form-control-label sr-only"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="form-control" type="password" name="password" id="password" placeholder="<?php _e( 'Password', 'woocommerce' ); ?>" />
			</p>
		</div>
	</div>

	<?php do_action( 'woocommerce_login_form' ); ?>

	<?php wp_nonce_field( 'woocommerce-login' ); ?>
	<input type="submit" class="btn btn-primary mr-3" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
	<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />
		
	<label class="custom-control custom-checkbox">
		<input class="custom-control-input" name="rememberme" type="checkbox" id="rememberme" value="forever" />
		<span class="custom-control-indicator"></span>
		<span class="custom-control-description"><?php _e( 'Remember me', 'woocommerce' ); ?></span>
	</label>

	<p class="lost_password mt-3">
		<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="text-muted"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
	</p>

	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>
