<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;

if (!comments_open()) {
    return;
}
?>
<div id="reviews" class="woocommerce-Reviews row mt-5">

    <div class="col-md-6 col-lg-6">
        <h2 class="h4 text-initial strong">Reviews</h2>
        <?php
        global $wpdb;
        global $post;
        $product = wc_get_product($post->ID);
        $rating_count = $product->get_rating_count();
        $num = 5;
        $allcount = array();
        while ($num > 0) {
            $allcount[] = $wpdb->get_var("SELECT COUNT(*) meta_value FROM $wpdb->commentmeta LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id =$wpdb->comments.comment_ID WHERE meta_key = 'rating' AND comment_post_ID =  $post->ID AND comment_approved = '1' AND meta_value = $num");
            $num = $num - 1;
        }

        $fifthPercent = $allcount[0] > 0 ? ($allcount[0] / $rating_count) * 100 : 0;
        $fourthPercent = $allcount[1] > 0 ? ($allcount[1] / $rating_count) * 100 : 0;
        $thirdPercent = $allcount[2] > 0 ? ($allcount[2] / $rating_count) * 100 : 0;
        $secondPercent = $allcount[3] > 0 ? ($allcount[3] / $rating_count) * 100 : 0;
        $firstPercent = $allcount[4] > 0 ? ($allcount[4] / $rating_count) * 100 : 0;
        $ratedCount = $rating_count > 0 ? (5 / $rating_count) * 100 : 0;

        $fifthTitle = round($fifthPercent) != 0 ? round($fifthPercent) . '%' . __('of reviews have 5 stars', 'onlinemarketingnl') : '';
        $fourthTitle = round($fourthPercent) != 0 ? round($fourthPercent) . '%' . __('of reviews have 5 stars', 'onlinemarketingnl') : '';
        $thirdTitle = round($thirdPercent) != 0 ? round($thirdPercent) . '%' . __('of reviews have 5 stars', 'onlinemarketingnl') : '';
        $secondTitle = round($secondPercent) != 0 ? round($secondPercent) . '%' . __('of reviews have 5 stars', 'onlinemarketingnl') : '';
        $firstTitle = round($firstPercent) != 0 ? round($firstPercent) . '%' . __('of reviews have 5 stars', 'onlinemarketingnl') : '';
        ?>

        <?php if ($product->get_rating_count() > 0) : ?>
            <p class="h1"><?php echo number_format($product->get_average_rating(), 1); ?> <span class="small text-muted">/ 5</span></p>
            <?php
            $average = $product->get_average_rating();
            $rating_per = $average * 20;

            $review_count = $product->get_review_count();
            ?>

            <div class="ratings rt-rating">
                <div class="empty-stars">
                    <div class="full-stars" style="width:<?php echo $rating_per; ?>%"></div>
                    <?php
                    if ($review_count > 0) {
                        echo '<p>' . $review_count . ' reviews</p>';
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>



        <div id="comments">

            <?php if (have_comments()) : ?>

                <ul class="commentlist list-unstyled">
                    <?php wp_list_comments(apply_filters('woocommerce_product_review_list_args', array('callback' => 'woocommerce_comments'))); ?>
                </ul>

                <?php
                if (get_comment_pages_count() > 1 && get_option('page_comments')) :
                    echo '<nav class="woocommerce-pagination">';
                    paginate_comments_links(apply_filters('woocommerce_comment_pagination_args', array(
                        'prev_text' => '&larr;',
                        'next_text' => '&rarr;',
                        'type' => 'list',
                    )));
                    echo '</nav>';
                endif;
                ?>

            <?php endif; ?>
        </div>

        <?php if (get_option('woocommerce_review_rating_verification_required') === 'no' || wc_customer_bought_product('', get_current_user_id(), $product->get_id())) : ?>

            <div id="review_form_wrapper">
                <div id="review_form">
                    <?php
                    $commenter = wp_get_current_commenter();

                    if (is_user_logged_in()) {
                        $comment_form = array(
                            'title_reply' => have_comments() ? '<h2 class="h4 text-initial mt-5">' . __('Add a review', 'woocommerce') . '</h2>' : '<h2 class="h4 text-initial">' . __('Be the first to review', 'maatwerkonline') . '</h2>',
                            'title_reply_to' => __('Leave a Reply to %s', 'woocommerce'),
                            'title_reply_before' => '<span id="reply-title" class="comment-reply-title">',
                            'title_reply_after' => '</span>',
                            'comment_notes_before' => '',
                            'comment_notes_after' => '',
                            'fields' => array(
                                'author' => '<div class="form-group hidden_label">' . '<label for="author">' . esc_html__('Name', 'woocommerce') . '</label> ' .
                                '<input id="author" class="form-control required" name="author" type="text" placeholder="' . esc_html__('Name', 'onlinemarketingnl') . '" value="' . esc_attr($commenter['comment_author']) . '" size="30" aria-required="true" required /></div>',
                                'email' => '<div class="form-group hidden_label"><label for="email">' . esc_html__('Email', 'woocommerce') . '</label>' .
                                '<input id="email" class="form-control required" name="email" type="email" placeholder="' . esc_html__('Email address', 'onlinemarketingnl') . '" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" aria-required="true" required /><small id="emailHelp" class="form-text text-muted"><i class="dvv dvv-arrow-right mr-2"></i> ' . esc_html__('Will not be published.', 'onlinemarketingnl') . '</small></div>',
                            ),
                            'label_submit' => __('Review plaatsen', 'woocommerce'),
                            'class_submit' => 'btn btn-primary float-right',
                            'logged_in_as' => '',
                            'comment_field' => '',
                        );
                    } else {
                        $comment_form = array(
                            'title_reply' => have_comments() ? '<h2 class="h4 text-initial mt-5">' . __('Add a review', 'woocommerce') . '</h2>' : '<h2 class="h4 text-initial">' . __('Be the first to review', 'maatwerkonline') . '</h2>',
                            'title_reply_to' => __('Leave a Reply to %s', 'woocommerce'),
                            'title_reply_before' => '<span id="reply-title" class="comment-reply-title">',
                            'title_reply_after' => '</span>',
                            'comment_notes_before' => '',
                            'comment_notes_after' => '',
                            'fields' => array(
                                'author' => '<div class="form-group hidden_label">' . '<label for="author">' . esc_html__('Name', 'woocommerce') . '</label> ' .
                                '<input id="author" class="form-control required" name="author" type="text" placeholder="' . esc_html__('Name', 'onlinemarketingnl') . '" value="' . esc_attr($commenter['comment_author']) . '" size="30" aria-required="true" required /></div>',
                                'email' => '<div class="form-group hidden_label"><label for="email">' . esc_html__('Email', 'woocommerce') . '</label>' .
                                '<input id="email" class="form-control required" name="email" type="email" placeholder="' . esc_html__('Email address', 'onlinemarketingnl') . '" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" aria-required="true" required /><small id="emailHelp" class="form-text text-muted"><i class="dvv dvv-arrow-up mr-2"></i> ' . esc_html__('Will not be published.', 'onlinemarketingnl') . '</small></div>',
                                'comment_field' => '<div class="comment-form-comment form-group hidden_label"><label for="comment">' . esc_html__('Your review', 'woocommerce') . '</label><textarea class="form-control required" id="comment" name="comment" cols="45" rows="3" aria-required="true" required placeholder="' . esc_html__('Review', 'woocommerce') . '"></textarea></div>',
                            ),
                            'label_submit' => __('Review plaatsen', 'woocommerce'),
                            'class_submit' => 'btn btn-primary float-right',
                            'logged_in_as' => '',
                            'comment_field' => '',
                        );
                    }



                    if ($account_page_url = wc_get_page_permalink('myaccount')) {
                        $comment_form['must_log_in'] = '<p class="alert alert-warning" role="alert">' . sprintf(__('You must be <a href="%s">logged in</a> to post a review.', 'woocommerce'), esc_url($account_page_url)) . '</p>';
                    }
                    if (is_user_logged_in()) {

                        $comment_form['comment_field'] .= '<div class="comment-form-comment form-group hidden_label"><label for="comment">' . esc_html__('Your review', 'woocommerce') . '</label><textarea class="form-control required" id="comment" name="comment" cols="45" rows="3" aria-required="true" required placeholder="' . esc_html__('Review', 'woocommerce') . '"></textarea></div>';
                    }

                    comment_form(apply_filters('woocommerce_product_review_comment_form_args', $comment_form));
                    ?>
                </div>
            </div>

        <?php else : ?>

            <div class="woocommerce-verification-required alert alert-danger" role="alert"><?php _e('Only logged in customers who have purchased this product may leave a review.', 'woocommerce'); ?></div>

        <?php endif; ?>

    </div>
    <?php
        $product = wc_get_product( get_the_id() );
    ?>
    
    <div class="col-md-6 col-lg-6">
        <div class="wpl-enquiry">
            <h2 class="h4 text-initial strong">Stel een vraag over dit artikel</h2>
            <form action="<?php echo admin_url('admin-ajax.php'); ?>" id="wpl-enquiry-form" method="post">
                <div class="col-12">
                    <textarea name="eq-content" placeholder="Vraag" required></textarea>
                    <input type="text" name="wpname" placeholder="Naam" value="" required />
                    <input type="email" name="wpemail" placeholder="E-mailadres of telefoonnummer" value="" required />
                    <input type="hidden" name="wpproductname" value="<?php echo $product->get_name(); ?>">
                    <input type="Submit" class="btn-primary" name="wpSubmit" value="Verzenden" >
                    <input type="hidden" name="action" value="custom_action">
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    jQuery(document).ready(function ($) {
        $('#wpl-enquiry-form').on('submit', function (e) {
            e.preventDefault();
            var $form = $(this);
            $.post($form.attr('action'), $form.serialize(), function (data) {
                alert(data.message);
            }, 'json');
        });
    });
</script>
