<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $product;
?>

<section id="product" class="pt-3">
    <div class="container">
        <?php $classes = array('row',); ?>
        <div id="product-<?php the_ID(); ?>" <?php post_class($classes); ?>>

            <?php
            /**
             * woocommerce_before_single_product hook.
             *
             * @hooked wc_print_notices - 10
             */
            do_action('woocommerce_before_single_product');

            if (post_password_required()) {
                echo get_the_password_form();
                return;
            }
            ?>

            <?php
            /**
             * woocommerce_before_single_product_summary hook.
             *
             * @hooked woocommerce_show_product_sale_flash - 10
             * @hooked woocommerce_show_product_images - 20
             */
            do_action('woocommerce_before_single_product_summary');
            ?>

            <div class="col-md-12 col-lg-5 summary entry-summary">
                <div class="row">
                    <div class="col-12 product-summary-holdlder">
                        <div class="sl-product clearfix">

                            <?php if ( $product->is_type( 'simple' ) ) : ?>
                                <?php 
                                    $inhoud_kies_je_inhoud = $product->get_attribute( 'pa_kies-je-inhoud' );
                                    $inhoud = $product->get_attribute( 'pa_inhoud' );
                                    $inhoud_in_kilo = $product->get_attribute( 'pa_inhoud-in-kilo' );
                                    $inhoud_in_meters = $product->get_attribute( 'pa_inhoud-in-meters' );
                                    $wit_of_kleur = $product->get_attribute( 'pa_wit-of-kleur' );
                                    $kwast_maat = $product->get_attribute( 'pa_kwast-maat' );
                                    $poolhoogte = $product->get_attribute( 'pa_poolhoogte' );
                                    $roller_breedte = $product->get_attribute( 'pa_roller-breedte' );
                                    $tape_lengte = $product->get_attribute( 'pa_tape-lengte' );
                                    $tape_breedte = $product->get_attribute( 'pa_tape-breedte' );
                                    $tape_kleur = $product->get_attribute( 'pa_tape-kleur' );
                                    $schuurpapier_lengte = $product->get_attribute( 'pa_schuurpapier-lengte' );
                                    $grofte = $product->get_attribute( 'pa_grofte' );
                                ?>
                                <table class="simple-product variations" cellspacing="0">
                                    <tbody>

                                        <?php if($inhoud_kies_je_inhoud): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_kies-je-inhoud'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $inhoud_kies_je_inhoud; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $inhoud_kies_je_inhoud; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $inhoud_kies_je_inhoud; ?>" title="<?php echo $inhoud_kies_je_inhoud; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $inhoud_kies_je_inhoud; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        
                                        <?php if($inhoud): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_inhoud'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $inhoud; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $inhoud; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $inhoud; ?>" title="<?php echo $inhoud; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $inhoud; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($inhoud_in_kilo): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_inhoud-in-kilo'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $inhoud_in_kilo; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $inhoud_in_kilo; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $inhoud_in_kilo; ?>" title="<?php echo $inhoud_in_kilo; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $inhoud_in_kilo; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($inhoud_in_meters): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_inhoud-in-meters'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $inhoud_in_meters; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $inhoud_in_meters; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $inhoud_in_meters; ?>" title="<?php echo $inhoud_in_meters; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $inhoud_in_meters; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($kwast_maat): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_kwast-maat'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $kwast_maat; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $kwast_maat; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $kwast_maat; ?>" title="<?php echo $kwast_maat; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $kwast_maat; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($poolhoogte): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_poolhoogte'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $poolhoogte; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $poolhoogte; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $poolhoogte; ?>" title="<?php echo $poolhoogte; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $poolhoogte; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($roller_breedte): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_roller-breedte'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $roller_breedte; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $roller_breedte; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $roller_breedte; ?>" title="<?php echo $roller_breedte; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $roller_breedte; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($tape_lengte): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_tape-lengte'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $tape_lengte; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $tape_lengte; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $tape_lengte; ?>" title="<?php echo $tape_lengte; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $tape_lengte; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($tape_breedte): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_tape-breedte'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $tape_breedte; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $tape_breedte; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $tape_breedte; ?>" title="<?php echo $tape_breedte; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $tape_breedte; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($tape_kleur): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_tape-kleur'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $tape_kleur; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $tape_kleur; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $tape_kleur; ?>" title="<?php echo $tape_kleur; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $tape_kleur; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($schuurpapier_lengte): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_schuurpapier-lengte'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $schuurpapier_lengte; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $schuurpapier_lengte; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $schuurpapier_lengte; ?>" title="<?php echo $schuurpapier_lengte; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $schuurpapier_lengte; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($grofte): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_grofte'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $grofte; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper radio-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $grofte; ?>" class="variable-item radio-variable-item selected" data-title="<?php echo $grofte; ?>" title="<?php echo $grofte; ?>" role="radio" tabindex="0">
                                                            <input class="wvs-radio-variable-item" checked="checked" type="radio"><i></i><label><?php echo $grofte; ?></label>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if($wit_of_kleur): ?>
                                            <tr>
                                                <td class="label">
                                                    <label><?php echo wc_attribute_label('pa_wit-of-kleur'); ?></label>
                                                    <span class="woo-selected-variation-item-name">: <?php echo $wit_of_kleur; ?></span>
                                                </td>
                                                <td class="value woo-variation-items-wrapper">
                                                    <ul role="radiogroup" class="variable-items-wrapper button-variable-wrapper">
                                                        <li aria-checked="true" data-wvstooltip="<?php echo $wit_of_kleur; ?>" class="variable-item button-variable-item selected" data-title="<?php echo $wit_of_kleur; ?>" title="<?php echo $wit_of_kleur; ?>" role="radio" tabindex="0">
                                                            <span class="variable-item-span variable-item-span-button"><?php echo $wit_of_kleur; ?></span>
                                                        </li>
                                                        <?php if($wit_of_kleur == 'Kleur') { ?>
                                                            <div class="input-box input-box-colorselector">
                                                                <input name="colorselector" onload="setContrast()" class="required-entry colorselector-input" id="colorselector" type="text" placeholder="Druk op 'Kleur' hierboven of type je kleurcode">
                                                                <div class="colorselector-color" id="colorselector-color" data-kleurenwaaier="Nee"></div>
                                                            </div>
                                                            <input type="hidden" name="remember-color-input" id="remember-color-input" value=""><input type="hidden" name="remember-color-code" id="remember-color-code" value="">
                                                        <?php }; ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                    </tbody>
                                </table>
                                
                            <?php endif; ?>

                            <?php
                            /**
                             * woocommerce_single_product_summary hook.
                             *
                             * @hooked woocommerce_template_single_title - 5
                             * @hooked woocommerce_template_single_rating - 10
                             * @hooked woocommerce_template_single_price - 10
                             * @hooked woocommerce_template_single_excerpt - 20
                             * @hooked woocommerce_template_single_add_to_cart - 30
                             * @hooked woocommerce_template_single_meta - 40
                             * @hooked woocommerce_template_single_sharing - 50
                             * @hooked WC_Structured_Data::generate_product_data() - 60
                             */
                            do_action('woocommerce_single_product_summary');
                            ?>
                        </div>
                    </div>
                </div>
            </div><!-- .summary -->

            <?php
            /**
             * woocommerce_after_single_product_summary hook.
             *
             * @hooked woocommerce_output_product_data_tabs - 10
             * @hooked woocommerce_upsell_display - 15
             * @hooked woocommerce_output_related_products - 20
             */
            do_action('woocommerce_after_single_product_summary');
            ?>

        </div><!-- #product-<?php the_ID(); ?> -->

    </div>
</section>

<?php do_action('woocommerce_after_single_product'); ?>
