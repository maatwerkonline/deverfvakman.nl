<?php
/**
 * Pay for order form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-pay.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see      https://docs.woocommerce.com/document/template-structure/
 * @author   WooThemes
 * @package  WooCommerce/Templates
 * @version  2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<form id="order_review" class="row" method="post">

	<div class="col-sm-1 col-md-3 col-lg-3 col-xl-4"></div>

	<div class="col-sm-10 col-md-6 col-lg-6 col-xl-4">
		<div class="card shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
			<div class="card-header text-center">
				<h1 class="h4 text-normal mb-0"><?php _e('Your order', 'woocommerce'); ?></h1>
			</div>
			<div class="card-body">
				<?php if ( sizeof( $order->get_items() ) > 0 ) : ?>
					<div class="d-none">
						<?php foreach ( $order->get_items() as $item_id => $item ) : ?>
							<?php
								if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
									continue;
								}
							?>
							<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
								<td class="product-name">
									<?php
										echo apply_filters( 'woocommerce_order_item_name', esc_html( $item->get_name() ), $item, false );

										do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

										wc_display_item_meta( $item );

										do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
									?>
								</td>
								<td class="product-quantity"><?php echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', esc_html( $item->get_quantity() ) ) . '</strong>', $item ); ?></td>
								<td class="product-subtotal"><?php echo $order->get_formatted_line_subtotal( $item ); ?></td>
							</tr>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if ( $totals = $order->get_order_item_totals() ) : ?>
					<table class="shop_table woocommerce-checkout-review-order-table table mb-0">
						<tfoot>
							<?php foreach ( $totals as $total ) : ?>
								<tr>
									<th class="product-total-label strong"><?php echo $total['label']; ?></th>
									<td class="product-total"><?php echo $total['value']; ?></th>
								</li>
							<?php endforeach; ?>
						</tfoot>
					</table>
				<?php endif; ?>
			</div>

			<div id="payment" class="card-footer">
				<?php if ( $order->needs_payment() ) : ?>
					<ul class="wc_payment_methods payment_methods methods">
						<?php
							if ( ! empty( $available_gateways ) ) {
								foreach ( $available_gateways as $gateway ) {
									wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
								}
							} else {
								echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', __( 'Sorry, it seems that there are no available payment methods for your location. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) ) . '</li>';
							}
						?>
					</ul>
				<?php endif; ?>
				<div class="form-row">
					<input type="hidden" name="woocommerce_pay" value="1" />

					<?php wc_get_template( 'checkout/terms.php' ); ?>

					<?php do_action( 'woocommerce_pay_order_before_submit' ); ?>

					<?php echo apply_filters( 'woocommerce_pay_order_button_html', '<input type="submit" class="btn btn-lg btn-block btn-primary button alt mt-4" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>

					<?php do_action( 'woocommerce_pay_order_after_submit' ); ?>

					<?php wp_nonce_field( 'woocommerce-pay' ); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-1 col-md-3 col-lg-3 col-xl-4"></div>
	
</form>
