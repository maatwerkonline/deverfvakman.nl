<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="woocommerce-order row">

	<div class="col-sm-1 col-md-12 order-md-2"></div>

	<div class="col-sm-10 col-md-5 col-xl-5 order-md-1">

		<?php if ( $order ) : ?>

			<?php if ( $order->has_status( 'failed' ) ) : ?>

				

			<?php else : ?>

				<div class="card">
					<div class="card-header">
						<h1 class="h4 text-normal mb-0 woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></h1>
					</div>

					<div class="card-body">
						<table class="shop_table woocommerce-order-overview woocommerce-thankyou-order-details order_details table mb-0">
							<tfoot>

								<tr class="woocommerce-order-overview__order order">
									<th class="strong"><?php _e( 'Order number:', 'maatwerkonline' ); ?></th>
									<td><?php echo $order->get_order_number(); ?></td>
								</tr>

								<?php if (in_array('woo-postnl/woocommerce-postnl.php', get_option('active_plugins')) || is_plugin_active_for_network('woo-postnl/woocommerce-postnl.php')) : ?>
									<tr class="woocommerce-order-overview__delivery date">
										<th class="strong"><?php _e( 'Delivery date:', 'maatwerkonline' ); ?></th>
										<td><?php echo WooCommerce_PostNL()->admin->show_order_delivery_options($order); ?></td>
									</tr>
								<?php endif; ?>

								<tr class="woocommerce-order-overview__date date">
									<th class="strong"><?php _e( 'Order date:', 'maatwerkonline' ); ?></th>
									<td><?php echo wc_format_datetime( $order->get_date_created() ); ?></td>
								</tr>

								<tr class="woocommerce-order-overview__total total">
									<th class="strong"><?php _e( 'Total:', 'maatwerkonline' ); ?></th>
									<td><?php echo $order->get_formatted_order_total(); ?></td>
								</tr>

								<?php if ( $order->get_payment_method_title() ) : ?>
									<tr class="woocommerce-order-overview__payment-method method">
										<th class="strong"><?php _e( 'Payment method:', 'maatwerkonline' ); ?></th>
										<td><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></td>
									</tr>
								<?php endif; ?>

							</tfoot>
						</table>
					</div>

					<div class="card-footer">
						<?php if ( is_user_logged_in() ) : ?>
							<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="btn btn-primary btn-block"><?php _e( 'My account', 'woocommerce' ); ?></a>
						<?php endif; ?>
					</div>
				</div>


			<?php endif; ?>

			<?php //do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>

		<?php else : ?>

			<h2 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></h2>

		<?php endif; ?>

	</div>

	<!--<div class="col-1"></div>-->

	<div class="col-md-6 col-xl-7">
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
	</div>

</div>
