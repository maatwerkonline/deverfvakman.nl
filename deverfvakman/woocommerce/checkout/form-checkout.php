<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}

wc_print_notices();
?>

<div class="row">
    <div id="before-checkout" class="col-12">

        <?php
        do_action('woocommerce_before_checkout_form', $checkout);

        // If checkout registration is disabled and not logged in, the user cannot checkout
        if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
            echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
            return;
        }
        ?>

    </div>
</div>
<form id="checkout" name="checkout" method="post" class="row checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

    <div class="col-md-8 mt-3" id="customer_details">
        <div class="mb-5 d-md-flex justify-content-between">
            <h1 class="h5 text-normal fw-600"><?php _e('Delivery', 'maatwerkonline'); ?></h1>
        </div>
        <?php if ($checkout->get_checkout_fields()) : ?>

            <?php do_action('woocommerce_checkout_before_customer_details'); ?>

            <div id="woocommerce-payment-process" role="tablist" aria-multiselectable="true">

                <?php do_action('woocommerce_checkout_billing'); ?>

                <?php do_action('woocommerce_checkout_shipping'); ?>

            </div>

            <?php do_action('woocommerce_checkout_after_customer_details'); ?>

        <?php endif; ?>

    </div>

    <div class="col-md-4">
        <div class="sticky-class">
            <div class="cart-step mb-5">
                <nav class="form-steps">
                    <div class="form-steps-item">
                        <div class="form-steps-item-content form-steps-item-active">
                            <span class="form-steps-item-text"><?php _e('Cart', 'maatwerkonline'); ?></span>
                        </div>
                    </div>
                    <div class="form-steps-item form-steps-item-active">
                        <div class="form-steps-item-content">
                            <span class="form-steps-item-line"></span>
                            <span class="form-steps-item-text"><?php _e('Delivery', 'maatwerkonline'); ?></span>
                        </div>
                    </div>
                    <div class="form-steps-item">
                        <div class="form-steps-item-content">
                            <span class="form-steps-item-line"></span>
                            <span class="form-steps-item-text"><?php _e('Payment', 'maatwerkonline'); ?></span>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="your-order">
                <h2 class="h5 text-normal"><?php _e('Checkout', 'woocommerce'); ?></h2>
                <?php wc_get_template('checkout/review-order.php'); ?>

                <?php do_action('woocommerce_checkout_before_order_review'); ?>

                <div id="order_review" class="woocommerce-checkout-review-order">
                    <?php do_action('woocommerce_checkout_order_review'); ?>
                </div>

                <?php do_action('woocommerce_checkout_after_order_review'); ?>
            </div>
        </div>
    </div>

</form>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
