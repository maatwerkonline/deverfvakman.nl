<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="woocommerce-steps woocommerce-shipping-fields">
	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

		<h3 class="h5 text-normal" data-toggle="collapse" data-parent="#woocommerce-payment-process" href="#shipping" aria-expanded="false" aria-controls="shipping"><?php _e( 'Do you want to ship to a different address?', 'maatwerkonline' ); ?></h3>

		<p id="ship-to-different-address">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox custom-control custom-checkbox">
				<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox custom-control-input" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" />
				<span class="custom-control-indicator"></span>
				<span class="custom-control-description"><?php _e( 'Yes, please ship to a different address then my details above', 'maatwerkonline' ); ?></span>
			</label>
		</p>
		
		<div class="collapse shipping_address" id="shipping" data-parent="#woocommerce-payment-process" aria-expanded="false">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>
			
			<div class="woocommerce-shipping-fields__field-wrapper row">
				<?php
					$fields = $checkout->get_checkout_fields( 'shipping' );

					foreach ( $fields as $key => $field ) {
						if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
							$field['country'] = $checkout->get_value( $field['country_field'] );
						}
						woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
					}
				?>
			</div>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>
			
		</div>

	<?php endif; ?>
</div>

<div class="woocommerce-steps woocommerce-additional-fields">

	<h2 class="h5 text-normal" data-toggle="collapse" data-parent="#woocommerce-payment-process" href="#additional" aria-expanded="false" aria-controls="additional"><?php _e( 'Any additional information?', 'maatwerkonline' ); ?></h2>

	<div class="collapse" id="additional" data-parent="#woocommerce-payment-process" aria-expanded="false">

		<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

			<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

				<h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

			<?php endif; ?>

			<div class="woocommerce-additional-fields__field-wrapper row">
				<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
				<?php endforeach; ?>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>

	</div>
</div>
