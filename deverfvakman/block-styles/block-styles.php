<?php
/**
 * Register Custom Block Styles
 */
if ( function_exists( 'register_block_style' ) ) {
    function block_styles_register_block_styles() {

        wp_register_style('block-styles-stylesheet', TEMPPATH . '/block-styles/block-styles-stylesheet.css');

        register_block_style(
            'core/heading',
            array(
                'name'         => 'orange-header',
                'label'        => 'Orange header',
                'style_handle' => 'block-styles-stylesheet',
            )
        );

    }

    add_action( 'init', 'block_styles_register_block_styles' );
}
?>