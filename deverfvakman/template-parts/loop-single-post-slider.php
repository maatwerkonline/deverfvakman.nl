<?php
if(empty($post_classes)){
    $post_classes = array();
}
$class_array = array_merge_recursive($post_classes, get_post_class());
$post_class_out = 'class="'. join( ' ', $class_array ) .'"'; 
ob_start();?>

<div <?php echo $post_class_out;?> >
    <a href="<?php echo get_the_permalink();?>" class="shadow rounded">
        <div class="image-holder mb-0">
            <?php echo wp_get_attachment_image( get_post_thumbnail_id($post->ID), array(15,10), '', array('class' => 'mb-0 image-crop', 'sizes' => array(150,100)) );?>
            <div class="post-author mb-0 text-white small"><span class="author"><img src="<?php echo get_avatar_url(get_the_author_meta( 'ID' ), ['size' => '30']);?>" class="d-inline avatar mr-2"> <?php echo get_author_name();?></span></div>
        </div>
        <div class="text-container p-4">
        <?php  $categories = wp_get_post_categories( $post->ID );
        if($categories) {?>
            <div class="categories d-none">
                <?php 
            foreach($categories as $category){
                $cat = get_category( $category );
                $cat_id = get_cat_ID( $cat->name );?>
                <span class="badge badge-pill badge-primary small mr-1"><?php echo $cat->name?></span>
            <?php } ?>
            </div>
        <?php }?>
        <h3 class="h4 mb-0 d-inline">
        <?php echo get_the_title();?>
        </h3>
        <span class="new-post-indicator badge badge-pill badge-primary small ml-2 align-top"><?php _e('New', 'maatwerkonline');?></span>
        <div class="post-meta text-muted small"><?php echo human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) );  _e( 'ago', 'maatwerkonline' ); echo  do_shortcode('[rt_reading_time label="· Leestijd:" postfix="minuten" postfix_singular="minuut"]') ?> 
        </div>
        <p class="mb-0">
        <?php echo $content;?>
        </p>
        </div>
    </a>
</div>
<?php
    $return .= ob_get_contents();
    ob_get_clean();
    