<?php
/**
 * Block Name: Logo carousel
 * This is the template that displays the block logo-carousel.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }
?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --><!-- Here you can add your own code --></div> */ ?>

<section class="pl-0 pr-0">
    <div class="container" style="width:1180">
        <div class="row" style="padding-top: 20px; padding-bottom:20px;">
            <div class="col-md-12 col-12">
                <div class="logos-list carousel-element mt-5 mb-5">
                    <?php
                    if(have_rows('carousel_logos')):
                        while(have_rows('carousel_logos')): the_row();

                        $logo_image = get_sub_field('logo_afbeelding');
                        $logo_link = get_sub_field('logo_link');
                        ?>
                            <div style="width: 230px;">
                                <div class="logo justify-content-center d-flex">
                                    <?php if ($logo_link): ?>
                                        <a href="<?php echo $logo_link['url'] ?>" class="align-self-center" target="<?php echo $logo_link['target'] ?>">
                                            <img src="<?php echo $logo_image ?>" class="mb-0" alt="">
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
