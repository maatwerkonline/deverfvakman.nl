<?php
/**
 * Block Name: Uitgelichte producten
 * This is the template that displays the block uitgelichte-producten.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }

 $categories_name = get_field('categorieen_titel');
 $categories_links = get_field('categorieen_links');
 $categories_link_bottom = get_field('categorieen_link_onder');

 $products_title = get_field('producten_titel');
 $products = get_field('producten');
 ?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --> <!-- Here you can add your own code --> </div> */ ?>

<section class="mobile-categories">
    <div class="container" style="width: 1180;">
        <div class="row pt-5">
            <div class="col-md-3 col-12">
                <?php if ($categories_name): ?>
                    <h3 class="mb-4">
                        <p>
                            <strong>
                                <?php echo $categories_name ?>
                            </strong>
                        </p>
                    </h3>
                <?php endif; ?>
                <?php if(have_rows('categorieen_links')):?>
                    <ul class="product-categories pl-0">
                        <?php while(have_rows('categorieen_links')): the_row();?>
                            <?php
                            $url = get_sub_field('categorie_link')['url'];
                            $target = get_sub_field('categorie_link')['target'];
                            $title = get_sub_field('categorie_link')['title'];
                            ?>
                            <li>
                                <a href="<?php echo $url ?>" <?php echo $target ?>>
                                    <?php echo $title ?>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                <?php if ($categories_link_bottom): ?>
                    <a class="btn btn-link btn-sharp  w-100 all-categories" href="<?php echo $categories_link_bottom['url'] ?>" <?php echo $$categories_link_bottom['target'] ?>>
                        <?php echo $categories_link_bottom['title'] ?>
                        <i class="dvv dvv-arrow-right right"></i>
                    </a>
                <?php endif; ?>
            </div>
            <div class="col-md-9 col-12">
                <?php if ($products_title): ?>
                    <h3 class="mb-4">
                        <p>
                            <strong>
                                <?php echo $products_title ?>
                            </strong>
                        </p>
                    </h3>
                <?php
                endif;
                if ($products):
                    ?>
                    <div class="woocommerce columns-4">
                        <div class="products list-unstyled row">
                            <?php
                            if (strpos($products, ',') == true) {
                        		$products = array_map('trim',explode(",",$products));
                        	}
                        	$products = new WP_Query(array('orderby' => 'post__in', 'post__in' => $products,'post_type' => 'product' ));
                            while($products->have_posts()) : $products->the_post(); ?>
                				<?php wc_get_template_part( 'content', 'product' ); ?>
                			<?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
