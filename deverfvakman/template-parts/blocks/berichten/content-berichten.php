<?php
/**
 * Block Name: berichten
 * This is the template that displays the block berichten.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }

 $posts_image = get_field('berichten_afbeelding');
 $posts_amount = get_field('berichten_hoeveelheid');
 ?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --> <!-- Here you can add your own code --> </div> */ ?>

<section class="pb-2" style="<?php if ($posts_image): ?>background-image: url('<?php echo $posts_image ?>'); <?php endif; ?> background-repeat:no-repeat; background-size:cover; background-position:center center;">
    <div class="container" style="width: 1180;">
        <div class="row">
            <div class="col-12 pb-0" style="padding-bottom:50px;">
                <div class="posts-section">
                    <div class="posts row">
                        <?php $products = new WP_Query(array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $posts_amount,
                            'orderby' => 'menu_order',
                            'order' => 'ASC'
                        ));
                        while($products->have_posts()) : $products->the_post(); ?>
                            <?php
                            $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
                            ?>
                            <div class="post col-12 col-sm-6 col-md-4 col-lg-4 post-26709 post type-post status-publish format-standard has-post-thumbnail hentry category-verfadvies">
                                <a href="<?php echo get_permalink() ?>" class="row py-1 py-md-3">
                                    <div class="col-12 d-flex">
                                        <div class="image-holder mb-0 rounded w-100">
                                            <img width="729" height="313" class="attachment- size- wvs-attachment-image" src="<?php echo get_the_post_thumbnail_url() ?>" sizes="(max-width: 729px) 100vw, 729px" alt="<?php echo get_the_title() ?>" sizes="(max-width: 729px) 100vw, 729px">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex">
                                        <div class="text-container">
                                            <h3 class="h5 mb-3 mt-2 bericht-titel">
                                                <strong>
                                                    <?php echo get_the_title() ?>
                                                </strong>
                                            </h3>
                                            <p class="mb-0 d-md-block bericht-content">
                                                <?php echo get_the_excerpt() ?>
                                            </p>
                                            <p class="btn-primary btn float-sm-right mt-4 waves-effect waves-light">
                                                Lees meer
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
