<?php
/**
 * Block Name: Banner
 * This is the template that displays the block banner.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }

$banner_image = get_field('banner_afbeelding');
$banner_title = get_field('banner_titel');
$banner_tekst = get_field('banner_tekst');
$banner_buttons = get_field('banner_buttons');
$banner_buttons_afbeelding = get_field('banner_buttons_afbeelding');
?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --></div>  */ ?>

<section style="<?php if ($banner_image): ?>background-image: url('<?php echo $banner_image ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; <?php endif; ?>">
    <div class="container">
        <div class="row text-center" style="padding-top: 75px; padding-bottom: 75px;">
            <div class="col-12">
                <?php if($banner_title): ?>
                    <h1 class="text-white mb-0">
                        <?php echo $banner_title ?>
                    </h1>
                <?php endif; ?>

                <?php if($banner_tekst): ?>
                    <div class="text-white h2 mb-0">
                        <p>
                            <?php echo $banner_tekst ?>
                        </p>
                    </div>
                <?php endif; ?>

                <?php if ($banner_buttons): ?>

                    <?php if ($banner_buttons['button1_link']): ?>

                        <?php $button = $banner_buttons['button1_link']; ?>

                        <a class="btn btn-primary btn-lg btn-sharp banner-btn waves-effect waves-light"
                            <?php if ($button['url']): ?>
                                href="<?php echo $button['url'] ?>"
                            <?php else: ?>
                                href="#"
                            <?php endif; ?>

                            <?php if ($button['target']): ?>
                                target="<?php echo $button['target'] ?>"
                            <?php endif; ?>
                        >

                        <?php if ($button['title']): ?>
                            <?php echo $button['title'] ?>
                        <?php endif; ?>

                    </a>

                    <?php endif; ?>

                    <?php if ($banner_buttons['button2_link']): ?>

                        <?php $button = $banner_buttons['button2_link']; ?>

                        <a style class="btn btn-primary btn-lg btn-sharp banner-btn waves-effect waves-light"
                            <?php if ($button['url']): ?>
                                href="<?php echo $button['url'] ?>"
                            <?php else: ?>
                                href="#"
                            <?php endif; ?>

                            <?php if ($button['target']): ?>
                                target="<?php echo $button['target'] ?>"
                            <?php endif; ?>
                        >

                            <?php if ($button['title']): ?>
                                <?php echo $button['title'] ?>
                            <?php endif; ?>

                        </a>

                    <?php endif; ?>

                    <?php if ($banner_buttons['button3_link']): ?>

                        <?php $button = $banner_buttons['button3_link']; ?>

                        <a style class="btn btn-secondary btn-lg btn-sharp  banner-btn waves-effect waves-light"
                            <?php if ($button['url']): ?>
                                href="<?php echo $button['url'] ?>"
                            <?php else: ?>
                                href="#"
                            <?php endif; ?>

                            <?php if ($button['target']): ?>
                                target="<?php echo $button['target'] ?>"
                            <?php endif; ?>
                        >

                            <?php if ($button['title']): ?>
                                <?php echo $button['title'] ?>
                            <?php endif; ?>

                        </a>

                    <?php endif; ?>

                <?php endif; ?>

                <?php if ($banner_buttons_afbeelding): ?>

                    <figure class="image mt-2 d-none d-xl-block waar-wil-je-verven">

                        <img src="<?php echo $banner_buttons_afbeelding ?>">

                    </figure>

                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
