<?php
/**
 * Block Name: USP's
 * This is the template that displays the block usps.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }

 $usp1 = get_field('usp1');
 $usp2 = get_field('usp2');
 ?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --><!-- Here you can add your own code --></div> */ ?>

<section class="container" style="width: 1180;">
    <div class="row m-0 home-usps">
        <?php if ($usp1['usp_afbeelding']): ?>
            <div class="col-md-1 col-lg-1 col-3">
                <figure class="image country mb-0">
                    <img src="<?php echo $usp1['usp_afbeelding'] ?>" alt="">
                </figure>
            </div>
        <?php endif; ?>
        <?php if($usp1['usp_titel'] || $usp1['usp_tekst']): ?>
            <div class="col-md-4 col-lg-3 col-9">
                <?php if ($usp1['usp_titel']): ?>
                    <h4 class="mt-5 mt-md-0 mt-lg-0">
                        <p>
                            <strong>
                                <?php echo $usp1['usp_titel'] ?>
                            </strong>
                        </p>
                    </h4>
                <?php endif; ?>
                <?php if ($usp1['usp_tekst']): ?>
                    <div class="mb-3 mb-md-0 mb-lg-0">
                        <p>
                            <?php echo $usp1['usp_tekst'] ?>
                        </p>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if ($usp1['usp_link']): ?>
            <div class="col-md-1 col-lg-2 col-12 my-auto pl-lg-0">
                <a class="btn btn-primary btn-sharp d-none d-lg-block" href="<?php echo $usp1['usp_link']['url'] ?>" <?php echo $usp1['usp_link']['target'] ?>>
                    <?php echo $usp1['usp_link']['title'] ?>
                </a>
            </div>
        <?php endif; ?>

        <?php if ($usp2['usp_afbeelding']): ?>
            <div class="col-md-1 col-lg-1 col-3">
                <figure class="image country mb-0">
                    <img src="<?php echo $usp2['usp_afbeelding'] ?>" alt="">
                </figure>
            </div>
        <?php endif; ?>
        <?php if($usp2['usp_titel'] || $usp2['usp_tekst']): ?>
            <div class="col-md-4 col-lg-3 col-9">
                <?php if ($usp2['usp_titel']): ?>
                    <h4 class="mt-5 mt-md-0 mt-lg-0">
                        <p>
                            <strong>
                                <?php echo $usp2['usp_titel'] ?>
                            </strong>
                        </p>
                    </h4>
                <?php endif; ?>
                <?php if ($usp2['usp_tekst']): ?>
                    <div class="mb-3 mb-md-0 mb-lg-0">
                        <p>
                            <?php echo $usp2['usp_tekst'] ?>
                        </p>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if ($usp2['usp_link']): ?>
            <div class="col-md-1 col-lg-2 col-12 my-auto pl-lg-0">
                <a class="btn btn-primary btn-sharp d-none d-lg-block" href="<?php echo $usp1['usp_link']['url'] ?>" <?php echo $usp1['usp_link']['target'] ?>>
                    <?php echo $usp2['usp_link']['title'] ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
</section>
