<?php
/**
 * Block Name: Accordion
 * This is the template that displays the block accordion.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }
 ?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --> <!-- Here you can add your own code --> </div> */ ?>
<?php if (have_rows('accordion_items')): ?>
    <div style="" id="accordion" class="home-accordion  " role="tablist" aria-multiselectable="true">
        <?php
        //Get unqiue block id to assign to panel
		$block_id = $block['id'];
		$i = 0;
        ?>
        <?php while(have_rows("accordion_items")): the_row(); ?>
			<?php 
			$i++; 
			$item_id = $block_id . '-' . $i;
			?>
			<div class="accordion-card pt-3 pb-3">
				<div role="tab" id="heading-<?php echo $item_id ?>">
					<a class="text-uppercase accordion-card-header d-block collapsed" data-toggle="collapse" data-parent="#accordion" href="#" data-target="#collapse-<?php echo $item_id?>" aria-expanded="false" aria-controls="collapse-<?php echo $item_id?>">
						<strong><?php echo get_sub_field('accordion_item_header'); ?></strong>
						<span class="dvv dvv-chevron-down"> </span>
					</a>
				</div>
				<div id="collapse-<?php echo $item_id?>" class="collapse 0 == <?php echo $item_id ?>" role="tabpanel" aria-labelledby="heading-1">
					<div class="card-block">
						<p><?php echo get_sub_field('accordion_item_content'); ?></p>
					</div>
				</div>
			</div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
