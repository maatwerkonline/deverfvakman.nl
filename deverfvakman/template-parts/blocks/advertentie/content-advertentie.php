<?php
/**
 * Block Name: Advertentie
 * This is the template that displays the block advertentie.
 *
 * @author Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

 if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
 }
 if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
 }
 if( !empty($block['align_text']) ) {
	$className .= ' align-text-' . $block['align_text'];
 }
 if( !empty($block['align_content']) ) {
	$className .= ' is-position-' . $block['align_content'];
 }

 $ad_image = get_field('advertentie_afbeelding');
 $ad_link = get_field('advertentie_link');
 ?>

<?php /* <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"> <!-- You may change this element to another element (for example blockquote for quotes) --><!-- Here you can add your own code --></div> */ ?>

<div class="pl-0 pr-0">
    <div class="container" style="width: 1180">
        <div class="row">
            <div class="col-12">
                <figure class="image">
                    <?php if($ad_link): ?>
                        <a href="<?php echo $ad_link['url'] ?>" target="<?php if($ad_link['target']): echo $ad_link['target']; endif; ?>">
                            <img src="<?php echo $ad_image ?>" alt="">
                        </a>
                    <?php else: ?>
                        <img src="<?php echo $ad_image ?>" alt="">
                    <?php endif; ?>
                </figure>
            </div>
        </div>
    </div>
</div>
