<header id="header">
    
    <nav class="navbar navbar-light" role="navigation">
        <div class="container">

            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <?php $logo = get_theme_mod( 'logo' );
                if( $logo!=''): ?>
                    <?php if( $logo!='' ): ?>
                        <img src="<?php echo esc_attr($logo); ?>" data-no-lazy="1" alt="<?php echo esc_attr(get_bloginfo('name', 'display'));?>" class="mb-0">
                    <?php endif; ?>
                <?php else: ?>
                    <p class="site-title mb-0 h6"><?php bloginfo( 'name' ); ?></p>
                    <?php $description = get_bloginfo( 'description', 'display' );
                    if ( $description || is_customize_preview() ) : ?>
                        <p class="site-description mb-0"><small><?php echo $description; ?></small></p>
                    <?php endif; ?>
                <?php endif; ?>
            </a>

            <?php wp_nav_menu( array( 'menu' => 'topbar', 'theme_location' => 'topbar', 'container_id' => 'default-topbar', 'container_class' => 'ml-auto', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu()) ); ?>
            <?php get_search_form(); ?>

        </div>
    </nav>

    <nav class="navbar navbar-dark bg-primary" role="navigation">
        <div class="container">

            <div id="menu-toggle" class="d-block d-lg-none <?php if(is_page('offline')){ ?> inactive <?php }; ?>">
                <div class="hamburger">
                    <div class="hamburger-box">
                        <?php if(is_page('offline')){ ?>
                            <i class="dvv dvv-undo2"></i>
                        <?php } else { ?>
                            <div class="hamburger-inner"></div>
                        <?php }; ?>
                    </div>
                </div>
                <span class="mobile-menu-label"><?php if(is_page('offline')){ _e('Reload', 'maatwerkonline'); } else { _e('Menu', 'maatwerkonline'); }; ?></span>
            </div>

            <?php wp_nav_menu( array( 'menu' => 'default', 'theme_location' => 'default', 'container_id' => 'default-menu', 'container_class' => 'd-none d-lg-block', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu()) ); ?>
        
        </div>
    </nav>

    <div class="collapse navbar-collapse" id="navbarNav">
        <div class="container">
            <?php wp_nav_menu( array( 'menu' => 'mobile', 'theme_location' => 'mobile', 'container_id' => 'mobile-menu', 'container_class' => 'd-xl-none ml-auto', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu()) ); ?>
        </div>
    </div>
    
</header>