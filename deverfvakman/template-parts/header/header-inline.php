<header id="header">
	
	<nav class="navbar navbar-light" role="navigation">
		<div class="container">
			
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php $logo = get_theme_mod( 'logo' );
				$icon = get_site_icon_url();
				if( $logo!=''): ?>
					<?php if( $logo!='' ): ?>
						<img src="<?php echo esc_attr($logo); ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display'));?>" class="mb-0">
					<?php endif; ?>
				<?php else: ?>
					<p class="site-title mb-0 h6"><?php bloginfo( 'name' ); ?></p>
					<?php $description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description mb-0"><small><?php echo $description; ?></small></p>
					<?php endif; ?>
				<?php endif; ?>
			</a>

			<?php  wp_nav_menu( array(
    'theme_location'  => 'primary',
    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
    'container'       => 'div',
    'container_class' => 'collapse navbar-collapse',
    'container_id'    => 'bs-example-navbar-collapse-1',
    'menu_class'      => 'navbar-nav mr-auto',
    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
    'walker'          => new WP_Bootstrap_Navwalker(),
) ); ?>
			
			<div id="menu-toggle" class="d-block d-lg-none <?php if(is_page('offline')){ ?> inactive <?php }; ?>">
				<div class="hamburger">
					<div class="hamburger-box">
						<?php if(is_page('offline')){ ?>
							<i class="dvv dvv-undo2"></i>
						<?php } else { ?>
							<div class="hamburger-inner"></div>
						<?php }; ?>
					</div>
				</div>
				<span class="mobile-menu-label"><?php if(is_page('offline')){ _e('Reload', 'maatwerkonline'); } else { _e('Menu', 'maatwerkonline'); }; ?></span>
			</div>

		</div>
	</nav>
	
</header>