<?php
if(empty($post_classes)){
    $post_classes = array();
}
$class_array = array_merge_recursive($post_classes, get_post_class());
$post_class_out = 'class="'. join( ' ', $class_array ) .'"';   
 
$return .= '<div '.$post_class_out.'>';
    $return .= '<a href="'.get_the_permalink().'" class="row py-1 py-md-3">';
        $return .= '<div class="col-12 d-flex align-items-center">';
            $return .= '<div class="image-holder mb-0 rounded w-100">';
                $return .= wp_get_attachment_image( get_post_thumbnail_id($post->ID), '', array('class' => 'mb-0 image-crop w-100', 'sizes' => '') );
            $return .= '</div>';
        $return .= '</div>';
        $return .= '<div class="col-12 d-flex align-items-center">';
            $return .= '<div class="text-container">';
                $return .= '<h3 class="h5 mb-3 mt-2" data-mh="post-title">';
                    $return .='<strong>'.get_the_title().'</strong>';
                $return .= '</h3>';
                $return .= '<p class="mb-0 d-md-block" data-mh="post-content">';
                    $return .= $content;
                $return .= '</p>';
                $return .= '<p class="btn-primary btn float-sm-right mt-4">';
                    $return .= __( 'Read more', 'maatwerkonline' );
                $return .= '</p>';
            $return .= '</div>';
        $return .= '</div>';
    $return .= '</a>';
$return .= '</div>';
