<?php
get_header();
$post_id = get_theme_mod( 'error_404_page' );
$post = get_post($post_id);
?>

<section>
	<div class="container ">
		<div class="row p-3">
			<div class="col-12">
				<?php echo get_the_title(); ?>
				<?php if ( function_exists('yoast_breadcrumb') ) {  /* If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs */
					yoast_breadcrumb( '<p class="mb-0" id="breadcrumbs">','</p>' );
				}; ?>
			</div>
		</div>
	</div>
</section>

<?php echo apply_filters( 'the_content', $post->post_content ); ?>

<?php get_footer();
