<?php
get_header();
?>

<?php if(!is_front_page() && !is_cart() && !is_checkout()) : ?>
	<section id="title-breadcrumbs" class="py-0">
		<div class="container ">
			<div class="row cut-off-image pt-4 pb-5 text-center">
				<div class="col-12">
					<h1 class="text-normal mb-0"><?php echo get_the_title(); ?></h1>	
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p class="mb-0" id="breadcrumbs">','</p>' );
					}; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
	<?php the_content();?>
<?php endwhile; endif;?>

<?php get_footer();
