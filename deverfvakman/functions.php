<?php
define('THEME_NAME', 'De Verfvakman');
define('THEME_VERSION', '1.0.0');
define('TEMPPATH', get_template_directory_uri());
define('IMAGES', TEMPPATH . "/images");

// Apply content width
if (!isset($content_width)) {
    $content_width = 1200;
}

// Apply theme's stylesheet to the visual editor
function add_editor_styles() {
    add_editor_style(get_stylesheet_uri());
}
add_action('init', 'add_editor_styles');

// Theme setup
function theme_setup() {

    // Load Theme Textdomain
    load_theme_textdomain('maatwerkonline', get_template_directory() . '/languages');

    // Add Theme Support
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('post-formats', array('quote', 'video'));
    add_theme_support('automatic-feed-links');

    // Add Image Size
    add_image_size('card', 318, 270, true, array('center', 'center'));

    // Block styles
    require_once __DIR__ . '/block-styles/block-styles.php';

    // Theme Options
    require_once __DIR__ . '/inc/admin/theme-options.php';

    // Register Sidebars
    require_once __DIR__ . '/inc/sidebars.php';

    // Add Pagination functionalities
    require_once __DIR__ . '/inc/class.pagination.php';

    // Add Gravity Forms functionalities
    if (is_plugin_active('gravityforms/gravityforms.php')) {
        require_once 'inc/class.gravityforms.php';
    }

    // Add WooCommerce functionalities
    if (is_plugin_active('woocommerce/woocommerce.php')) {
        require_once 'inc/class.woocommerce.php';
        require_once 'inc/apply-price-limit.php';
        if(!is_admin()):
            add_action('wp_loaded', 'apply_price_limit');
        endif;
    }

    // Widgets functionalities
    add_filter('widget_text', 'do_shortcode');

    // Menu Walker support
    require_once __DIR__ . '/inc/class.menu-walker.php';

    // Load Admin Scripts
    function admin_enqueue($hook) {
        
        wp_enqueue_script('additional_menu_fields', TEMPPATH . '/inc/admin/js/update_margin_fields.js');

        if ($hook == 'nav-menus.php') {
            wp_enqueue_script('additional_menu_fields', TEMPPATH . '/inc/admin/js/additional-fields-menu.js');
            wp_localize_script('additional_menu_fields', 'objectL10n', array(
                'menutype' => __('Menu Type', 'maatwerkonline'),
                'defaultmenu' => __('Default Menu', 'maatwerkonline'),
                'megamenusinglecolumn' => __('Mega Menu - 1 Column', 'maatwerkonline'),
                'megamenu2columns' => __('Mega Menu - 2 Columns', 'maatwerkonline'),
                'megamenu3columns' => __('Mega Menu - 3 Columns', 'maatwerkonline'),
                'megamenu4columns' => __('Mega Menu - 4 Columns', 'maatwerkonline'),
                'megamenu5columns' => __('Mega Menu - 5 Columns', 'maatwerkonline'),
                'megamenu6columns' => __('Mega Menu - 6 Columns', 'maatwerkonline'),
                'megamenu7columns' => __('Mega Menu - 7 Columns', 'maatwerkonline'),
                'createdropdown' => __('Create a dropdown menu on smaller screens', 'maatwerkonline'),
                'usedescription' => __('Use description field as HTML content', 'maatwerkonline'),
                'hidetitle' => __('Hide title', 'maatwerkonline')
            ));
        }
    }
    add_action('admin_enqueue_scripts', 'admin_enqueue');

    // Register Menus
    function register_menus() {
        register_nav_menus(array(
            'default' => __('Default Menu', 'maatwerkonline'),
            'topbar' => __('Topbar Menu', 'maatwerkonline'),
            'mobile' => __('Mobile Menu', 'maatwerkonline')
        ));
    }
    add_action('init', 'register_menus');
}
add_action('after_setup_theme', 'theme_setup');

// Enqueue stylesheets and scripts
function enqueue() {
    // Stylesheets
    // If you would like to use Google Font (https://fonts.google.com/) you can add the font-css here
    // wp_enqueue_style('font-css','https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,600italic,400italic,300italic,700italic');
    wp_enqueue_style('carousel-slick', TEMPPATH . '/css/carousel-slick.css');
    wp_enqueue_style('bootstrap', TEMPPATH . '/css/bootstrap.min.css');
    wp_enqueue_style('main', get_stylesheet_uri(), array('bootstrap'));

    //enqueue icons
    wp_enqueue_style('dvvicons', TEMPPATH . '/css/icons/dvv.css');

    // Disable Gutenberg Frontend Stylesheet
    //wp_dequeue_style('wp-block-library');

    // Scripts
    wp_enqueue_script('carousel-slick', TEMPPATH . '/js/carousel-slick.js', array(), '', true);
    wp_enqueue_script('popper', TEMPPATH . '/js/popper.min.js', array(), '', true);
    wp_enqueue_script('bootstrap', TEMPPATH . '/js/bootstrap.min.js', array(), '', true);
    wp_enqueue_script('scripts', TEMPPATH . '/js/scripts.min.js', array('jquery'), '', false);
    wp_enqueue_script('init', TEMPPATH . '/js/init.js', true);
    wp_localize_script('init', 'objectL10n', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'post_id' => get_the_ID()
    ));

    if ((!is_admin()) && is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'enqueue');

// Check if is Post Type
function is_post_type($type) {
    global $wp_query;

    if ($type == get_post_type($wp_query->post->ID))
        return true;
    return false;
}

// Check if is Blog
function is_blog() {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

// Disable srcset images
function wdo_disable_srcset( $sources ) {
    return false;
}
add_filter( 'wp_calculate_image_srcset', 'wdo_disable_srcset' );

// Add the wp-editor back into the Blog page after it was removed in 4.2.2.
function fix_no_editor_on_posts_page($post) {
    if (isset($post) && $post->ID != get_option('page_for_posts')) {
        return;
    }
    remove_action('edit_form_after_title', '_wp_posts_page_notice');
    add_post_type_support('page', 'editor');
}
add_action('edit_form_after_title', 'fix_no_editor_on_posts_page', 0);

// Format Number Abbreviation
function formatNumberAbbreviation($number) {
    $abbrevs = array(12 => "T", 9 => "B", 6 => "M", 3 => "K", 0 => "");
    foreach ($abbrevs as $exponent => $abbrev) {
        if ($number >= pow(10, $exponent)) {
            return intval($number / pow(10, $exponent)) . $abbrev;
        }
    }
}

// Allow shortlinks to be retrieved for pages and custom post types
function shortlinks_custom_post_type($shortlink, $id, $context) {
    $post_id = 0;

    if ('query' == $context && is_singular('press')) {
        $post_id = get_queried_object_id();
    } elseif ('post' == $context) {
        $post_id = $id;
    }
    if ('press' == get_post_type($post_id)) {
        $shortlink = home_url('?p=' . $post_id);
    }
    return $shortlink;
}
add_filter('pre_get_shortlink', 'shortlinks_custom_post_type', 10, 3);

// Custom Post Type Category Pagination Fix
function fix_category_pagination($qs) {
    if (isset($qs['category_name']) && isset($qs['paged'])) {
        $qs['post_type'] = get_post_types($args = array(
            'public' => true,
            '_builtin' => false
        ));
        array_push($qs['post_type'], 'post');
    }
    return $qs;
}
add_filter('request', 'fix_category_pagination');

// Load categories custom post types
function query_post_type($query) {
    if (is_category()) {
        $post_type = get_query_var('post_type');
        if ($post_type)
            $post_type = $post_type;
        else
            $post_type = array('nav_menu_item', 'post', 'knowledgebase'); // don't forget nav_menu_item to allow menus to work!
        $query->set('post_type', $post_type);
        return $query;
    }
}
add_filter('pre_get_posts', 'query_post_type');

// Bootstrap Breadcrumbs
function breadcrumbs_wrapper_class($class) {
    $class = 'breadcrumb';
    return $class;
}
add_filter('wpseo_breadcrumb_output_class', 'breadcrumbs_wrapper_class', 10, 1);

// Bootstrap Tag Markup
function wp_bootstrap_add_tag_class($taglinks) {
    $tags = explode('</a>', $taglinks);
    $regex = "#(.*tag-link[-])(.*)(' title.*)#e";

    foreach ($tags as $tag) {
        $tagn[] = preg_replace($regex, "('$1$2 label tag-'.get_tag($2)->slug.'$3')", $tag);
    }
    $taglinks = implode('</a>', $tagn);
    
    return $taglinks;
}
add_action('wp_tag_cloud', 'wp_bootstrap_add_tag_class');

function wp_bootstrap_wp_tag_cloud_filter($return, $args) {
    return '<div id="tag-cloud">' . $return . '</div>';
}
add_filter('wp_tag_cloud', 'wp_bootstrap_wp_tag_cloud_filter', 10, 2);

// Get Attachment ID by url
function wp_get_attachment_id($url) {
    $attachment_id = 0;
    $dir = wp_upload_dir();

    if (false !== strpos($url, $dir['baseurl'] . '/')) { // Is URL in uploads directory?
        $file = basename($url);
        $query_args = array(
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'fields' => 'ids',
            'meta_query' => array(
                array(
                    'value' => $file,
                    'compare' => 'LIKE',
                    'key' => '_wp_attachment_metadata',
                ),
            )
        );

        $query = new WP_Query($query_args);

        if ($query->have_posts()) {
            foreach ($query->posts as $post_id) {
                $meta = wp_get_attachment_metadata($post_id);
                $original_file = basename($meta['file']);
                $cropped_image_files = wp_list_pluck($meta['sizes'], 'file');

                if ($original_file === $file || in_array($file, $cropped_image_files)) {
                    $attachment_id = $post_id;
                    break;
                }
            }
        }
    }
    return $attachment_id;
}

// Get The Trimmed Excerpt (trimmed from shortcodes)
function get_the_trimmed_excerpt($charlength) {
    if (!$charlength) {
        $charlength = 55;
    }
    if ($charlength > 0) {
        $excerpt = do_shortcode(get_the_content());
        $excerpt = apply_filters('the_content', $excerpt);
        $excerpt = str_replace(']]>', ']]&gt;', $excerpt);
        $excerpt = strip_tags($excerpt);
        $count = preg_split("/[\n\r\t ]+/", $excerpt, $charlength + 1, PREG_SPLIT_NO_EMPTY);
        $ending = (count($count) > $charlength) ? '[...]' : '';
        $count = array_slice($count, 0, $charlength);
        $excerpt = implode(' ', $count) . $ending;
    } else {
        $excerpt = get_the_excerpt();
    }
    return $excerpt;
}

// Get strings between
function get_string_between($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);

    if ($ini == 0)
        return '';

    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;

    return substr($string, $ini, $len);
}

// Thumbnail upscale
function image_crop_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop) {
    if (!$crop)
        return null; // let the wordpress default function handle this

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);
    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);
    $s_x = floor(($orig_w - $crop_w) / 2);
    $s_y = floor(($orig_h - $crop_h) / 2);

    return array(0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h);
}
add_filter('image_resize_dimensions', 'image_crop_dimensions', 10, 6);

// Post Views
function get_post_views($post_ID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($post_ID, $count_key, true);

    if ($count == '') {
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
        return '0';
    }

    return $count;
}

function set_post_views($post_ID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($post_ID, $count_key, true);

    if ($count == '') {
        $count = 0;
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_ID, $count_key, $count);
    }
}

// Click Counts
function get_click_count($post_ID, $key) {
    $count_key = 'post_click_count_' . $key;
    $count = get_post_meta($post_ID, $count_key, true);

    if ($count == '') {
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
        return '0';
    }

    return $count;
}

function set_click_count($post_ID) {
    $count_key = 'post_click_count_' . $_POST['key'];
    $post_ID = $_POST['post_id'];
    $count = get_post_meta($post_ID, $count_key, true);

    if ($count == '') {
        $count = 0;
        delete_post_meta(get_the_ID(), $count_key);
        add_post_meta($post_ID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_ID, $count_key, $count);
    }

    echo $count;
    die();
}
add_action('wp_ajax_set_click_count', 'set_click_count');
add_action('wp_ajax_nopriv_set_click_count', 'set_click_count');

// Use All Youtube Parameters
function custom_oembed_result($html, $url, $args) {
    if ($args) {
        $newargs = $args;
        $parameters = http_build_query($newargs);
        $html = str_replace('<iframe', '<iframe class="embed-responsive-item"', $html);
        $html = str_replace('?feature=oembed', '?feature=oembed' . '&amp;' . $parameters, $html);
    }

    return $html;
}
add_filter('oembed_result', 'custom_oembed_result', 10, 3);

// Expanded allowed tags
function expanded_alowed_tags() {
    $my_allowed = wp_kses_allowed_html('post');

    // iframe
    $my_allowed['iframe'] = array(
        'src' => array(),
        'height' => array(),
        'width' => array(),
        'frameborder' => array(),
        'allowfullscreen' => array(),
    );

    // form fields - input
    $my_allowed['input'] = array(
        'class' => array(),
        'id' => array(),
        'name' => array(),
        'value' => array(),
        'type' => array(),
    );

    // select
    $my_allowed['select'] = array(
        'class' => array(),
        'id' => array(),
        'name' => array(),
        'value' => array(),
        'type' => array(),
    );

    // select options
    $my_allowed['option'] = array(
        'selected' => array(),
    );

    // style
    $my_allowed['style'] = array(
        'types' => array(),
    );

    return $my_allowed;
}

// By default don't link images
function imagelink_setup() {
    $image_set = get_option('image_default_link_type');
    
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'imagelink_setup', 10);

// Year Shortcode
function year_shortcode() {
    $year = date('Y');
    return $year;
}
add_shortcode('year', 'year_shortcode');

// Disable JC Submenu Custom Walker
function jc_disable_public_walker($default) {
    return false;
}
add_filter('jcs/enable_public_walker', 'jc_disable_public_walker');

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['shipping_address_2']);
    return $fields;
}

add_filter('robots_txt', 'addToRoboText');
function addToRoboText($robotext) {
    $additions =    "
                    # Added by filter in functions
                    Sitemap: https://www.deverfvakman.nl/sitemap_index.xml 
                    Disallow: /wp-includes/ 
                    Disallow: /winkelmand/ 
                    Disallow: /mijn-account/ 
                    Disallow: /?s= 
                    ";
    return $robotext . $additions;
}

// Add functionality to set the tax-status when creating a new product.
function new_product_tax_status_none() {
    global $post, $pagenow;

    // Only on new product pages
    if( $pagenow === 'post-new.php' ) : ?>
        <script>
            jQuery(function($){
                // On load set the tax status to none
                $('select[name="_tax_status"]').val('none');
            });
        </script>
    <?php endif;
}
add_action('woocommerce_product_options_tax', 'new_product_tax_status_none');

// Set the product price in the created JSON data as the lowest price when there is a quantity discount
function mo_change_price_schema($markup_offer){
    if ( is_product() && !is_admin() ) {
        global $product;

        $product_id=$product->get_id();
        $product_price = $product->get_price();
        $parent_product=$product->get_parent_id();

        if($product->get_type() == "variation"){
            $quantity_pricing = get_post_meta($parent_product, "o-discount", true);
        }else{
            $quantity_pricing = get_post_meta($product_id, "o-discount", true);
        }

        // If product has discount applied
        if($quantity_pricing['enable'] == 1){
            // Make an array of all prices with discount
            $step_prices = [];

            foreach ($quantity_pricing["rules"] as $rule) {
                if ($quantity_pricing["type"] == "fixed") {
                    $step_price = $product_price - $rule["discount"];
                } else if ($quantity_pricing["type"] == "percentage") {
                    $step_price = $product_price - ($product_price * $rule["discount"]) / 100;
                }
                array_push($step_prices, $step_price);
            }
            
            $min_price = min($step_prices);

            // Changing the schema data
            $markup_offer = array(
                '@type'                 => 'Offer',
                'price'                 => $min_price,
                'priceValidUntil'       => (date('y')+2).'-'.date('m').'-'.date('d'),
                'priceSpecification'    => array(
                    'price'                 => $min_price,
                    'priceCurrency'         => 'EUR',
                    'valueAddedTaxIncluded' => 'false',
                ),
                'priceCurrency'         => 'EUR',
                'availability'  => 'https://schema.org/' . ( $product->is_in_stock() ? 'InStock' : 'OutOfStock' ),
                'url'           => get_permalink( $product->get_id() ),
                'seller'        => array(
                    '@type' => 'Organization',
                    'name'  => 'De VerfVakman',
                    'url'   => 'https://www.deverfvakman.nl',
                ),
            );
            return $markup_offer;
        } else {
            // return unchanged
            return $markup_offer;
        }
    
    }
}
add_filter('woocommerce_structured_data_product_offer', 'mo_change_price_schema');

/**
 * Show prices without tax
 */
// function mo_price_tax_starting_value() {
//     if( is_user_logged_in() ) {
//         $user = wp_get_current_user();
//         $roles = ( array ) $user->roles;
//         foreach($roles as $role){
//             if($role == 'administrator' || $role == 'zzp'){
//                 echo "<div id='taxfree' value='true' hidden></div>";
//                 return true;
//             } else {
//                 echo "<div id='taxfree' value='false' hidden></div>";
//                 return false;
//             }
//         }
//     } else {
//         echo "<div id='taxfree' value='false' hidden></div>";
//         return false;
//     }
//     echo "<script> console.log('test_action'); </script>";
// }

//add_action('after_body_open_tag', 'mo_price_tax_starting_value');