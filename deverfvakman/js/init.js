jQuery(document).ready(function ($) {
    "use strict";

    // Make product images same height
    function matchHeightProducts() {
        var minHeight = 0;

        $(".product .image").each(function () {
            if ($(this).height() > minHeight) { minHeight = $(this).height(); }
        });

        $(".product .image").height(minHeight);
    }

    matchHeightProducts();

    $(document).ajaxStop(function () {
        setTimeout(function () {

            matchHeightProducts();

        }, 50);
    });


    // Make divs always same height
    setTimeout(function () {
        var minHeight = 0;

        $(".woocommerce-product-usps.adjust").each(function () {
            if ($(this).height() > minHeight) { minHeight = $(this).height(); }
        });

        $(".woocommerce-product-usps.adjust").height(minHeight);

    }, 50);

    // Search bar mobile
    /*
     $('#searchVisible').on('click', function () {
     $('#searchform').css('z-index', '99');
     $('#searchform').animate({
     opacity: 1
     });
     });
     */

    //Make sure that the woocommerce image column has the col-lg-7 class
    $(".woocommerce-product-gallery.images").addClass("col-lg-7").addClass("mb-5");

    // Search bar mobile toggle
    $('#searchVisible').click(function () {
        $('#searchform').toggle();
        $('#searchform').css('z-index', '99');
        $('#searchform').animate({
            opacity: 1
        });
    });
    $(window).resize(function () {
        if ($(this).width() > 992 && $('#searchform').css('display') == 'none') {
            $('#searchform').css('display', 'block');
            ;
        }
    });
    $('p').each(function () {
        var $this = $(this);
        if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
    });

    // Filters
    function filters() {
        $('#show-filter').on('click', function () {

            var filtersTop = ($('#show-filter').offset().top);

            $(this).toggleClass('active');
            $('#filters').toggleClass('show').css({ 'top': filtersTop });

        });

        $('.wcapf-layered-nav').on('click', function () {
            $('#show-filter').removeClass('active');
            $('#filters').removeClass('show').css({ 'top': '' });
        });
    }
    filters();

    function responsive_filters() {
        if ($(window).width() > 768) {
            $('#show-filter').removeClass('active');
            $('#filters').removeClass('show').css({ 'top': '' });
        }
    }
    function header_top_width() {
        if ($(".header-top").length > 0) {
            var headerWidth = $(".menu-holder").width();
            $(".header-top").width(headerWidth);
        }
    }
    header_top_width();
    $(window).resize(function () {
        responsive_filters();
        header_top_width();
    });

    // Init Popover
    $(function () {
        $('[data-toggle="popover"]').popover({
            boundary: 'viewport'
        });

        $('[data-toggle="popover"]').on("click", function (e) {
            e.preventDefault();
        });
    });

    // Init Tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    // Init Match Height
    function matchHeight() {
        $('.match-height').matchHeight();
        $('.match-height-by-row').matchHeight({ byRow: true });
        // $('.slick-slide').matchHeight({ byRow: true });
    };
    matchHeight();

    $(document).ajaxComplete(function ($) {
        matchHeight();
    });

    // Close Modal By Dragging
    $('.modal').each(function () {
        var $this = $(this);
        var $draggable = $this.draggabilly({// Use https://draggabilly.desandro.com/ script
            axis: 'y'
        });
        var draggie = $draggable.data('draggabilly');
        var firstPosition = '';
        var dragClose = '';

        if ($(this).outerHeight() > window.innerHeight) {
            $(this).addClass('modal-full');
        }

        $this.one('shown.bs.modal', function (e) {
            firstPosition = $(this).offset().top - $(window).scrollTop();
            dragClose = $(this).outerHeight() / 3;
        });

        $draggable.on('dragMove', function (event, pointer, moveVector) {
            if (draggie.position.y < firstPosition) { // If user tries to drag the modal to the top
                $draggable.draggabilly('disable'); // Disable dragging
                $draggable.on('pointerUp', function () { // On releasing mouse
                    $draggable.draggabilly('enable'); // Enable dragging again
                    $draggable.draggabilly('setPosition', 0, firstPosition); // And jump back to original position
                });
            }
            ;
        });

        $draggable.on('pointerUp', function () {
            if ((draggie.position.y - firstPosition) > dragClose) { // If is dragged more then 50 pixels
                $this.find('.close').click(); // Close modal
            } else {
                $draggable.draggabilly('setPosition', 0, firstPosition); // Otherwise jump back to original position
            }
        });

        $this.on('hidden.bs.modal', function (e) {
            $draggable.draggabilly('setPosition', 0, firstPosition); // When Modal is closed, set position back to original position
        });
    });

    // Count clicks on [data-click-count] buttons
    $('[data-click-count]').on('click', function () {

        var count_key = $(this).attr("data-click-count");

        $.ajax({
            type: 'post',
            url: objectL10n.ajaxurl,
            data: {
                action: 'set_click_count',
                post_id: objectL10n.post_id,
                key: count_key,
            }
        });

    });

    // Add loading class on form button click
    $('form button:not(#searchsubmit):not(#searchclose)').on('click', function () {
        $(this).addClass('loading');
    });

    // Remove validation message when click on list item with error
    $(document).bind('gform_post_render', function () {
        $('.gfield_error').on('click', function () {
            $(this).find('.validation_message').fadeOut();
        });
    });

    // Smooth Scroll
    $('a[href*="#"]').not('[href="#"]').not('[href="#!"]').not('[href="#0"]').not('[data-commentid]').click(function (event) { // Remove links that don't actually link to anything
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) { // On-page links
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 60
                }, 1000, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    }
                    ;
                });
            }
        }
    });

    // Mobile menu sidebar
    $('#page-content-wrapper').on('click', function () {
        if ($('#wrapper').hasClass('toggled')) {
            $('body').toggleClass('menu-open');
            $('#menu-toggle').removeClass('active');
            $('#wrapper').removeClass('toggled');
        }
        ;
    }).on('click', '#menu-toggle', function (e) {
        e.stopPropagation();
        if ($('#menu-toggle').hasClass('inactive')) {
            $('body').toggleClass('menu-open');
            parent.history.back();
            return false;
        } else {
            $('body').toggleClass('menu-open');
            $(this).toggleClass('active');
            $('#wrapper').toggleClass('toggled');
        }
    });

    /*********** Sticky menu ************************************************************/

    function menu_scroll_top() {
        if ($(document).scrollTop() > 122) {
            $(".main-menu").addClass("active");
            $(".dvv-cart").addClass("active");
            $(".badge-primary").addClass("active");
        } else {
            $(".main-menu").removeClass("active");
            $(".dvv-cart").removeClass("active");
            $(".badge-primary").removeClass("active");
        }
    }

    menu_scroll_top();

    $(window).on("scroll", function (e) {
        menu_scroll_top();
    });

    /*********** Slick Slider ************************************************************/
    if ($(".carousel").length > 0) {
        $(".carousel").each(function () {
            var $this = $(this);

            var accessibility = ($this.data('accessibility') == 1 ? true : false);
            var adaptiveHeight = ($this.data('adaptiveheight') == 1 ? true : false);
            var autoplay = ($this.data('autoplay') == 1 ? true : false);
            var arrows = ($this.data('arrows') == 1 ? true : false);
            var centerMode = ($this.data('centermode') == 1 ? true : false);
            var focusOnSelect = ($this.data('focusonselect') == 1 ? true : false);
            var dots = ($this.data('dots') == 1 ? true : false);
            var fade = ($this.data('effect') == 'fade' ? true : false);
            var infinite = ($this.data('infinite') == 1 ? true : false);
            var pauseOnHover = ($this.data('pauseonhover') == 1 ? true : false);
            var swipe = ($this.data('swipe') == 1 ? true : false);
            var variableWidth = ($this.data('variablewidth') == 1 ? true : false);

            var responsive = ($this.data('responsive') == 1 ?
                [{
                    breakpoint: 1190,
                    settings: {
                        slidesToShow: $this.data('slidestoshow_1190'),
                        slidesToScroll: $this.data('slidestoscroll_1190'),
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: $this.data('slidestoshow_768'),
                        slidesToScroll: $this.data('slidestoscroll_768'),
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: $this.data('slidestoshow_480'),
                        slidesToScroll: $this.data('slidestoscroll_480'),
                    }
                }
                ] : false);

            if ($this.data('random') == 1) {
                $this.on('init', function (event, slick) {
                    $('.slick-track').randomize('.slick-slide');
                });
            }

            $this.slick({
                accessibility: accessibility,
                adaptiveHeight: adaptiveHeight,
                autoplay: autoplay,
                autoplaySpeed: $this.data('autoplayspeed'),
                arrows: arrows,
                easing: $this.data('easing'),
                prevArrow: '<button type="button" class="slick-prev ' + $this.data('prevarrow') + '"></button>',
                nextArrow: '<button type="button" class="slick-next ' + $this.data('nextarrow') + '"></button>',
                centerMode: centerMode,
                focusOnSelect: focusOnSelect,
                dots: dots,
                fade: fade,
                infinite: infinite,
                pauseOnHover: pauseOnHover,
                speed: $this.data('speed'),
                swipe: swipe,
                variableWidth: variableWidth,
                slidesToShow: $this.data('slidestoshow'),
                slidesToScroll: $this.data('slidestoscroll'),
                asNavFor: $this.data('asnavfor'),
                responsive: responsive,
            });

        });

        $('.slick-next-slider-nav').on('click', function () {
            $('.carousel').slick('slickNext');
        });
        $('.slick-prev-slider-nav').on('click', function () {
            $('.carousel').slick('slickPrev');
        });

        var slides = $(".slider-for .slick-track > .slick-slide").length;
        $('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            var inFocus = $('.slider-for .slick-current').attr('data-slick-index');
            $('.slider-nav .slick-current').removeClass('slick-current');
            $('.slider-nav .slick-slide[data-slick-index="' + inFocus + '"]').addClass('slick-current');
        });
    }

    /*********** Quantity field ***********/
    // Input +- tweak
    $(function (a) {
        a(document).on("click", ".plus, .minus", function () {
            var b = a(this).closest(".quantity").find(".qty"),
                c = parseFloat(b.val()),
                d = parseFloat(b.attr("max")),
                e = parseFloat(b.attr("min")),
                f = b.attr("step");
            c && "" !== c && "NaN" !== c || (c = 0),
                ("" === d || "NaN" === d) && (d = ""),
                ("" === e || "NaN" === e) && (e = 0),
                ("any" === f || "" === f || void 0 === f || "NaN" === parseFloat(f)) && (f = 1),
                a(this).is(".plus") ? b.val(d && (d == c || c > d) ? d : c + parseFloat(f)) : e && (e == c || e > c) ? b.val(e) : c > 0 && b.val(c - parseFloat(f)),
                b.trigger("change");
            $('[name="update_cart"]').trigger('click');
        });
    });

    // Auto update Card on Quantity change
    $('.woocommerce-cart-form input.qty').change(function () {
        $('[name="update_cart"]').trigger('click');
    });

    $('.button-variable-item-wit').click(function () {
        $('.palette-slider-wraper').hide();
        $('.palette-field-wrap').hide();
        $('.input-box-colorselector').hide();
    });

    //$('.button-variable-item-mengkleur-kiezen').click(function () {
    $('.button-variable-item-kleur').click(function () {
        $('#colorselector-color').trigger('click');
        $('.palette-slider-wraper').show();
        $('.palette-field-wrap').show();
        $('.input-box-colorselector').show();
    });
    if ($('#pa_wit-of-kleur').length > 0) {
        $('.input-box-colorselector').appendTo($('#pa_wit-of-kleur').parent().find('ul'));
        $('#remember-color-input').appendTo($('#pa_wit-of-kleur').parent().find('ul'));
        $('#remember-color-code').appendTo($('#pa_wit-of-kleur').parent().find('ul'));
        $('#colorselector').attr('readonly="readonly"');
        $('.input-box-colorselector').hide();
    }
    $(".wvs-radio-variable-item").wrap("<span class='custom-radio1'></span>");
    $(".wvs-radio-variable-item").after("<i></i>");

    $('.carousel-element').slick({
        infinite: true,
        autoplay: true,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 3000,
        variableWidth: true,
        swipe: true,
    });

    // Make divs always same height
    var minHeight = 0;

    $(".bericht-titel").each(function () {
        if ($(this).height() > minHeight) { minHeight = $(this).height(); }
    });
    $(".bericht-titel").height(minHeight);

    //Causes problems with IOS
    // $(".bericht-content").each(function(){
    //     if ($(this).height() > minHeight) { minHeight = $(this).height(); }
    // });
    // $(".bericht-content").height(minHeight);

    //Edit checkout input classes
    $("#billing_house_number_field").addClass("col-md-4");
    $("#billing_house_number_suffix_field").addClass("col-md-4");

    $("#billing_city_field").addClass("col-md-5");
    $("#billing_street_name_field").addClass("col-md-7");

    $("#shipping_house_number_field").addClass("col-md-4");
    $("#shipping_house_number_suffix_field").addClass("col-md-4");

    $("#shipping_city_field").addClass("col-md-5");
    $("#shipping_street_name_field").addClass("col-md-7");

    // Disallow numbers in streetname input field in checkout
    $("#billing_street_name").attr("onkeypress", "return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)");

    // Change location of the advanced discount table
    $('.sl-product.clearfix .product_meta').insertBefore('p.price');

    // Show loading cursor after clicking on the 'Add to cart' button
    $('.product button[type="submit"]').on('click', function () {
        $('body').addClass('adding-product');
    });

    // Check if the order button should be disabled based on the subtotal and what categories the products in the cart have
    function check_disable_order(){ 
        $.ajax({
            type: 'POST',
            url: objectL10n.ajaxurl,
            data: {
                'ajax_request' : 1,
                'action' : 'apply_price_limit',
            },
            success: function (data) {
                // 1 = disable order functionality
                if(data == 1){
                    $('.wc-proceed-to-checkout')
                    .addClass('disabled')
                    .find('a').attr('href', '#');

                    $('.cart-notice').css('display', 'block');
                } else{
                    $('.wc-proceed-to-checkout')
                    .removeClass('disabled')
                    .find('a').attr('href', window.location.origin + "/afrekenen/");

                    $('.cart-notice').css('display', 'none');
                }
            }
        });
    }

    if (top.location.pathname === '/winkelmand/'){
        check_disable_order();
    }

    $(document.body).on('updated_cart_totals', function(){
        check_disable_order();
    });

    // Homepage prompt
    $('.homepage-prompt-close, .close-homepage-prompt').on('click', function(){
        $('.homepage-prompt-overlay').hide();
    });

    $('.choose-alt-homepage').on('click', function(){
        localStorage.setItem("homepageType", "business");
    });
    $('.choose-reg-homepage').on('click', function(){
        localStorage.setItem("homepageType", "consumer");
    });

    // If the prices are currently with (true), or without (false) tax
    //var prices_taxless = false;

    /**
     * Remove 21% of all prices currently on the page, or restore the original price if doing the opposite
     */
    // function toggle_price_tax(remove_tax){
    //     if(remove_tax && prices_taxless == false){
    //         $(document).find('.woocommerce-Price-amount.amount bdi').each(function(){
    //             var price_html = $(this).html();
    //             var price = parseFloat($(this).attr('price').toString().replace(',','.')).toFixed(2);

    //             // Remove 21%
    //             var price_taxless = (price / 100) * 79;

    //             // Replace price in element
    //             var price_taxless_html = price_html.replace(price.toString().replace('.',','), price_taxless.toFixed(2).toString().replace('.',','));
    //             $(this).html(price_taxless_html);

    //             prices_taxless = true;
    //         });
    //     } else if(!remove_tax && prices_taxless == true){
    //         $(document).find('.woocommerce-Price-amount.amount bdi').each(function(){
    //             var price_html = $(this).html();
    //             var current_price = parseFloat(price_html.substring(price_html.indexOf(';') + 1).replace(',','.')).toFixed(2);
    //             var taxed_price = parseFloat($(this).attr('price').toString().replace(',','.')).toFixed(2);

    //             var current_price_html = current_price.toString().replace('.',',');
    //             var new_price_html = taxed_price.toString().replace('.',',');

    //             var price_taxless_html = price_html.replace(current_price_html, new_price_html);
    //             $(this).html(price_taxless_html);

    //             prices_taxless = false;
    //         });
    //     }
    // }

    /**
     * Toggle the slider that toggles the prices between taxed and taxless
     */
    // $('#price-tax-toggle-close').on('click', function(){
    //     $('.price-tax-toggle').toggleClass('closed');
    // });

    /**
     * Toggle between the taxed and taxless prices with a slider
     */
//     $('#toggle-price-tax').on('change', function(){
//         if(prices_taxless == false){
//             toggle_price_tax(true);
//         } else {
//             toggle_price_tax(false);
//         }
//     });

//     $('.single-product button[type="submit"]').on('click', function(e){
//         if($('li[data-title="Kleur"]').hasClass('selected')){
//             if($('#remember-color-input').attr('value')){
//                 e.preventDefault();
//                 $('.woocommerce-notices-wrapper').append('\
//                     <div class="alert floating alert-warning alert-dismissible fade show" role="alert">\
//                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">\<span aria-hidden="true">×</span>\</button>\
//                         <ul class="list-unstyled mb-0">\
//                             <li>Geen kleur gekozen.</li>\
//                         </ul>\
//                     </div>\
//                 ');
//                 $('html, body').animate({
//                     scrollTop: $(".woocommerce-notices-wrapper").offset().top - 200
//                 }, 2000);
//             }
//         }
//     });

//     $(window).load(function(){

//         // Give the attribute 'price' to all price elements so the original value is preserved
//         $(document).find('.woocommerce-Price-amount.amount bdi').each(function(){
//             var price_html = $(this).html();
//             var price = price_html.substring(price_html.indexOf(';') + 1);
//             $(this).attr('price', price);
//         });
    
//         // Determine if prices should be taxless by default
//         var taxless_at_init = $('#taxfree').attr('value');
    
//         if(taxless_at_init == 'true'){
//             toggle_price_tax(1);
//         }
//     });
// });