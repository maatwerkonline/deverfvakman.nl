<?php
get_header();
?>

<section id="content" class="pb-0 pt-4">
	<div class="container">

		<div class="row">

			<div class="col-12 text-center pb-4 mb-3">
				<h1 class="text-uppercase mb-0 d-inline">
					<?php _e('Instructie Blogs', 'maatwerkonline'); ?>
				</h1>

				<?php if ( function_exists('yoast_breadcrumb') ) {  /* If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs */
					yoast_breadcrumb( '<p id="breadcrumbs" class="mb-0">','</p>' );
				}; ?>
			</div>

			<div class="col-12">

				<div class="posts row">

						<?php if (have_posts()) : $i = 0; while (have_posts()) : the_post();
							global $post; ?>

							<?php if(empty($post_classes)){
								$post_classes = array();
							}
							$class_array = array_merge_recursive($post_classes, get_post_class());
							$post_class_out = 'class="'. join( ' ', $class_array ) .'"';
							
							if(!empty($post->post_excerpt)) {
								$content = get_the_excerpt();
							} else {
								$content = get_the_content();
								$content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $content);
								$content = strip_shortcodes( $content );
								$content = wp_trim_words( $content, 10, ' [...]' );
								
							}; ?>
							
							<div class="col-md-4">
								<div <?php echo $post_class_out; ?>>
									<a href="<?php echo get_the_permalink(); ?>" class="row py-1 py-md-3">
										<div class="col-12 d-flex align-items-center">
											<div class="image-holder mb-0 rounded w-100">
												<?php echo wp_get_attachment_image( get_post_thumbnail_id($post->ID), '', array('class' => 'mb-0 image-crop w-100', 'sizes' => '') ); ?>
											</div>
										</div>
										<div class="col-12 d-flex align-items-center">
											<div class="text-container">
												<h3 class="h5 mb-3 mt-2" data-mh="post-title">
													<strong><?php echo get_the_title(); ?></strong>
												</h3>
												<p class="mb-0 d-md-block" data-mh="post-content">
													<?php echo $content; ?>
												</p>
												<p class="btn-primary btn float-right mt-4">
													<?php _e( 'Read more', 'maatwerkonline' ); ?>
												</p>
											</div>
										</div>
									</a>
								</div>
							</div>

							<?php endwhile;
						else: ?>
							<div id="404">
								<h2><?php _e('No posts were found. Sorry!', 'maatwerkonline'); ?></h2>
							</div>
						<?php endif; ?>

						<?php $args = array(
							'previous_string' => __( 'Previous', 'maatwerkonline' ),
							'next_string'     => __( 'Next', 'maatwerkonlin' ),
							'before_output'   => '<div class="col-12 d-flex"><div class="ml-auto"><nav aria-label="'.__('Post navigation', 'onlinemarketingnl').'"><ul class="pagination">',
							'after_output'    => '</ul></nav></div></div>'
						);
						wp_bootstrap_pagination( $args ); ?>

					</div>

			</div>

		</div>

	</div>
</section>

<?php get_footer();
