<?php
// Register Custom Post Type
function example() {

	$labels = array(
		'name'                => _x( 'Example', 'Post Type General Name', 'maatwerkonline' ),
		'singular_name'       => _x( 'Example', 'Post Type Singular Name', 'maatwerkonline' ),
		'menu_name'           => __( 'Examples', 'maatwerkonline' ),
		'name_admin_bar'      => __( 'Examples', 'maatwerkonline' ),
		'all_items'           => __( 'All Examples', 'maatwerkonline' ),
		'add_new_item'        => __( 'Add New Example', 'maatwerkonline' ),
		'add_new'             => __( 'Add New', 'maatwerkonline' ),
		'new_item'            => __( 'New Example', 'maatwerkonline' ),
		'edit_item'           => __( 'Edit Example', 'maatwerkonline' ),
		'update_item'         => __( 'Update Example', 'maatwerkonline' ),
		'view_item'           => __( 'Example', 'maatwerkonline' ),
		'search_items'        => __( 'Search Example', 'maatwerkonline' ),
		'not_found'           => __( 'Not found', 'maatwerkonline' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'maatwerkonline' ),
	);
	$rewrite = array(
		'slug'                  => __( 'example', 'maatwerkonline' ),
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'               => __( 'Example', 'maatwerkonline' ),
		'description'         => __( 'Individual Example', 'maatwerkonline' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields', 'page-attributes' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-edit', // https://developer.wordpress.org/resource/dashicons/
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'               => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'example', $args );

}
add_action( 'init', 'example', 0 );
