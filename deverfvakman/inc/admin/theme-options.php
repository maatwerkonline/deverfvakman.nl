<?php
// Admin Reset
if ( ! defined( 'ABSPATH' ) ) exit;

// Kirki Customizer fields
function kirki_panels_sections( $wp_customize ) {

	// Add Kirki Panels
	$wp_customize->add_panel( 'appearance_controls', array(
		'priority'    => 10,
		'title'       => __( 'Appearance', 'maatwerkonline' ),
		'description' => __( 'This panel contains the Appearance options', 'maatwerkonline' ),
	));

	$wp_customize->add_panel( 'elements_controls', array(
		'priority'    => 20,
		'title'       => __( 'Elements', 'maatwerkonline' ),
		'description' => __( 'This panel contains the different elements of the site.', 'maatwerkonline' ),
	));

	$wp_customize->add_panel( 'contact_controls', array(
		'priority'    => 40,
		'title'       => __( 'Contact', 'maatwerkonline' ),
		'description' => __( 'This panel contains the Contact options', 'maatwerkonline' ),
	));

	// Add Kirki Sections
	$wp_customize->add_section( 'title_tagline', array(
		'title'    => __( 'Site Title, Tagline, Logo & Favicon', 'maatwerkonline' ),
		'priority' => 10,
		'panel'       => 'appearance_controls',
	));

	$wp_customize->add_section( 'header', array(
		'title'    => __( 'Header', 'maatwerkonline' ),
		'priority' => 20,
		'panel'       => 'appearance_controls',
	));

	$wp_customize->add_section( 'static_front_page', array(
		'title'          => __( 'Static Front Page', 'maatwerkonline' ),
		'priority'       => 10,
		'panel'       => 'elements_controls',
		'description'    => __( 'Your theme supports a static front page.', 'maatwerkonline' ),
	));

	$wp_customize->add_section( 'relations_custom_post_types_section', array(
		'title'          => __( 'Relations Custom Post Types' ),
	 'priority'       => 15,
	 'panel'       => 'elements_controls',
   ) );

	$wp_customize->add_section( 'wpseo_breadcrumbs_customizer_section', array(
		'title'       => __( 'Breadcrumbs', 'maatwerkonline' ),
		'priority'    => 40,
		'panel'       => 'elements_controls',
	));

	$wp_customize->add_section( 'cookienotification_section', array(
		'title'          => __( 'Cookie Notification', 'maatwerkonline' ),
		'priority'       => 50,
		'panel'       => 'elements_controls',
		'description'    => __( 'If you want to show uses you use cookies you want to change these options.', 'maatwerkonline' ),
	));

	$wp_customize->add_section( '404_warning_section', array(
		'title'          => __( '404 Warning', 'maatwerkonline' ),
		'priority'       => 60,
		'panel'       => 'elements_controls',
	));

	$wp_customize->add_section( 'contact_section', array(
		'title'       => __( 'Contact page', 'maatwerkonline' ),
		'priority'    => 5,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'registration_section', array(
		'title'       => __( 'Registration numbers', 'maatwerkonline' ),
		'priority'    => 10,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'address_section', array(
		'title'       => __( 'Address', 'maatwerkonline' ),
		'priority'    => 20,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'opening_hours_section', array(
		'title'       => __( 'Opening Hours', 'maatwerkonline' ),
		'priority'    => 25,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'phone_section', array(
		'title'       => __( 'Phone', 'maatwerkonline' ),
		'priority'    => 30,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'email_section', array(
		'title'       => __( 'Email', 'maatwerkonline' ),
		'priority'    => 40,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'social_media_links_section', array(
		'title'       => __( 'Social Media Icons', 'maatwerkonline' ),
		'priority'    => 50,
		'panel'       => 'contact_controls',
	));

	$wp_customize->add_section( 'usps_section', array(
		'title'          => __( 'USPs', 'maatwerkonline' ),
		'priority'       => 10,
		'panel'       => 'appearance_controls',
	));

	$wp_customize->add_section( 'proadvan_section', array(
		'title'          => __( 'Product Widgets', 'maatwerkonline' ),
		'priority'       => 10,
		'panel'       => 'appearance_controls',
	));

}
add_action( 'customize_register', 'kirki_panels_sections' );

// Add Kirki Controls
function kirki_controls( $controls ) {

	$post_type_items = array();
	$post_type_items['user'] = __( 'Users', 'onm_textdomain' );
	$post_types = get_post_types( array (
		'show_ui' => true,
		'show_in_menu' => true,
	), 'objects' );

	foreach ( $post_types  as $post_type ) {
		if ( $post_type->name == 'attachment' ) continue;
		$post_type_items[ $post_type->name ] = $post_type->labels->name;
	}

	$controls[] = array(
		'type'        => 'repeater',
		'label'       => __( 'Relations', 'onm_textdomain' ),
		'section'     => 'relations_custom_post_types_section',
    	'priority'    => 10,
		'settings'    => 'relations',
		'default'     => array(),
		'fields' => array(
			'relation_1' => array(
        		'type'        => 'select',
				'label'       => __( 'From', 'onm_textdomain' ),
        		'choices'     => $post_type_items,
			),
			'relation_2' => array(
				'type'        => 'select',
				'label'       => __( 'With', 'onm_textdomain' ),
				'choices'     => $post_type_items,
			),
		)
	);

	$controls[] = array(
		'type'        => 'repeater',
		'label'       => __( 'USPs', 'maatwerkonline' ),
		'section'     => 'usps_section',
    	'priority'    => 10,
		'settings'    => 'uspbox',
		'default'     => array(),
		'fields' => array(
			'icon' => array(
				'type'        => 'text',
			 	'label'       => __( 'Icon', 'maatwerkonline' )
		 	),
			'text' => array(
       			'type'        => 'text',
				'label'       => __( 'USP', 'maatwerkonline' )
			),
			'link' => array(
				'type'        => 'text',
				 'label'       => __( 'Link', 'maatwerkonline' ),
				 'default'     => 'https://'
			 ),
		)
	);


	$controls[] = array(
		'type'        => 'repeater',
		'label'       => __( 'Product Advantages', 'maatwerkonline' ),
		'section'     => 'proadvan_section',
    	'priority'    => 10,
		'settings'    => 'proadvan',
		'default'     => array(),
		'fields' => array(
			'list' => array(
				'type'        => 'textarea',
				 'label'       => __( 'Advantage', 'maatwerkonline' ),
		 	),
			
		)
	);

	

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'protitle',
		'label'       => __( 'Title', 'maatwerkonline' ),
		'description' => __( 'Enter title', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'proadvan_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'procontent',
		'label'       => __( 'Telephone', 'maatwerkonline' ),
		'description' => __( 'Enter telephone number', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'proadvan_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'prodaytime',
		'label'       => __( 'Day&time', 'maatwerkonline' ),
		'description' => __( 'Enter Day time', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'proadvan_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'probuttom',
		'label'       => __( 'Button Text', 'maatwerkonline' ),
		'description' => __( 'Enter button text', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'proadvan_section',
		'priority'    => 10,
	);



	$controls[] = array(
		'type'        => 'image',
		'settings'     => 'logo',
		'label'    => __( 'Logo', 'maatwerkonline' ),
		'description'    => __( 'If you upload a Logo this will overwrite the Site Title and Tagline in the header.', 'maatwerkonline' ),
		'help'        => __( 'If you have your logo in .png, please upload it in .png.', 'maatwerkonline' ),
		'section'     => 'title_tagline',
		'priority'    => 20,
	);

	$controls[] = array(
		'type'        => 'radio-buttonset',
		'settings'     => 'header',
		'label'    => __( 'Header', 'maatwerkonline' ),
		'description'    => __( 'How would you like to make the Header look like? Would like the Logo and Navigation to be Inline or Seperated?', 'maatwerkonline' ),
		'help'        => __( 'Want to adjust one of these? Look into the /template-parts/headers/folder and add or edit your header.', 'maatwerkonline' ),
		'section'     => 'header',
		'priority'    => 10,
		'default'     => 'header_inline',
		'choices'     => array(
			'header_inline'   => __( 'Inline', 'maatwerkonline' ),
			'header_seperated' => __( 'Seperated', 'maatwerkonline' ),
		),
	);

	$controls[] = array(
		'type'        => 'dropdown-pages',
		'settings'     => 'error_404_page',
		'label'       => __( 'Error 404 page', 'maatwerkonline' ),
		'description' => __( 'Choose the page which will be displayed when people got the 404-error.', 'maatwerkonline' ),
		'section'     => '404_warning_section',
		'priority'    => 30,
	);

	$controls[] = array(
		'type'        => 'dropdown-pages',
		'settings'     => 'contact_page',
		'label'       => __( 'Contact page', 'maatwerkonline' ),
		'description' => __( 'Choose the page which will be displayed when people click the contact button.', 'maatwerkonline' ),
		'section'     => 'contact_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'vat',
		'label'       => __( 'VAT number', 'maatwerkonline' ),
		'description' => __( 'Enter your VAT number here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'registration_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'coc',
		'label'       => __( 'Chamber of Commerce number', 'maatwerkonline' ),
		'description' => __( 'Enter your Chamber of Commerce number here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'registration_section',
		'priority'    => 20,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'bank_account',
		'label'       => __( 'Bank Account number', 'maatwerkonline' ),
		'description' => __( 'Enter your Bank Account number here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'registration_section',
		'priority'    => 30,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'visiting_building',
		'label'       => __( 'Visiting Address', 'maatwerkonline' ),
		'description' => __( 'Enter the name of the building you are working in', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 5,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'visiting_address',
		'description' => __( 'Enter your street address here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'visiting_zipcode',
		'description' => __( 'Enter your zip-code here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 20,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'visiting_city',
		'description' => __( 'Enter your city here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 30,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'post_address',
		'label'       => __( 'Post Address', 'maatwerkonline' ),
		'description' => __( 'Enter your street address here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 40,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'post_zipcode',
		'description' => __( 'Enter your zip-code here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 50,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'post_city',
		'description' => __( 'Enter your city here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'address_section',
		'priority'    => 60,
	);

	$controls[] = array(
		'type'        => 'textarea',
		'settings'     => 'opening_hours_text',
		'label'       => __( 'Opening Hours text', 'maatwerkonline' ),
		'description' => __( 'Enter the text which will be displayed to the visitors.', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'opening_hours_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'repeater',
		'label'       => __( 'Opening Hours', 'maatwerkonline' ),
		'section'     => 'opening_hours_section',
    	'priority'    => 10,
		'settings'    => 'opening_hours',
		'default'     => array(),
		'fields' => array(
			'day' => array(
        		'type'        => 'select',
				'label'       => __( 'Day', 'maatwerkonline' ),
        		'choices'     => array(
					'Mo' => __( 'Monday', 'maatwerkonline' ),
					'Tu' => __( 'Tuesday', 'maatwerkonline' ),
					'We' => __( 'Wednesday', 'maatwerkonline' ),
					'Th' => __( 'Thursday', 'maatwerkonline' ),
					'Fr' => __( 'Friday', 'maatwerkonline' ),
					'Sa' => __( 'Saturday', 'maatwerkonline' ),
					'Su' => __( 'Sunday', 'maatwerkonline' ),
				),
			),
			'open' => array(
				'type'        => 'select',
				'label'       => __( 'Opening Hour', 'maatwerkonline' ),
        		'choices'     => array(
					'00:30' => '00:30',
					'01:00' => '01:00',
					'01:30' => '01:30',
					'02:00' => '02:00',
					'02:30' => '02:30',
					'03:00' => '03:00',
					'03:30' => '03:30',
					'04:00' => '04:00',
					'04:30' => '04:30',
					'05:00' => '05:00',
					'05:30' => '05:30',
					'06:00' => '06:00',
					'06:30' => '06:30',
					'07:00' => '07:00',
					'07:30' => '07:30',
					'08:00' => '08:00',
					'08:30' => '08:30',
					'09:00' => '09:00',
					'09:30' => '09:30',
					'10:00' => '10:00',
					'10:30' => '10:30',
					'11:00' => '11:00',
					'11:30' => '11:30',
					'12:00' => '12:00',
					'12:30' => '12:30',
					'13:00' => '13:00',
					'13:30' => '13:30',
					'14:00' => '14:00',
					'14:30' => '14:30',
					'15:00' => '15:00',
					'15:30' => '15:30',
					'16:00' => '16:00',
					'16:30' => '16:30',
					'17:00' => '17:00',
					'17:30' => '17:30',
					'18:00' => '18:00',
					'18:30' => '18:30',
					'19:00' => '19:00',
					'19:30' => '19:30',
					'20:00' => '20:00',
					'20:30' => '20:30',
					'21:00' => '21:00',
					'21:30' => '21:30',
					'22:00' => '22:00',
					'22:30' => '22:30',
					'23:00' => '23:00',
					'23:30' => '23:30',
					'00:00' => '00:00',
				),
			),
			'close' => array(
        		'type'        => 'select',
				'label'       => __( 'Closing Hour', 'maatwerkonline' ),
        		'choices'     => array(
					'00:30' => '00:30',
					'01:00' => '01:00',
					'01:30' => '01:30',
					'02:00' => '02:00',
					'02:30' => '02:30',
					'03:00' => '03:00',
					'03:30' => '03:30',
					'04:00' => '04:00',
					'04:30' => '04:30',
					'05:00' => '05:00',
					'05:30' => '05:30',
					'06:00' => '06:00',
					'06:30' => '06:30',
					'07:00' => '07:00',
					'07:30' => '07:30',
					'08:00' => '08:00',
					'08:30' => '08:30',
					'09:00' => '09:00',
					'09:30' => '09:30',
					'10:00' => '10:00',
					'10:30' => '10:30',
					'11:00' => '11:00',
					'11:30' => '11:30',
					'12:00' => '12:00',
					'12:30' => '12:30',
					'13:00' => '13:00',
					'13:30' => '13:30',
					'14:00' => '14:00',
					'14:30' => '14:30',
					'15:00' => '15:00',
					'15:30' => '15:30',
					'16:00' => '16:00',
					'16:30' => '16:30',
					'17:00' => '17:00',
					'17:30' => '17:30',
					'18:00' => '18:00',
					'18:30' => '18:30',
					'19:00' => '19:00',
					'19:30' => '19:30',
					'20:00' => '20:00',
					'20:30' => '20:30',
					'21:00' => '21:00',
					'21:30' => '21:30',
					'22:00' => '22:00',
					'22:30' => '22:30',
					'23:00' => '23:00',
					'23:30' => '23:30',
					'00:00' => '00:00',
				),
			),
		)
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'phone_number',
		'label'       => __( 'Phone number', 'maatwerkonline' ),
		'description' => __( 'Enter your phone number here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'phone_section',
		'priority'    => 10,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'mobile_number',
		'label'       => __( 'Mobile number', 'maatwerkonline' ),
		'description' => __( 'Enter your mobile number here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on the frontend.', 'maatwerkonline' ),
		'section'     => 'phone_section',
		'priority'    => 20,
	);

	$controls[] = array(
		'type'        => 'text',
		'settings'     => 'email',
		'label'       => __( 'Email', 'maatwerkonline' ),
		'description' => __( 'Enter your email here', 'maatwerkonline' ),
		'help'        => __( 'If you leave this field empty it will not be shown on your website.', 'maatwerkonline' ),
		'section'     => 'email_section',
		'priority'    => 30,
	);

	$controls[] = array(
		'type'        => 'repeater',
		'label'       => __( 'Social Media Links', 'maatwerkonline' ),
		'section'     => 'social_media_links_section',
		'default'     => 'facebook',
    	'priority'    => 10,
		'settings'    => 'social_networks',
		'default'     => array(),
		'fields' => array(
			'network' => array(
				'type'        => 'select',
				'label'       => __( 'Social Network', 'maatwerkonline' ),
				'choices'     => array(
					'at' => __( 'Email', 'maatwerkonline' ),
					'facebook' => __( 'Facebook', 'maatwerkonline' ),
					'linkedin' => __( 'LinkedIn', 'maatwerkonline' ),
					'envelope' => __( 'Newsletter', 'maatwerkonline' ),
					'rss' => __( 'RSS', 'maatwerkonline' ),
					'twitter' => __( 'Twitter', 'maatwerkonline' ),
					'whatsapp' => __( 'Whatsapp', 'maatwerkonline' ),
					'wordpress' => __( 'Wordpress', 'maatwerkonline' ),
					'instagram' => __( 'Instagram', 'maatwerkonline' ),
					'youtube' => __( 'Youtube', 'maatwerkonline' ),
        		),
			),
			'url' => array(
				'type'        => 'text',
				'label'       => __( 'Link URL', 'maatwerkonline' ),
				'default'     => 'https://'
			),
		)
	);

	return $controls;

}
add_filter( 'kirki/controls', 'kirki_controls' );

// Kirki Style Customizer
function kirki_customizer_style() {
    wp_add_inline_style( 'customize-controls', '.customize-control { margin-bottom: 15px }');
}
add_action( 'customize_controls_enqueue_scripts', 'kirki_customizer_style');

// Relations
function connection_types() {
	$relations = get_theme_mod( 'relations', array() );
	foreach ( $relations as $relation ) {
		if( $relation['relation_1'] == 'user' || $relation['relation_2'] == 'user' ) {
			p2p_register_connection_type( array(
				'name' => $relation['relation_1'].'_to_'.$relation['relation_2'],
     			'from' => $relation['relation_1'],
      			'to' => $relation['relation_2'],
      			'sortable' => 'any',
      			'admin_column' => 'any',
			) );
		} elseif( $relation['relation_1'] == $relation['relation_2'] ) {
			p2p_register_connection_type( array(
				'name' => $relation['relation_1'].'_to_'.$relation['relation_2'],
     			'from' => $relation['relation_1'],
      			'to' => $relation['relation_2'],
      			'sortable' => 'any',
      			'admin_column' => 'to',
      			'reciprocal' => true,
      			'admin_box' => array(
					'show' => 'to'
  				)
			) );
		} else {
			p2p_register_connection_type( array(
				'name' => $relation['relation_1'].'_to_'.$relation['relation_2'],
      			'from' => $relation['relation_1'],
      			'to' => $relation['relation_2'],
      			'sortable' => 'any',
      			'admin_column' => 'any',
			) );
		};
	};
};
add_action( 'p2p_init', 'connection_types' );

// Add admin.css
function admin_style() {
	$user = wp_get_current_user();
	$user_id = get_current_user_id();
	$user_info = get_userdata($user_id);
	$user_email = $user_info->user_email;
	
	if($user_email != 'info@maatwerkonline.nl'){
		wp_enqueue_style('hide-notices', TEMPPATH.'/inc/admin/css/hide-notices.css', array(), THEME_VERSION);
	}
	wp_enqueue_style('theme-admin-css', TEMPPATH.'/inc/admin/css/admin.css', array(), THEME_VERSION);
}
add_action('admin_enqueue_scripts', 'admin_style');

// Add cusom loginlogo
function onm_custom_loginlogo() {
	echo '<style type="text/css">h1 a {background-image: url("'.esc_url( get_site_icon_url( 180 ) ).'") !important; }</style>';
}
add_action('login_head', 'onm_custom_loginlogo');

// Login Logo Link
function onm_custom_loginlogo_url($url) {
	return esc_url( home_url( '/' ));
}
add_filter( 'login_headerurl', 'onm_custom_loginlogo_url' );

// Login Default Remember Me
function login_checked_remember_me() {
	add_filter( 'login_footer', 'rememberme_checked' );
}
add_action( 'init', 'login_checked_remember_me' );

function rememberme_checked() {
	echo "<script>document.getElementById('rememberme').checked = true;</script>";
}

// Hide Migrate plugin
function hide_migrate() {
	$current_user = wp_get_current_user();
	if($current_user->user_email != 'info@maatwerkonline.nl'){
		   remove_menu_page('ai1wm_export'); 
	} else {
		global $menu;   
		foreach($menu as $key => $item) {
			if ( $item[0] === 'All-in-One WP Migration' ) {
				$menu[$key][0] = "Beta Creator";     //change name
			}
		  }
	}
}
add_action( 'admin_init', 'hide_migrate', 999999 );

//Change Footer Admin Text
function remove_footer_admin () {
	echo 'Powered by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Brought to you by <a href="https://www.maatwerkonline.nl/" target="_blank">Maatwerk Online</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

// Remove Widgets
function remove_widgets() {
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_RSS');
}
add_action( 'widgets_init', 'remove_widgets' );

// Remove Metabox Items
function my_remove_meta_boxes() {
  	remove_meta_box('postcustom', 'post', 'normal');
	remove_meta_box('dashboard_primary', 'dashboard', 'core');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );
