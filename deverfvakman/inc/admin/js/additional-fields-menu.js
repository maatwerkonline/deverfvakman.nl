jQuery(document).ready(function($) {
    "use strict";

    var $depth_all = $('#menu-to-edit').find('.menu-item');
    var $depth_zero = $('#menu-to-edit').find('.menu-item-depth-0');
    var $depth_one = $('#menu-to-edit').find('.menu-item-depth-1');

    var i = 0;
    $depth_zero.find('.field-description').each(function(){
        i++;
        
        $(this).before('<p class="field-menu-type description description-wide"><label for="add_mega'+i+'">'+objectL10n.menutype+'<br><select id="add_mega'+i+'" class="onm_additional_input add_mega widefat"><option value="">'+objectL10n.defaultmenu+'</option><option value="mega-menu columns-1">'+objectL10n.megamenusinglecolumn+'</option><option value="mega-menu columns-2">'+objectL10n.megamenu2columns+'</option><option value="mega-menu columns-3">'+objectL10n.megamenu3columns+'</option><option value="mega-menu columns-4">'+objectL10n.megamenu4columns+'</option><option value="mega-menu columns-5">'+objectL10n.megamenu5columns+'</option><option value="mega-menu columns-6">'+objectL10n.megamenu6columns+'</option><option value="mega-menu columns-7">'+objectL10n.megamenu7columns+'</option></select></p>');
        var classes = $(this).siblings('.field-css-classes').find('input').val();
        var current_c;
        for (var c = 1; c <= 7; c++) {
            current_c = 'mega-menu columns-'+c;
            if(classes.indexOf(current_c) >= 0) {
                $(this).siblings('.field-menu-type').find('select').val(current_c);
            }
        };
    });

    $depth_all.find('.field-description').each(function(){
        i++;
        var no_title_state = ($(this).siblings('.field-css-classes').find('input').val().indexOf('no-title') >= 0) ? ' checked' : '';
        $(this).after('<p class="field-no-title description description-wide"><label for="no_title'+i+'"><input type="checkbox" id="no_title'+i+'" class="onm_additional_input no_title" value="no-title"'+no_title_state+'>'+objectL10n.hidetitle+'</label></p>');
    });

    $('.onm_additional_input, .edit-menu-item-classes').change(function() {
        var $parent_item = $(this).closest('.menu-item');
        define_classes($parent_item);
    });

    function define_classes($item){
        var $class_field = $item.find('.field-css-classes input');
        var current_class_value = $class_field.val().replace('no-title','').replace('mega-menu columns-1','').replace('mega-menu columns-2','').replace('mega-menu columns-3','').replace('mega-menu columns-4','').replace('mega-menu columns-5','').replace('mega-menu columns-6','').replace('mega-menu columns-7','').replace('  ',' ');

        var new_class_value = [];

        new_class_value.push(current_class_value.trim());

        if($item.find('.add_mega').length > 0 && $item.find('.add_mega').val() !== ''){
            new_class_value.push($item.find('.add_mega').val());
        }

        if($item.find('.no_title').length > 0 && $item.find('.no_title').is(':checked')){
            new_class_value.push('no-title');
        }

        $class_field.val(new_class_value.join(' ').trim());

    }


});