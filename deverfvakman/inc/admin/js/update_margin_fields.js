jQuery(document).ready(function($) {
    "use strict";

    /**
     * Update margin for simple products
     */

    function update_margins(){

        var cog         = parseFloat($('#_wc_cog_cost').val().replace(',','.')).toFixed(2);
        var price       = parseFloat($('#_sale_price').val().replace(',','.')).toFixed(2);
        var price_zzp   = parseFloat($('#action_price_zzp').val().replace(',','.')).toFixed(2);

        // Text that is used when there is no calculation possible for a margin
        var missing_text = "-";

        console.log(cog);
        console.log(price);
        console.log(price_zzp);

        // If there is no CoG value on the product
        if(isNaN(cog)){

            $('#profit_margin_euro').val(missing_text);
            $('#profit_margin_percentage').val(missing_text);
            $('#profit_margin_zzp_euro').val(missing_text);
            $('#profit_margin_zzp_percentage').val(missing_text);

        } else {

            var margin_euro             = price - cog;
            var margin_percentage       = ((price - cog) / price) * 100;
            var margin_zzp_euro         = price_zzp - cog;
            var margin_zzp_percentage   = ((price_zzp - cog) / price_zzp) * 100;
    
            $('#profit_margin_euro').val(margin_euro.toFixed(2).toString().replace('.',','));
            $('#profit_margin_percentage').val(margin_percentage.toFixed(2).toString().replace('.',',') + '%');
            $('#profit_margin_zzp_euro').val(margin_zzp_euro.toFixed(2).toString().replace('.',','));
            $('#profit_margin_zzp_percentage').val(margin_zzp_percentage.toFixed(2).toString().replace('.',',') + '%');
            
            if(isNaN(price)){
                $('#profit_margin_euro').val(missing_text);
                $('#profit_margin_percentage').val(missing_text);
            }
            if(isNaN(price_zzp)){
                $('#profit_margin_zzp_euro').val(missing_text);
                $('#profit_margin_zzp_percentage').val(missing_text);
            }
        }
    }

    $(document).on('change', '#_sale_price, #action_price_zzp, #_wc_cog_cost', function() {
        update_margins();
    });

    /**
     * Update margin for variable products
     */

     function update_margins_variations(variation_nr){

        // Cog for specific variation
        var cog = parseFloat($('input[name="variable_cost_of_good['+variation_nr+']"]').val());

        // If cog for a specific variation has no value, use the general cog
        if(!cog){
            // CoG for the entire product (under the 'inventory' tab)
            var cog = parseFloat($('input[name="_wc_cog_cost_variable"]').val());
        }

        var price       = parseFloat($('input[name="variable_sale_price['+variation_nr+']"]').val());
        var price_zzp   = parseFloat($('input[name="action_prz_zzp['+variation_nr+']"]').val());

        // Text that is used when there is no calculation possible for a margin
        var missing_text = "-";

        // If there is no CoG value on neither the product nor the variation
        if(isNaN(cog)){

            $('[name="profit_margin_euro['+variation_nr+']"]').val(missing_text);
            $('[name="profit_margin_percentage['+variation_nr+']"]').val(missing_text);
            $('[name="profit_margin_zzp_euro['+variation_nr+']"]').val(missing_text);
            $('[name="profit_margin_zzp_percentage['+variation_nr+']"]').val(missing_text);

        } else {

            var margin_euro             = price - cog;
            var margin_percentage       = ((price - cog) / price) * 100;
            var margin_zzp_euro         = price_zzp - cog;
            var margin_zzp_percentage   = ((price_zzp - cog) / price_zzp) * 100;
    
            $('[name="profit_margin_euro['+variation_nr+']"]').val(margin_euro.toString().replace('.',','));
            $('[name="profit_margin_percentage['+variation_nr+']"]').val(margin_percentage.toFixed(2).toString().replace('.',',') + '%');
            $('[name="profit_margin_zzp_euro['+variation_nr+']"]').val(margin_zzp_euro.toString().replace('.',','));
            $('[name="profit_margin_zzp_percentage['+variation_nr+']"]').val(margin_zzp_percentage.toFixed(2).toString().replace('.',',') + '%');

            if(isNaN(price)){
                $('[name="profit_margin_euro['+variation_nr+']"]').val(missing_text);
                $('[name="profit_margin_percentage['+variation_nr+']"]').val(missing_text);
            }
            if(isNaN(price_zzp)){
                $('[name="profit_margin_zzp_euro['+variation_nr+']"]').val(missing_text);
                $('[name="profit_margin_zzp_percentage['+variation_nr+']"]').val(missing_text);
            }
        }
    }

    // If the CoG of a single variation changes
    $(document).on('change', '\
    .woocommerce_variation input[name*="variable_sale_price"],\
    .woocommerce_variation input[name*="variable_cost_of_good"],\
    .woocommerce_variation input[name*="action_prz_zzp"]\
    ', function(){
        var variation_name  = $(this).attr('name');
        var variation_nr    = variation_name[variation_name.length - 2];

        update_margins_variations(variation_nr);
    });

    // If the CoG for the entire product (under the 'inventory' tab) changes
    $(document).on('change', '#_wc_cog_cost_variable', function(){
        var variation_nr = 0;
        $('.woocommerce_variations').find('.woocommerce_variation').each(function(){

            update_margins_variations(variation_nr);
            variation_nr++;
        });
    });
});