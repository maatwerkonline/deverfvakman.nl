<?php

/* * ******* Woocommerce Shop ********** */

add_theme_support('woocommerce');

add_theme_support('wc-product-gallery-zoom');

add_theme_support('wc-product-gallery-lightbox');

add_filter('woocommerce_enqueue_styles', '__return_empty_array');

add_filter('woocommerce_ship_to_different_address_checked', '__return_false');

add_filter('woocommerce_helper_suppress_admin_notices', '__return_true');



class class_woocommerce {



    public function __construct() {

        add_filter('init', array(&$this, 'init'));

    }



    function init() {

        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

        remove_action('woocommerce_checkout_order_review', 'woocommerce_order_review', 10);

        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

        remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

        remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);

        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

        remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);

        remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

        remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

        remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);

        remove_action('woocommerce_before_shop_loop', 'wc_print_notices', 10);

        



        add_filter('woocommerce_catalog_orderby', array(&$this, 'translate_sorting_names'));

        add_filter('woocommerce_default_catalog_orderby_options', array(&$this, 'translate_sorting_names'));

        add_action('woocommerce_before_single_product', 'yoast_breadcrumb', 5);

        add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 8);

        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 25);

        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 50);

        add_action('woocommerce_before_single_product_summary', array(&$this, 'woocommerce_breadcrumb'), 5);

        add_action('woocommerce_before_single_product_summary', array(&$this, 'woocommerce_template_single_title'), 8);

        add_action('woocommerce_after_shop_loop_item_title', array(&$this, 'woocommerce_template_loop_rating'), 6);

        add_action('woocommerce_after_single_product', 'woocommerce_upsell_display', 15);

        add_action('woocommerce_before_shop_loop_item_title', array(&$this, 'woocommerce_our_choice_badge'), 10);

        add_action('woocommerce_before_single_product_summary', array(&$this, 'woocommerce_our_choice_badge'), 10);

        add_action('woocommerce_product_options_pricing', array(&$this, 'wc_cost_product_field'));

        add_action( 'save_post', array(&$this, 'wc_cost_save_product'));

        /* Add profit margin fields */
        add_action('woocommerce_product_options_pricing', array(&$this, 'mo_add_profit_margin_fields_simple'));
        add_action('woocommerce_variation_options_pricing', array(&$this, 'mo_add_profit_margin_fields_variations'), 11, 3);
        add_filter('woocommerce_available_variation', array(&$this, 'mo_add_profit_margin_fields_variations_data'));
        
        /* Save profit margin fields */
        add_action('woocommerce_process_product_meta_simple', array(&$this, 'mo_save_profit_margin_simple'), 10);
        add_action('woocommerce_save_product_variation', array(&$this, 'mo_save_profit_margin_variations'), 10, 2);
        

        /* Add custom message */

        add_action('woocommerce_product_options_general_product_data', array(&$this, 'add_custom_message') , 10, 1);

        add_action('save_post', array(&$this, 'save_custom_message') , 10);

        /* Add ZZP price field Optiion */ 

		add_action('woocommerce_variation_options_pricing', array(&$this, 'add_custom_price_to_variations') , 10, 3);

        add_action('woocommerce_save_product_variation', array(&$this, 'save_custom_price_variations'), 10, 2);

        add_filter('woocommerce_available_variation', array(&$this, 'add_custom_price_variation_data'));

        /* Simple product hook */

        add_filter('woocommerce_get_price', array(&$this, 'custom_price_product'), 10, 4);

        add_filter('woocommerce_get_regular_price', array(&$this, 'custom_price_product_regular_price'), 10, 2);

        add_filter('woocommerce_get_sale_price', array(&$this, 'custom_price_product_sale_price'), 10, 4);

        

        /* Variable product hook */

        add_filter('woocommerce_product_variation_get_price', array(&$this, 'filter_woocommerce_get_price'), 10, 4);

		add_filter('woocommerce_product_variation_get_regular_price', array(&$this, 'filter_woocommerce_get_regular_price'), 10, 2);

        add_filter('woocommerce_product_variation_get_price', array(&$this, 'filter_woocommerce_get_sale_price'), 10, 4);





        add_filter('woocommerce_package_rates', array(&$this, 'hide_shipping_when_free_is_available'), 10, 2);

        add_filter('loop_shop_per_page', array(&$this, 'custom_loop_shop_per_page'), 20);

        add_filter('woocommerce_add_to_cart_fragments', array(&$this, 'custom_woocommerce_add_to_cart_fragments'));

        add_filter('woocommerce_payment_complete_order_status', array(&$this, 'custom_woocommerce_auto_complete_virtual_orders'), 10, 2);

        add_filter('woocommerce_form_field_args', array(&$this, 'custom_woocommerce_form_field_args'), 10, 3);

        add_filter('woocommerce_breadcrumb_defaults', array(&$this, 'custom_woocommerce_breadcrumb_defaults_args'), 10, 1);

    }



    // Hide shipping rates when free shipping is available, but keep "Local pickup" 

    function hide_shipping_when_free_is_available( $rates, $package ) {

        $new_rates = array();

        foreach ( $rates as $rate_id => $rate ) {

            // Only modify rates if free_shipping is present.

            if ( 'free_shipping' === $rate->method_id ) {

                $new_rates[ $rate_id ] = $rate;

                break;

            }

        }



        if ( ! empty( $new_rates ) ) {

            //Save local pickup if it's present.

            foreach ( $rates as $rate_id => $rate ) {

                if ('local_pickup_plus' === $rate->method_id ) {

                    $new_rates[ $rate_id ] = $rate;

                    break;

                }

            }

            return $new_rates;

        }



        return $rates;

    }



    function translate_sorting_names( $catalog_orderby ) {

        $catalog_orderby = str_replace('Default sorting', __('Default sorting', 'maatwerkonline'), $catalog_orderby);

        $catalog_orderby = str_replace('Sort by popularity', __('Sort by popularity', 'maatwerkonline'), $catalog_orderby);

        $catalog_orderby = str_replace('Sort by average rating', __('Sort by average rating', 'maatwerkonline'), $catalog_orderby);

        $catalog_orderby = str_replace('Sort by latest', __('Sort by latest', 'maatwerkonline'), $catalog_orderby);

        $catalog_orderby = str_replace('Sort by price: low to high', __('Sort by price: low to high', 'maatwerkonline'), $catalog_orderby);

        $catalog_orderby = str_replace('Sort by price: high to low', __('Sort by price: high to low', 'maatwerkonline'), $catalog_orderby);

        return $catalog_orderby;

    }

    

    /* Custom message field */

    function add_custom_message(){

        woocommerce_wp_text_input(

            array (

                'id'        =>  'custom_message',

                'class'     =>  'wc_custom_message',

                'label'     =>  'Optioneel bericht',

            )

        );

    }

    
    /* Custom message field save */

    function save_custom_message($post_id){

        $woocommerce_custom_message = $_POST['custom_message'];

        update_post_meta($post_id, 'custom_message', esc_attr($woocommerce_custom_message));

    }   



    /* Custom Pricing field for ZZP user simple product */

    function wc_cost_product_field() {

        woocommerce_wp_text_input( array( 'id' => 'regular_price_zzp', 'class' => 'wc_input_price short', 'label' => __( 'Reguliere prijs ZZP', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')' ) );

        woocommerce_wp_text_input( array( 'id' => 'action_price_zzp', 'class' => 'wc_input_price short', 'label' => __( 'Actieprijs ZZP', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')' ) ); 

    }



    /* Simple product custom pricing field value save */

    function wc_cost_save_product( $product_id ) {

         // stop the quick edit interferring as this will stop it saving properly, when a user uses quick edit feature

         if (wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce'))

            return;



        // If this is a auto save do nothing, we only save when update button is clicked

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )

            return;

        if ( isset( $_POST['regular_price_zzp'] ) ) {

            update_post_meta( $product_id, 'regular_price_zzp', $_POST['regular_price_zzp']);

        } 



        if ( isset( $_POST['action_price_zzp'] ) ) {

            update_post_meta( $product_id, 'action_price_zzp', $_POST['action_price_zzp']);

        } 



    }


     /* Custom Pricing field for ZZP user variation product */

    function add_custom_price_to_variations( $loop, $variation_data, $variation ) {

        woocommerce_wp_text_input( array(

            'id' => 'regular_prz_zzp[' . $loop . ']',

            'class' => 'short wc_input_price',

            'label' => __( 'Reguliere prijs ZZP (€)', 'woocommerce' ),

            'value' => get_post_meta( $variation->ID, 'regular_prz_zzp', true )

        ));


        woocommerce_wp_text_input( array(

            'id' => 'action_prz_zzp[' . $loop . ']',

            'class' => 'short wc_input_price',

            'label' => __( 'Actieprijs ZZP (€)', 'woocommerce' ),

            'value' => get_post_meta( $variation->ID, 'action_prz_zzp', true )

        ));

    }
   

    /* Variation product custom pricing field value save */

    function save_custom_price_variations( $variation_id, $i ) {

        $regular_prz_zzp = $_POST['regular_prz_zzp'][$i];

        $action_prz_zzp = $_POST['action_prz_zzp'][$i];

        $profit_margin_euro = $_POST['profit_margin_euro'][$i];

        if ( isset( $regular_prz_zzp ) ) update_post_meta( $variation_id, 'regular_prz_zzp', esc_attr( $regular_prz_zzp ) );

        if ( isset( $action_prz_zzp ) ) update_post_meta( $variation_id, 'action_prz_zzp', esc_attr( $action_prz_zzp ) );

        if ( isset( $profit_margin_euro ) ) update_post_meta( $variation_id, 'profit_margin_euro', esc_attr( $profit_margin_euro ) );

    }



    /* Show Custom price for Variation Product */

    function add_custom_price_variation_data( $variations ) {

        $variations['regular_prz_zzp'] = '<div class="woocommerce_custom_field">Reguliere prijs ZZP (€): <span>' . get_post_meta( $variations[ 'variation_id' ], 'regular_prz_zzp', true ) . '</span></div>';

        $variations['action_prz_zzp'] = '<div class="woocommerce_custom_field">Actieprijs ZZP (€): <span>' . get_post_meta( $variations[ 'variation_id' ], 'action_prz_zzp', true ) . '</span></div>';

        return $variations;

    }





    /* Show Custom price for ZZP user simple product */

    function custom_price_product($price, $product) {



	    if (!is_user_logged_in()) return $price;

	    $pid = $product->get_id();

	    if ($this->has_role_deverfvakman('zzp')) {

	        if( $product->is_type( 'simple' ) ){

	        	$rpz = get_post_meta($pid, 'regular_price_zzp', true);

	        	$apz = get_post_meta($pid, 'action_price_zzp', true);

	        	if(empty($apz)){

	        		$cstptrz = $rpz;

	        	}else{

	        		$cstptrz = $apz;

	        	}



	            //$cstptrz = get_post_meta($pid, 'regular_price_zzp', true);

	            $searchForValue_act = ',';

	            $stringValue_act = (string) $cstptrz;



	            if( strpos($stringValue_act, $searchForValue_act) !== false ) {

	                $action_prz = str_replace(',', '.', $stringValue_act);

	                if(!empty($cstptrz)){

	                    $price   = $action_prz;    

	                }

	             }else{



                    if(!empty($cstptrz)){

                        $price   = number_format($cstptrz,2);

                    }

                 }

	        } 

	                

	    }

	   	return $price;

	}



	 function custom_price_product_regular_price($price, $product) {



	    if (!is_user_logged_in()) return $price;

	    $pid = $product->get_id();

	    if ($this->has_role_deverfvakman('zzp')) {

	        if( $product->is_type( 'simple' ) ){

	            $cstptrz = get_post_meta($pid, 'regular_price_zzp', true);

	            $searchForValue_act = ',';

	            $stringValue_act = (string) $cstptrz;



	            if( strpos($stringValue_act, $searchForValue_act) !== false ) {

	                $action_prz = str_replace(',', '.', $stringValue_act);

	                if(!empty($cstptrz)){

	                    $price   = $action_prz;    

	                }

	             } else {

                    if(!empty($cstptrz)){

                        $price   = number_format($cstptrz,2);

                    }

                 }

	        } 

	                

	    }

	   	return $price;

	}



	 function custom_price_product_sale_price($price, $product) {



	    if (!is_user_logged_in()) return $price;

	    $pid = $product->get_id();

	    if ($this->has_role_deverfvakman('zzp')) {

	        if( $product->is_type( 'simple' ) ){

	            $cstptrz = get_post_meta($pid, 'action_price_zzp', true);

                $searchForValue_act = ',';

	            $stringValue_act = (string) $cstptrz;



	            if( strpos($stringValue_act, $searchForValue_act) !== false ) {

	                $action_prz = str_replace(',', '.', $stringValue_act);

	                if(!empty($cstptrz)){

	                    $price   = $action_prz;    

	                }

	             }else {

                    if(!empty($cstptrz)){

                        $price   = number_format($cstptrz,2);

                    }

                 }

	        } 

	                

	    }



	   	return $price;

	}


    /* Variation hook */



    function filter_woocommerce_get_price( $price, $product ) { 

    	

     if (!is_user_logged_in()) return $price;

        $pid = $product->get_id();

        if ($this->has_role_deverfvakman('zzp')) {

           		$cstptrz = get_post_meta($pid, 'regular_prz_zzp', true);

           		$searchForValue = ',';

                $stringValue = (string) $cstptrz;



                if( strpos($stringValue, $searchForValue) !== false ) {

                    $regular_prz = str_replace(',', '.', $stringValue);

                    if(!empty($cstptrz)){

                        $price   = $regular_prz;    

                    }

                 } else {

                    if(!empty($cstptrz)){

                        $price   = number_format($cstptrz,2);

                    }

                 }

            }

     

        return $price; 

    } 







    /* Show Custom price for ZZP user variable product */

    function filter_woocommerce_get_regular_price( $price, $product ) { 

    	

     if (!is_user_logged_in()) return $price;

        $pid = $product->get_id();

        if ($this->has_role_deverfvakman('zzp')) {

           		$cstptrz = get_post_meta($pid, 'regular_prz_zzp', true);

           		$searchForValue = ',';

                $stringValue = (string) $cstptrz;



                if( strpos($stringValue, $searchForValue) !== false ) {

                    $regular_prz = str_replace(',', '.', $stringValue);

                    if(!empty($cstptrz)){

                        $price   = $regular_prz;    

                    }

                 } else {

                    if(!empty($cstptrz)){

                        $price   = number_format($cstptrz,2);

                    }

                 } 

            }

     

        return $price; 

    } 





    function filter_woocommerce_get_sale_price( $price, $product ) { 

        if (!is_user_logged_in()) return $price;

        $pid = $product->get_id();

        

		if ($this->has_role_deverfvakman('zzp')) {

        $cstptrz = get_post_meta($pid, 'action_prz_zzp', true);

        $searchForValue = ',';

		$stringValue = (string) $cstptrz;



            if( strpos($stringValue, $searchForValue) !== false ) {

                $regular_prz = str_replace(',', '.', $stringValue);

                if(!empty($cstptrz)){

                    $price   = $regular_prz;    

                }

             } else {

                    if(!empty($cstptrz)){

                        $price   = number_format($cstptrz,2);

                    }

            } 

        }

      

        return $price;

    }







    /*Get current user login */

    function has_role_deverfvakman($role = '',$user_id = null){

        if ( is_numeric( $user_id ) )

            $user = get_user_by( 'id',$user_id );

        else

            $user = wp_get_current_user();



        if ( empty( $user ) )

            return false;



        return in_array( $role, (array) $user->roles );

    }





    function yoast_breadcrumb() {

        if (function_exists('yoast_breadcrumb')) {

            yoast_breadcrumb('<p id="breadcrumbs">', '</p>');

        }

    }



    function woocommerce_our_choice_badge() {



        $our_choice = get_post_meta(get_the_ID(), '_our_choice', true);



        if ($our_choice == 'yes') {

            echo '<span class="ourchoice">' . __('Our Choice', 'maatwerkonline') . '</span>';

        }

    }



    function remove_woocommerce_currency_symbol($currency_symbol, $currency) {



        $currency_symbol = '';



        return $currency_symbol;

    }



    function custom_loop_shop_per_page($cols) {

        $cols = 44;

        return $cols;

    }



    // Ensure cart contents update when products are added to the cart via AJAX

    function custom_woocommerce_add_to_cart_fragments($fragments) {

        ob_start();

        ?>

        <a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('Go to your Cart', 'maatwerkonline'); ?>" class="nav-link cart-contents">

            <i class="dvv dvv-cart"></i><?php if (WC()->cart->get_cart_contents_count() > 0) : ?><span class="product-amount badge badge-primary"><?php echo WC()->cart->get_cart_contents_count(); ?></span><?php endif; ?>

        </a>



        <?php

        $fragments['a.cart-contents'] = ob_get_clean();



        return $fragments;

    }



    function custom_woocommerce_auto_complete_virtual_orders($order_status, $order_id) {

        $order = new WC_Order($order_id);



        if ('processing' == $order_status && ( 'on-hold' == $order->status || 'pending' == $order->status || 'failed' == $order->status )) {



            $virtual_order = null;



            if (count($order->get_items()) > 0) {



                foreach ($order->get_items() as $item) {



                    if ('line_item' == $item['type']) {



                        $_product = $order->get_product_from_item($item);



                        if (!$_product->is_virtual()) {

                            // once we've found one non-virtual product we know we're done, break out of the loop

                            $virtual_order = false;

                            break;

                        } else {

                            $virtual_order = true;

                        }

                    }

                }

            }



            // virtual order, mark as completed

            if ($virtual_order) {

                return 'completed';

            }

        }



        // non-virtual order, return original status

        return $order_status;

    }



    function custom_woocommerce_form_field_args($args, $key, $value) {



        $args['class'][] = 'form-group col-12';

        $args['label_class'] = 'form-control-label sr-only';

        $args['input_class'][] = 'form-control';



        if ($args['id'] == 'billing_first_name' || $args['id'] == 'shipping_first_name') {

            $args['placeholder'] = __('First Name', 'maatwerkonline');

            $args['class'][] = 'col-md-6';

        } elseif ($args['id'] == 'billing_last_name' || $args['id'] == 'shipping_last_name') {

            $args['placeholder'] = __('Last Name', 'maatwerkonline');

            $args['class'][] = 'col-md-6';

        } elseif ($args['id'] == 'billing_company' || $args['id'] == 'shipping_company') {

            $args['placeholder'] = __('Company (optional)', 'maatwerkonline');

        // $args['class'][] = 'mb-5';

        } elseif ($args['id'] == 'billing_country' || $args['id'] == 'shipping_country') {

            $args['placeholder'] = __('Country', 'maatwerkonline');

        } elseif ($args['id'] == 'billing_street_name' || $args['id'] == 'shipping_street_name') {

            $args['placeholder'] = __('Street', 'maatwerkonline');

            $args['class'][] = 'col-md-6';

        } elseif ($args['id'] == 'billing_house_number' || $args['id'] == 'shipping_house_number') {

            $args['placeholder'] = __('Housenumber', 'maatwerkonline');

            $args['class'][] = 'col-sm-6 col-md-3';

        } elseif ($args['id'] == 'billing_house_number_suffix' || $args['id'] == 'shipping_house_number_suffix') {

            $args['placeholder'] = __('Suffix', 'maatwerkonline');

            $args['class'][] = 'col-sm-6 col-md-3';

        } elseif ($args['id'] == 'billing_postcode' || $args['id'] == 'shipping_postcode') {

            $args['placeholder'] = __('Postcode', 'maatwerkonline');

            $args['class'][] = 'col-sm-4';

        } elseif ($args['id'] == 'billing_city' || $args['id'] == 'shipping_city') {

            $args['placeholder'] = __('City', 'maatwerkonline');

            $args['class'][] = 'col-sm-8';

        } elseif ($args['id'] == 'billing_email' || $args['id'] == 'shipping_email') {

            $args['placeholder'] = __('Email', 'maatwerkonline');

            $args['class'][] = 'col-md-6';

        } elseif ($args['id'] == 'billing_phone' || $args['id'] == 'shipping_phone') {

            $args['placeholder'] = __('Phone', 'maatwerkonline');

            $args['class'][] = 'col-md-6';

        } elseif ($args['id'] == 'billing_state' || $args['id'] == 'shipping_state') {

            $args['class'][] = 'd-none';

        } elseif ($args['id'] == 'billing_address_1' || $args['id'] == 'shipping_address_1') {

            $args['class'][] = 'd-none';

        } elseif ($args['id'] == 'billing_address_2' || $args['id'] == 'shipping_address_2') {

            $args['class'][] = 'd-none';

        }



        return $args;

    }



    public function custom_woocommerce_breadcrumb_defaults_args($default) {

        $default['delimiter'] = '>';

        $default['before'] = '<span>';

        $default['after'] = '</span>';

        return$default;

    }



    public function woocommerce_template_loop_rating() {

        global $product;

        $rating_count = $product->get_rating_count();

        $review_count = $product->get_review_count();

        $average = $product->get_average_rating();

        echo '<div class="woocommerce-product-rating after-title-rating">';

        if ($rating_count > 0) {

            $rating_per = $average * 20;

            ?>

            <div class="ratings clearfix">

                <div class="empty-stars">

                    <div class="full-stars" style="width:<?php echo $rating_per; ?>%"></div>

                </div>

                <div class="review-text"><?php echo $review_count; ?> reviews </div>

            </div>            

            <?php

        }

        echo '</div>';

        $product_usps = get_post_meta($product->get_id(), 'product_usps', true);

        echo '<div class="woocommerce-product-usps match-height-by-row">';

            if ($product_usps > 0) {

                echo '<ul>';

                for ($i = 0; $i < $product_usps; $i++) {

                    echo '<li>' . get_post_meta($product->get_id(), 'product_usps_' . $i . '_usps', true) . '</li>';

                }

                echo '<ul>';

            }

        echo '</div>';

    }

    /** 
     * Add custom profit margin fields for simple products
     */
    function mo_add_profit_margin_fields_simple() {

        woocommerce_wp_text_input( array( 
            'id'        => 'profit_margin_euro',
            'class'     => 'wc_input_price short',
            'label'     => __( 'Margin', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));
        woocommerce_wp_text_input( array(
            'id'        => 'profit_margin_percentage',
            'class'     => 'wc_input_price short',
            'label'     => __( 'Margin', 'woocommerce' ) . ' (%)',
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));

        woocommerce_wp_text_input( array( 
            'id'        => 'profit_margin_zzp_euro',
            'class'     => 'wc_input_price short',
            'label'     => __( 'Margin ZZP', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));
        woocommerce_wp_text_input( array(
            'id'        => 'profit_margin_zzp_percentage',
            'class'     => 'wc_input_price short',
            'label'     => __( 'Margin ZZP', 'woocommerce' ) . ' (%)',
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));
    }

    /** 
     * Save custom profit margin fields for simple products
     */
    function mo_save_profit_margin_simple(){

        global $post;

        update_post_meta($post->ID, 'profit_margin_euro', $_POST['profit_margin_euro']);
        update_post_meta($post->ID, 'profit_margin_percentage', $_POST['profit_margin_percentage']);
        update_post_meta($post->ID, 'profit_margin_zzp_euro', $_POST['profit_margin_zzp_euro']);
        update_post_meta($post->ID, 'profit_margin_zzp_percentage', $_POST['profit_margin_zzp_percentage']);
    }


    /** 
     * Add custom profit margin fields for variations
     */
    function mo_add_profit_margin_fields_variations( $loop, $variation_data, $variation ) {

        woocommerce_wp_text_input( array(
            'id' => 'profit_margin_euro[' . $loop . ']',
            'class' => 'short wc_input_price',
            'label' => __( 'Margin', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
            'value' => get_post_meta( $variation->ID, 'profit_margin_euro', true ),
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));

        woocommerce_wp_text_input( array(
            'id' => 'profit_margin_percentage[' . $loop . ']',
            'class' => 'short wc_input_price',
            'label' => __( 'Margin', 'woocommerce' ) . ' (%)',
            'value' => get_post_meta( $variation->ID, 'profit_margin_percentage', true ),
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));

        woocommerce_wp_text_input( array(
            'id' => 'profit_margin_zzp_euro[' . $loop . ']',
            'class' => 'short wc_input_price',
            'label' => __( 'Margin ZZP', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
            'value' => get_post_meta( $variation->ID, 'profit_margin_zzp_euro', true ),
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));

        woocommerce_wp_text_input( array(
            'id' => 'profit_margin_zzp_percentage[' . $loop . ']',
            'class' => 'short wc_input_price',
            'label' => __( 'Margin ZZP', 'woocommerce' ) . ' (%)',
            'value' => get_post_meta( $variation->ID, 'profit_margin_zzp_percentage', true ),
            'custom_attributes' => array(
                'readonly' => 'readonly',
            ),
        ));
    }

    /* Show Custom price for Variation Product */

    function mo_add_profit_margin_fields_variations_data( $variations ) {

        $variations['profit_margin_euro'] = '<div class="woocommerce_custom_field">Margin (€): <span>' . get_post_meta( $variations[ 'variation_id' ], 'profit_margin_euro', true ) . '</span></div>';
        $variations['profit_margin_percentage'] = '<div class="woocommerce_custom_field">Margin (%): <span>' . get_post_meta( $variations[ 'variation_id' ], 'profit_margin_percentage', true ) . '</span></div>';
        $variations['profit_margin_zzp_euro'] = '<div class="woocommerce_custom_field">Margin zzp (€): <span>' . get_post_meta( $variations[ 'variation_id' ], 'profit_margin_zzp_euro', true ) . '</span></div>';
        $variations['profit_margin_zzp_percentage'] = '<div class="woocommerce_custom_field">Margin zzp (%): <span>' . get_post_meta( $variations[ 'variation_id' ], 'profit_margin_zzp_percentage', true ) . '</span></div>';
        
        return $variations;
    }

    /** 
     * Save custom profit margin fields for simple variations
     */
    function mo_save_profit_margin_variations( $variation_id, $i ) {

        $profit_margin_euro = $_POST['profit_margin_euro'][$i];
        $profit_margin_percentage = $_POST['profit_margin_percentage'][$i];
        $profit_margin_zzp_euro = $_POST['profit_margin_zzp_euro'][$i];
        $profit_margin_zzp_percentage = $_POST['profit_margin_zzp_percentage'][$i];

        update_post_meta( $variation_id, 'profit_margin_euro', esc_attr( $profit_margin_euro ));
        update_post_meta( $variation_id, 'profit_margin_percentage', esc_attr( $profit_margin_percentage ));
        update_post_meta( $variation_id, 'profit_margin_zzp_euro', esc_attr( $profit_margin_zzp_euro ));
        update_post_meta( $variation_id, 'profit_margin_zzp_percentage', esc_attr( $profit_margin_zzp_percentage ));
    }
}

// End class_woocommerce Class

if (class_exists('class_woocommerce')) { // Installation and uninstallation hooks

    register_activation_hook(__FILE__, array('class_woocommerce', 'activate'));

    register_deactivation_hook(__FILE__, array('class_woocommerce', 'deactivate'));



    new class_woocommerce();

}



function wc_get_account_menu_item_a_classes($endpoint) {

    global $wp;



    // Set current item class.

    $current = isset($wp->query_vars[$endpoint]);

    if ('dashboard' === $endpoint && ( isset($wp->query_vars['page']) || empty($wp->query_vars) )) {

        $current = true; // Dashboard is not an endpoint, so needs a custom check.

    }



    if ($current) {

        $classes = 'active';

    }



    return $classes;

}



// Add Link (Tab) to My Account menu

function custom_offerte_aanvragen_woocommerce_account_menu_items($menu_links) {



    $menu_links = array_slice($menu_links, 0, 2, true) + array('offerte-aanvragen' => __('Offerte aanvragen', 'maatwerkonline')) + array_slice($menu_links, 2, NULL, true);

    $menu_links = array_slice($menu_links, 0, 2, true) + array('speciale-tarieven-aanvragen' => __('Speciale tarieven aanvragen', 'maatwerkonline')) + array_slice($menu_links, 2, NULL, true);



    return $menu_links;

}



add_filter('woocommerce_account_menu_items', 'custom_offerte_aanvragen_woocommerce_account_menu_items', 40);



// Register Permalink Endpoint

function custom_offerte_aanvragen_add_endpoint() {

    add_rewrite_endpoint('offerte-aanvragen', EP_PAGES);

}



add_action('init', 'custom_offerte_aanvragen_add_endpoint');



function custom_speciale_tarieven_aanvragen_add_endpoint() {

    add_rewrite_endpoint('speciale-tarieven-aanvragen', EP_PAGES);

}



add_action('init', 'custom_speciale_tarieven_aanvragen_add_endpoint');



// Content for the Bookmarks page in My Account, woocommerce_account_bookmarks_endpoint

function offerte_aanvragen_my_account_endpoint_content() {

    wc_get_template('myaccount/offerte-aanvragen.php');

}



add_action('woocommerce_account_offerte-aanvragen_endpoint', 'offerte_aanvragen_my_account_endpoint_content');



function speciale_tarieven_aanvragen_my_account_endpoint_content() {

    wc_get_template('myaccount/speciale-tarieven-aanvragen.php');

}



add_action('woocommerce_account_speciale-tarieven-aanvragen_endpoint', 'speciale_tarieven_aanvragen_my_account_endpoint_content');





/* ZZP user pricing on home */



add_filter('woocommerce_variable_sale_price_html', 'shop_variable_product_price', 10, 4);

add_filter('woocommerce_variable_price_html', 'shop_variable_product_price', 10, 2);



function shop_variable_product_price($price, $product) {

    if ( is_front_page() || is_archive() ) {

    

    $productdta = $product->get_children();

    $frtvariable =  $productdta[0];



    $regprz = get_post_meta($frtvariable, 'regular_prz_zzp', true);

    $saleprz = get_post_meta($frtvariable, 'action_prz_zzp', true);



    if(!empty($regprz)){

        $searchForValue = ',';

        $stringValue = (string) $regprz;



        if( strpos($stringValue, $searchForValue) !== false ) {

            $regular_prz = str_replace(',', '.', $stringValue);

        }

    }



    if(!empty($saleprz)){

        $searchForValue_sale = ',';

        $stringValue_sale = (string) $saleprz;



        if( strpos($stringValue_sale, $searchForValue_sale) !== false ) {

            $sale_prz = str_replace(',', '.', $stringValue_sale);

        }

    }



    if ( is_user_logged_in() ) {

        $user = new WP_User(get_current_user_id());

        $crtrole =  $user->roles[0];

        if($crtrole == 'zzp'){

            

            if (!empty($regular_prz) && !empty($sale_prz)) {

                $price = '<div class="price-align rt">

                            <span class="price d-block price-incl position-relative" style="top: 13px;"><span class="outer">

                            <del class="strike">

                            <span class="woocommerce-Price-amount amount">

                            <bdi>'.wc_price($regular_prz).'</bdi></span></del>

            

                            <ins class="highlight">

                            <span class="woocommerce-Price-amount amount">

                            <bdi></bdi>

                            </span>

                            </ins>

                            </span>

                            </span>

                            <div class="clear"></div>

                            <span class="price d-block price-excl"><span class="small text-right">Vanaf</span> <span class="woocommerce-Price-amount amount"><bdi>'.wc_price($sale_prz).'</bdi></span></span></div>';

            }elseif(!empty($regular_prz) && empty($sale_prz) ){

                $price = '<div class="price-align yt"><ins class="">' . wc_price($variation_min_reg_price) . '</ins>';

            }else{

                $variation_min_reg_price = $product->get_variation_regular_price('min', true);

                $variation_min_sale_price = $product->get_variation_sale_price('min', true);

                if ($product->is_on_sale() && !empty($variation_min_sale_price)) {

                if (!empty($variation_min_sale_price))

                $price = '<div class="price-align yu">

                            <span class="price d-block price-incl position-relative" style="top: 13px;"><span class="outer">

                            <del class="strike">

                            <span class="woocommerce-Price-amount amount">

                            <bdi>'.wc_price($variation_min_reg_price).'</bdi></span></del>

            

                            <ins class="highlight">

                            <span class="woocommerce-Price-amount amount">

                            <bdi></bdi>

                            </span>

                            </ins>

                            </span>

                            </span>

                            <div class="clear"></div>

                            <span class="price d-block price-excl"><span class="small text-right">Vanaf</span> <span class="woocommerce-Price-amount amount"><bdi>'.wc_price($variation_min_sale_price).'</bdi></span></span></div>';

                } else {

                if (!empty($variation_min_reg_price))

                $price = '<ins class="">' . wc_price($variation_min_reg_price) . '</ins>';

                else

                $price = '<ins class="">' . wc_price($product->regular_price) . '</ins>';

                } 

            } 

       

        } else {



            $variation_min_reg_price = $product->get_variation_regular_price('min', true);

            $variation_min_sale_price = $product->get_variation_sale_price('min', true);

            if ($product->is_on_sale() && !empty($variation_min_sale_price)) {

            if (!empty($variation_min_sale_price))

            $price = '<div class="price-align pl pk12">

                            <span class="price d-block price-incl position-relative" style="top: 13px;"><span class="outer">

                            <del class="strike">

                            <span class="woocommerce-Price-amount amount">

                            <bdi>'.wc_price($variation_min_reg_price).'</bdi></span></del>

            

                            <ins class="highlight">

                            <span class="woocommerce-Price-amount amount">

                            <bdi></bdi>

                            </span>

                            </ins>

                            </span>

                            </span>

                            <div class="clear"></div>

                            <span class="price d-block price-excl"><span class="small text-right">Vanaf</span> <span class="woocommerce-Price-amount amount"><bdi>'.wc_price($variation_min_sale_price).'</bdi></span></span></div>';

            } else {

            if (!empty($variation_min_reg_price))

            $price = '<ins class="">' . wc_price($variation_min_reg_price) . '</ins>';

            else

            $price = '<ins class="">' . wc_price($product->regular_price) . '</ins>';

            }



        }    

    

    } else {



        $variation_min_reg_price = $product->get_variation_regular_price('min', true);

        $variation_min_sale_price = $product->get_variation_sale_price('min', true);

        if ($product->is_on_sale() && !empty($variation_min_sale_price)) {

        if (!empty($variation_min_sale_price))

            $price = '<div class="price-align">

                            <span class="price d-block price-incl position-relative" style="top: 13px;"><span class="outer">

                            <del class="strike">

                            <span class="woocommerce-Price-amount amount">

                            <bdi>'.wc_price($variation_min_reg_price).'</bdi></span></del>

            

                            <ins class="highlight">

                            <span class="woocommerce-Price-amount amount">

                            <bdi></bdi>

                            </span>

                            </ins>

                            </span>

                            </span>

                            <div class="clear"></div>

                            <span class="price d-block price-excl"><span class="small text-right">Vanaf</span> <span class="woocommerce-Price-amount amount"><bdi>'.wc_price($variation_min_sale_price).'</bdi></span></span></div>';

        } else {

        if (!empty($variation_min_reg_price))

            $price = '<ins class="">' . wc_price($variation_min_reg_price) . '</ins>';

        else

            $price = '<ins class="">' . wc_price($product->regular_price) . '</ins>';

        }

        

    }

    echo $price;

}

    

}



add_action( 'woocommerce_before_calculate_totals', 'dev_alter_price_cart', 9999 );

function dev_alter_price_cart( $cart ) {

    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

    

    if ( is_user_logged_in() ) {

        $user = new WP_User(get_current_user_id());

        $crtrole =  $user->roles[0];

        if($crtrole == 'zzp'){

            foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {

                $product = $cart_item['data'];

                $pid = $product->get_id();



                if( !$product->is_type( 'simple' ) ) {

                    $actprz  =  get_post_meta($pid, 'action_prz_zzp', true);

                        $price = $product->get_price();

                    $cart_item['data']->set_price($actprz);

                }

            }

        }

    }

}



// Remove Metabox for plugin product-visibility-by-user-role-for-woocommerce

add_action( 'add_meta_boxes' , 'remove_product_visibility_by_user_role_for_woocommerce_meta_boxes', 40 );

function remove_product_visibility_by_user_role_for_woocommerce_meta_boxes() {

    remove_meta_box( 'alg-wc-product-visibility-by-user-role-meta-box',  'product', 'side');

}



add_filter('woocommerce_product_tabs', 'woo_reorder_tabs', 98);



function woo_reorder_tabs($tabs) {



    $tabs['description']['priority'] = 5;

    $tabs['additional_information']['priority'] = 10;

    $tabs['documenten']['priority'] = 15;

    $tabs['upsell_product']['priority'] = 20;

    $tabs['vragen_product']['priority'] = 24;

    $tabs['reviews']['priority'] = 30;



    return $tabs;

}



/**

 * Add a custom product data tab

 */

add_filter('woocommerce_product_tabs', 'woo_new_product_tab');



function woo_new_product_tab($tabs) {



    $tabs['documenten'] = array(

        'title' => __('Documenten', 'maatwerkonline'),

        'priority' => 30,

        'callback' => 'woo_documenten_tab_content'

    );





    $tabs['upsell_product'] = array(

        'title' => __('Dit heb je ook nodig', 'woocommerce'),

        'priority' => 30,

        'callback' => 'woo_upsell_product_tab_content'

    );





    $tabs['vragen_product'] = array(

        'title' => __('Vragen over dit product', 'woocommerce'),

        'priority' => 30,

        'callback' => 'woo_vragen_product_tab_content'

    );







    return $tabs;

}



function woo_documenten_tab_content() {



    global $post, $product;



    $thepostid = $post->ID;

    $pdf_title = get_field('title', $thepostid);

    $pdf_file = get_field('pdf_file', $thepostid);





    if (!empty($pdf_title) || !empty($pdf_file)) {



        echo '<h2>Documenten</h2>';

        echo '<p><a href="' . $pdf_file . '" class="wp-pdf" target="_blank">' . $pdf_title . '</a></p>';

    }

}



function woo_upsell_product_tab_content() {

    global $post, $product;

    $thepostid = $post->ID;

    $product_object = $thepostid ? wc_get_product($thepostid) : new WC_Product();

    $product_ids = $product_object->get_upsell_ids('edit');

    echo '<div class="wp-upsell col-12 slider">';

    echo '<h2>Dit heb je ook nodig</h2>';

    echo '  <div class="products carousel" data-accessibility="0" data-adaptiveheight="0" data-autoplay="1" data-autoplayspeed="7000" data-arrows="1" data-prevarrow="dvv dvv-arrow-left" data-nextarrow="dvv dvv-arrow-right" data-centermode="0" data-dots="0" data-effect="scroll" data-swipe="1" data-easing="linear" data-infinite="0" data-initialslide="1" data-pauseonhover="0" data-speed="300" data-variablewidth="0" data-slidestoscroll="1" data-slidestoshow="4" data-responsive="1" data-slidestoscroll_1190="1" data-slidestoshow_1190="2" data-slidestoscroll_768="1" data-slidestoshow_768="2" data-slidestoscroll_480="1" data-slidestoshow_480="1">';

    if($product_ids){

        foreach ($product_ids as $product_id) {

            $product = wc_get_product($product_id);
    
            if (is_object($product)) {
    
                $rating_count = $product->get_rating_count();
    
                $review_count = $product->get_review_count();
    
                $average = $product->get_average_rating();
    
                ?>
                
                <div class="dit-product  product type-product status-publish has-post-thumbnail product_cat-geen-categorie product_cat-grondverf-primer product_cat-muurverf first instock shipping-taxable purchasable product-type-simple">
                    <a href="<?php echo get_permalink($product->get_id()); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                        <figure class="image image-with-bg rounded d-flex align-items-center" style="height: 278px;">
                            <div>
                                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail'); ?>
                                <img src="<?php echo $image[0]; ?>" data-id="<?php echo $product_id; ?>">
                            </div>
                        </figure>
                        <h6 class="match-height-by-row product_loop_item_title height-name-product pb-1" style="height: 23px;"><strong><?php echo $product->get_name(); ?></strong></h6>
                        <?php
                            if ($rating_count > 0) {
                            $rating_per = $average * 20;
                        ?>
                        <div class="woocommerce-product-rating after-title-rating">
                            <div class="ratings clearfix">
                                <div class="empty-stars">
                                    <div class="full-stars" style="width:<?php echo $rating_per; ?>%"></div>
                                </div>
                                <div class="review-text"><?php echo $review_count; ?> reviews </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="woocommerce-product-usps adjust">
                            <ul>
                                <?php
                                    $product_usps = get_field('product_usps', $product_id);
                                    foreach ($product_usps as $key) {
                                ?>
                                    <li class="list-unstyled"><?php echo $key['usps']; ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php 
                        if ( $product->is_type( 'variable' ) ) {
                            global $woocommerce;
                            $min_sale_price = $product->get_variation_sale_price( 'min' , true);
                            $min_regular_price = $product->get_variation_regular_price( 'min' ,true);
                            $product->variable_product_sync();
                               
                            } else {
                                $min_sale_price = $product->get_sale_price();
                                $min_regular_price = $product->get_regular_price();
                                
                            }
                        ?>

                           <div class="price-align">
                                <span class="price d-block price-incl position-relative"><span class="outer"><?php echo wc_price($min_regular_price); ?></span></span>
                            <div class="clear"></div>
                            
                            <span class="price d-block price-excl"><span class="small text-right"><?php _e('From', 'maatwerkonline'); ?></span> <?php echo wc_price($min_sale_price); ?></span></div>
                            <div class="clear"></div>

                        <div class="clear"></div>
                    </a>
    
                    <a href="<?php echo get_permalink($product->get_id()); ?>" class="button product_type_simple add_to_cart_button ajax_add_to_cart float-right single_add_to_cart_button button alt">
    
                        <i class="dvv dvv-cart"></i>
    
                    </a>
    
                </div>
    
                <?php
    
            }
    
        }
    
        
        
    }

    echo '</div>';

    echo '</div>';

}



add_action('wp_ajax_custom_action', 'custom_action');

add_action('wp_ajax_nopriv_custom_action', 'custom_action');

function custom_action() {

    $productname = $_POST['wpproductname'];

    $message = $_POST['eq-content'];

    $name = $_POST['wpname'];

    $email = $_POST['wpemail'];

    $user_data = get_userdata(3); // Emailadres of the client's own account

    $client_email = $user_data->user_email;

    $to = $client_email;

    $subject = 'Enquiry Form';

    $body = 'Naam : ' . $name . '<br>';

    $body .= 'E-mailadres : ' . $email . '<br>';

    $body .= 'Vraag : ' . $message . '<br>';

    $body .= 'On product : ' . $productname . '<br>';

    $headers = array('Content-Type: text/html; charset=UTF-8', 'From:' . $email);



    if (wp_mail($to, $subject, $body, $headers)) {

        $data = array('status' => true, 'message' => 'send successfully');

    } else {

        $data = array('status' => false, 'message' => 'please try again');

    }



    echo json_encode($data);



    die();

}



add_filter('woocommerce_show_variation_price', function() {

    return true;

});



function order_received_item_thumbnail_image($item_name, $item, $is_visible) {

    // Targeting order received page only

    if (!is_wc_endpoint_url('order-received'))

        return $item_name;



    // Get the WC_Product object (from order item)

    $product = $item->get_product();



    if ($product->get_image_id() > 0) {

        $product_image = '<span style="float:left;display:block;width:56px;">' . $product->get_image(array(48, 48)) . '</span>';

        $item_name = $product_image . $item_name;

    }



    return $item_name;

}

add_filter('woocommerce_order_item_name', 'order_received_item_thumbnail_image', 10, 3);



// Hides attributes on the extra information 

function my_attribute_hider($attributes) {



    if (isset($attributes['pa_EAN'])) {

        unset($attributes['pa_EAN']);

    }



    return $attributes;

}

add_filter('woocommerce_get_product_attributes', 'my_attribute_hider');



function prefix_translate_text($translated_text) {



    if ('Search Results for' === $translated_text) {

        $translated_text = 'Zoekresultaten voor:';

    }



    return $translated_text;

}

add_filter('gettext', 'prefix_translate_text');



// Changes empty card message

function custom_empty_cart_message() {

    $html = '<p class=""cart-empty woocommerce-info"">';

    $html .= wp_kses_post(apply_filters('wc_empty_cart_message', __('Uw winkelwagen is momenteel leeg', 'maatwerkonline')));

    echo $html . '</p>';

}

remove_action('woocommerce_cart_is_empty', 'wc_empty_cart_message', 10);

add_action('woocommerce_cart_is_empty', 'custom_empty_cart_message', 10);



// Fix difficult translate solutions

$catalog_orderby_options = apply_filters('woocommerce_catalog_orderby', array(

    'categories' => 'categorieën',

    'show all categories' => 'toon alle categorieën',

));

add_filter('user_can_richedit', '__return_true');