<?php

class social_icons extends WP_Widget {

	function social_icons(){
		$widget_ops = array(
			'classname' => 'widget-social-icons',
			'description' => __('Social icons', 'onm_textdomain' ),
		);
		$control_ops = array(
			'id_base' => 'social-icons',
		);
		$this->WP_Widget('social-icons', __('Social Icons', 'onm_textdomain' ), $widget_ops, $control_ops);
	}

	function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		echo $before_widget;

		if($title){
			echo $before_title.''.$title.''.$after_title;
		}

		$social_networks = get_theme_mod( 'social_networks', array() );

		if ( ! empty( $social_networks ) ) : ?>
			<ul>
				<?php foreach ( $social_networks as $social_network ) :?>
					<?php if ($social_network['url'] != "info@ibr.nl") :?>
						<li class="social <?php echo esc_attr( $social_network['network'] ); ?>">
       				<a href="<?php echo esc_url_raw( $social_network['url'] ); ?>" target="_blank">
           			<i class="social-icons social-icons-<?php echo esc_attr( $social_network['network'] ); ?> mr-3"></i> <span><?php echo esc_attr( $social_network['network'] ); ?></soan>
        			</a>
       			</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>

		<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = array();
		$instance['title'] = strip_tags($new_instance['title']);

		return $instance;
	}


	function form($instance){
		$defaults = array('title' => __('Social Icons', 'onm_textdomain' ));
		$instance = wp_parse_args((array) $instance, $defaults);
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'onm_textdomain');?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<p>
			<?php sprintf( __( 'Make sure you fill in the right data in %s in your settings %s', 'onm_textdomain' ), '<a href="./customize.php" target="_blank">' , '</a>' ); ?>
		</p>

	<?php
	}
}


function social_icons_widget(){
	register_widget('social_icons');
}

add_action('widgets_init', 'social_icons_widget');
