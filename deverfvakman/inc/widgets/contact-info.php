<?php

class contact_info extends WP_Widget {

	function contact_info(){
		$widget_ops = array(
			'classname' => 'contact-info',
			'description' => __('Contact informations with icons', 'onlinemarketingnl' ),
		);
		$control_ops = array(
			'id_base' => 'contact-info',
		);
		$this->__construct('contact-info', __('Contact Info', 'onlinemarketingnl' ), $widget_ops, $control_ops);
	}

	function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$visiting_address_enabled = $instance[ 'visiting_address_enabled' ] ? 'true' : 'false';
		$post_address_enabled = $instance[ 'post_address_enabled' ] ? 'true' : 'false';
		$phone_enabled = $instance[ 'phone_enabled' ] ? 'true' : 'false';
		$mobile_enabled = $instance[ 'mobile_enabled' ] ? 'true' : 'false';
		$mail_enabled = $instance[ 'mail_enabled' ] ? 'true' : 'false';
		$vat_enabled = $instance[ 'vat_enabled' ] ? 'true' : 'false';
		$coc_enabled = $instance[ 'coc_enabled' ] ? 'true' : 'false';
		$bank_account_enabled = $instance[ 'bank_account_enabled' ] ? 'true' : 'false';

		echo $before_widget;

		if($title == get_bloginfo( 'name' ) ){
			echo '<span itemscope itemtype="https://schema.org/Organization">'.$before_title.'<span itemprop="legalName">'.$title.'</span>'.$after_title;
		} elseif($title != '') {
			echo $before_title.''.$title.''.$after_title.'<span itemscope itemtype="https://schema.org/Organization">';
		}

			$visiting_address = get_theme_mod( 'visiting_address' );
			$visiting_zipcode = get_theme_mod( 'visiting_zipcode' );
			$visiting_city = get_theme_mod( 'visiting_city' );
			$post_address = get_theme_mod( 'post_address' );
			$post_zipcode = get_theme_mod( 'post_zipcode' );
			$post_city = get_theme_mod( 'post_city' );
			$phone_number = get_theme_mod( 'phone_number' );
			$mobile_number = get_theme_mod( 'mobile_number' );
			$email = get_theme_mod( 'email' );
			$vat = get_theme_mod( 'vat' );
			$coc = get_theme_mod( 'coc' );
			$bank_account = get_theme_mod( 'bank_account' ); ?>

			<?php if ($visiting_address_enabled == 'true' && ($visiting_address != '' || $visiting_zipcode != '' || $visiting_city != '' ) ) { ?><p class="visiting-address"><span itemprop="location" itemscope itemtype="https://schema.org/Place"><span itemprop="address" itemscope itemtype="https://schema.org/PostalAddress"><span class="street-address" itemprop="streetAddress"><?php echo esc_attr($visiting_address); ?></span></br><span class="postal-code" itemprop="postalCode"><?php echo esc_attr($visiting_zipcode); ?></span> <span class="locality" itemprop="addressLocality"><?php echo esc_attr($visiting_city); ?></span></span></span></p><?php }; ?>
			<?php if ($post_address_enabled == 'true' && ($post_address != '' || $post_zipcode != '' || $post_city != '' ) ) { ?><p class="post-address"><span itemprop="address" itemscope itemtype="https://schema.org/PostalAddress"><span class="street-address" itemprop="streetAddress"><?php echo esc_attr($post_address); ?></span></br><span class="postal-code" itemprop="postalCode"><?php echo esc_attr($post_zipcode); ?></<span> <span class="locality" itemprop="addressLocality"><?php echo esc_attr($post_city); ?></span></span></p><?php }; ?>
			<?php if ($phone_number != '' || $mobile_number != '' || $email != '' ) { ?><p class="phone-email"><?php }; if ($phone_enabled == 'true' && $phone_number != '' ) { ?><a href="tel:<?php echo esc_attr($phone_number); ?>" onclick="ga('send', 'event', { eventCategory: 'call', eventAction: 'call'});">T: <span itemprop="telephone"><?php echo esc_attr($phone_number); ?></span></a><?php }; ?>
			<?php if ($mobile_enabled == 'true' && $mobile_number != '' ) { ?></br><a href="tel:<?php echo esc_attr($mobile_number); ?>" onclick="ga('send', 'event', { eventCategory: 'call', eventAction: 'call'});"><i class="dvv dvv-smartphone"></i><span itemprop="telephone"><?php echo esc_attr($mobile_number); ?></span></a><?php }; ?>
			<?php if ($mail_enabled == 'true' && $email != '' ) { ?></br><a href="mailto:<?php echo esc_attr($email); ?>" onclick="ga('send', 'event', { eventCategory: 'mail', eventAction: 'mail'});">E: <meta itemprop="email" content="<?php echo $email; ?>"><span><?php echo $email; ?></span></a><?php }; if ($phone_number != '' || $mobile_number != '' || $email != '' ) { ?></p><?php }; ?>
			<?php if ($vat != '' || $coc != '' || $bank_account != '' ) { ?><p class="registration-numbers"><?php }; if ($vat_enabled == 'true' && $vat != '' ) { ?><?php _e('VAT', 'onlinemarketingnl');?>: <?php echo esc_attr($vat); ?></br><?php }; ?>
			<?php if ($coc_enabled == 'true' && $coc != '' ) { ?><?php _e('KVK', 'onlinemarketingnl');?>: <?php echo esc_attr($coc); ?></br><?php }; ?>
			<?php if ($bank_account_enabled == 'true' && $bank_account != '' ) { ?><?php _e('Bank Account', 'onlinemarketingnl');?>: <?php echo esc_attr($bank_account); ?><?php }; if ($vat != '' || $coc != '' || $bank_account != '' ) { ?></p><?php }; ?>


		</span>
		<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = array();
		$instance['title'] = strip_tags($new_instance['title']);
		$instance[ 'visiting_address_enabled' ] = $new_instance[ 'visiting_address_enabled' ];
		$instance[ 'post_address_enabled' ] = $new_instance[ 'post_address_enabled' ];
		$instance[ 'phone_enabled' ] = $new_instance[ 'phone_enabled' ];
		$instance[ 'mobile_enabled' ] = $new_instance[ 'mobile_enabled' ];
		$instance[ 'mail_enabled' ] = $new_instance[ 'mail_enabled' ];
		$instance[ 'vat_enabled' ] = $new_instance[ 'vat_enabled' ];
		$instance[ 'coc_enabled' ] = $new_instance[ 'coc_enabled' ];
		$instance[ 'bank_account_enabled' ] = $new_instance[ 'bank_account_enabled' ];

		return $instance;
	}


	function form($instance){
		$defaults = array('title' => get_bloginfo( 'name' ));
		$instance = wp_parse_args((array) $instance, $defaults);
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title', 'onlinemarketingnl');?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'visiting_address_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'visiting_address_enabled' ); ?>" name="<?php echo $this->get_field_name( 'visiting_address_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'visiting_address_enabled' ); ?>"><?php _e('Show Visiting Address', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'post_address_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'post_address_enabled' ); ?>" name="<?php echo $this->get_field_name( 'post_address_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'post_address_enabled' ); ?>"><?php _e('Show Post Address', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'phone_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'phone_enabled' ); ?>" name="<?php echo $this->get_field_name( 'phone_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'phone_enabled' ); ?>"><?php _e('Show Phone Number', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'mobile_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'mobile_enabled' ); ?>" name="<?php echo $this->get_field_name( 'mobile_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'mobile_enabled' ); ?>"><?php _e('Show Mobile Number', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'mail_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'mail_enabled' ); ?>" name="<?php echo $this->get_field_name( 'mail_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'mail_enabled' ); ?>"><?php _e('Show Mail Address', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'vat_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'vat_enabled' ); ?>" name="<?php echo $this->get_field_name( 'vat_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'vat_enabled' ); ?>"><?php _e('Show VAT Number', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'coc_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'coc_enabled' ); ?>" name="<?php echo $this->get_field_name( 'coc_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'coc_enabled' ); ?>"><?php _e('Show COC Number', 'onlinemarketingnl');?></label>
		</p>
		<p>
    		<input class="checkbox" type="checkbox" <?php checked( $instance[ 'bank_account_enabled' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'bank_account_enabled' ); ?>" name="<?php echo $this->get_field_name( 'bank_account_enabled' ); ?>" />
    		<label for="<?php echo $this->get_field_id( 'bank_account_enabled' ); ?>"><?php _e('Show Bank Account Number', 'onlinemarketingnl');?></label>
		</p>
		<p>
			<?php _e('Make sure you fill in the right data <a href="./customize.php" target="_blank">in your settings</a>', 'onlinemarketingnl');?>
		</p>

	<?php
	}
}


function contact_info_widget(){
	register_widget('contact_info');
}

add_action('widgets_init', 'contact_info_widget');
