<?php
class bootstrap_default_walker_nav_menu extends Walker_Nav_Menu {

	public function display_element($el, &$children, $max_depth, $depth = 0, $args, &$output){
		$id = $this->db_fields['id'];

		parent::display_element($el, $children, $max_depth, $depth, $args, $output);
	}

	// add classes to ul sub-menus
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// depth dependent classes
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$display_depth = ( $depth + 1); // because it counts the first submenu as 0

		$depth_classes = array(
			'dropdown-menu dropdown-menu-right',
			'menu-depth-' . $display_depth
		);
		$depth_class_names = implode( ' ', $depth_classes );
		// passed classes
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		// build html
		if(is_product()):
			$output .= "\n" . $indent . '<ul class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '" aria-labelledby="nav-menu-item-'. esc_attr($item->ID) . '">' . "\n";
		else:
			$output .= "\n" . $indent . '<ul class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '">' . "\n";
		endif;
		
		
	}

	// add main/sub classes to li's and links
 	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

		// passed classes
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = (in_array('current-menu-item', $classes)) ? 'active' : '';
		$classes[] = (in_array('menu-item-has-children', $classes)) ? 'dropdown' : '';
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
		$is_mega_menu = (strpos($class_names,'mega') !== false) ? true : false;
		$mobile_dropdown = (strpos($class_names,'mobile-dropdown') !== false) ? true : false;
		$is_sidebar = (strpos($class_names,'menu-sidebar') !== false) ? true : false;
		$is_navbar_divider = (strpos($class_names,'navbar-divider') !== false) ? true : false;
		$is_dropdown_divider = (strpos($class_names,'dropdown-divider') !== false) ? true : false;
		$is_title = (strpos($class_names,' title') !== false) ? true : false;
		$no_title = (strpos($class_names,'no-title') !== false) ? true : false;
		$is_description = (strpos($class_names,'description') !== false) ? true : false;

		// depth dependent classes
		$depth_classes = array(
			( $depth == 0 ? 'nav-item' : '' ),
			( $depth >=1 ? 'dropdown-item' : '' ),
			'nav-item-depth-' . $depth
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

		// build html
		if ($is_navbar_divider){
			$output .= $indent . '<li class="navbar-divider">';
		} elseif ($is_dropdown_divider){
			$output .= $indent . '<li class="dropdown-divider">';
		} else {
			$output .= $indent . '<li class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '">';
		}

		// link attributes
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="' . (($item->url[0] == "#" && !is_front_page()) ? esc_url(home_url()) : '') . esc_attr($item->url) .'"' : '';
		$attributes .= ( in_array( 'menu-item-has-children', $classes ) ? ' id="nav-menu-item-'. esc_attr($item->ID) . '" aria-haspopup="true" aria-expanded="false"' : '' );
		
		// link passed classes
		$button = get_field('button_enable', $item);
		$button_outline = get_field('button_outline', $item);
		$button_color = get_field('button_color', $item);
		$link_classes[] = ($button) ? 'btn '.$button[0] . $button_outline[0] . $button_color : 'nav-link';
		$link_classes[] = ($button_color) ? $button_color : '';
		$link_classes[] = (in_array( 'menu-item-has-children', $classes)) ? 'dropdown-toggle' : '';
		$link_class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $link_classes ), $item ) ) );
			
		// build html
		$title_output = ($is_title) ? $item->title : '';
		$description_output = ($is_description) ? $item->description : '';
		$item_output = '<a class="' . $link_class_names . '" '. $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</span></a>'.$description_output;

		if ($is_sidebar) {
			ob_start();
			dynamic_sidebar($item->description);
			$sidebar_html = ob_get_clean();

			// build html
			$sidebar_output = '<div class="sidebar-menu-item">'.$sidebar_html.'</div>';
			$item_output = $sidebar_output;

		} elseif ($is_title){

			// build html
			$item_output = '<span class="' . $link_class_names . '" '. $attributes . '>' . $title_output . '</span>';

		} elseif ($is_navbar_divider || $is_dropdown_divider){

			// build html
			$item_output = '';

		} elseif ($is_description){

			// build html
			$item_output = (!$no_title) ? '<a class="' . $link_class_names . '" '. $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>'. $description_output : '<a class="' . $link_class_names . '" '. $attributes . '>'. $description_output . '</a>';

		} else{
			
			// build html
			$item_output = (!$no_title) ? '<a class="' . $link_class_names . '" '. $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>'. $description_output : $description_output;
		}

		// build html
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		$is_mega_menu = (strpos($class_names,'mega') !== false) ? true : false;

    	$output .= "</li>\n";
	}

}

class bootstrap_footer_walker_nav_menu extends Walker_Nav_Menu {

	public function display_element($el, &$children, $max_depth, $depth = 0, $args, &$output){
		$id = $this->db_fields['id'];

		parent::display_element($el, $children, $max_depth, $depth, $args, $output);
	}

	// add classes to ul sub-menus
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// passed classes
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		// build html
		$output .= "\n" . $indent . '<ul class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '" aria-labelledby="nav-menu-item-'. esc_attr($item->ID) . '">' . "\n";
	}

	// add main/sub classes to li's and links
 	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

		// passed classes
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = (in_array('current-menu-item', $classes)) ? 'active' : '';
		if ($is_navbar_divider){
			$classes[] .= 'navbar-divider';
		} elseif ($is_description){
			$classes[] .= 'description';
		}
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
		$is_navbar_divider = (strpos($class_names,'navbar-divider') !== false) ? true : false;

		// depth dependent classes
		$depth_classes = array(
			( $depth == 0 ? 'nav-link' : '' ),
			( $depth >=1 ? 'nav-link' : '' )
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

		// build html
		$output .= $indent . '<li class="' . esc_attr($depth_class_names) . ' ' . esc_attr($class_names) . '">';

		// link attributes
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="' . (($item->url[0] == "#" && !is_front_page()) ? esc_url(home_url()) : '') . esc_attr($item->url) .'"' : '';

		// build html
		$title_output = ($is_title) ? $item->title : '';
		$item_output = (!$no_title) ? '<a ' . $attributes . '><span>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</span></a>'.$description_output : $description_output;

		if ($is_navbar_divider || $is_dropdown_divider){

			// build html
			$item_output = '';

		} else{
			// build html
			$item_output = '<a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>';

		}

		// build html
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

    	$output .= "</li>\n";
	}

}

//Sidebars in Menu
class sidebars_custom_menu {
	public function add_nav_menu_meta_boxes() {
		add_meta_box( 'sidebar_menu_add', __('Add Sidebar', 'maatwerkonline'), array( $this, 'nav_menu_link'), 'nav-menus', 'side', 'low' );
	}

	public function nav_menu_link() {?>
		<div id="posttype-sidebars" class="posttypediv">
			<div id="tabs-panel-sidebars" class="tabs-panel tabs-panel-active">
				<ul id ="sidebars-checklist" class="categorychecklist form-no-clear">
					<?php
						$i = -1;
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
							ob_start();
							dynamic_sidebar($sidebar['id']);
							$sidebar_html = ob_get_clean();
							?>
		        			<li>
		        				<label class="menu-item-title">
		        					<input type="checkbox" class="menu-item-checkbox" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-object-id]" value="<?php echo $sidebar['id']; ?>"> <?php echo ucwords( $sidebar['name'] ); ?>
		        				</label>
		        				<input type="hidden" class="menu-item-type" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-type]" value="custom">
		        				<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-attr-title]" value="<?php echo ucwords( $sidebar['name'] ); ?>">
								<input type="hidden" class="menu-item-title" name="menu-item[<?php echo esc_attr( $i ); ?>][menu-item-title]" value="&nbsp;" />
		        				<input type="hidden" class="menu-item-url" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-url]" value="">
		        				<input type="hidden" class="menu-item-classes" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-classes]" value="menu-sidebar">
		        				<input type="hidden" class="menu-item-description" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-description]" value="<?php echo $sidebar['id']; ?>">
		        			</li>
							<?php
							$i --;
						}
					?>
        		</ul>
        	</div>
        	<p class="button-controls">
        		<span class="list-controls">
        			<a href="<?php echo admin_url( 'nav-menus.php?page-tab=all&amp;selectall=1#posttype-sidebars' ); ?>" class="select-all"><?php _e('Select All', 'maatwerkonline'); ?></a>
        		</span>
        		<span class="add-to-menu">
        			<input type="submit" class="button-secondary submit-add-to-menu right" value="<?php _e('Add to Menu', 'maatwerkonline');?>" name="add-post-type-menu-item" id="submit-posttype-sidebars">
					<span class="spinner"></span>
				</span>
			</p>
		</div>
   <?php }
}
$custom_nav = new sidebars_custom_menu;
add_action('admin_init', array($custom_nav, 'add_nav_menu_meta_boxes'));

// Divider Menu
class bootstrap_custom_menu {
	public function add_nav_menu_meta_boxes() {
		add_meta_box( 'bootstrap_menu_add', __('Add Bootstrap Items', 'maatwerkonline'), array( $this, 'nav_menu_link'), 'nav-menus', 'side', 'low' );
	}

	public function nav_menu_link() {?>
		<div id="posttype-bootstrap" class="posttypediv">
			<div id="tabs-panel-bootstrap" class="tabs-panel tabs-panel-active">
				<ul id ="bootstrap-checklist" class="categorychecklist form-no-clear">
					<?php
						$i = -1;
						?>
		        		<li>
		        			<label class="menu-item-title">
		        				<input type="checkbox" class="menu-item-checkbox" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-object-id]" value="<?php echo esc_attr($i); ?>"> <?php _e('Navbar Divider', 'maatwerkonline'); ?>
		        			</label>
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-attr-title]" value="<?php _e('Navbar Divider', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-title]" value="<?php _e('Navbar Divider', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-type" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-type]" value="custom">
		        			<input type="hidden" class="menu-item-classes" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-classes]" value="navbar-divider">
		        		</li>
		        		<li>
		        			<label class="menu-item-title">
		        				<input type="checkbox" class="menu-item-checkbox" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-object-id]" value="<?php echo esc_attr($i); ?>"> <?php _e('Dropdown Divider', 'maatwerkonline'); ?>
		        			</label>
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-attr-title]" value="<?php _e('Dropdown Divider', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-title]" value="<?php _e('Dropdown Divider', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-type" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-type]" value="custom">
		        			<input type="hidden" class="menu-item-classes" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-classes]" value="dropdown-divider">
		        		</li>
		        		<li>
		        			<label class="menu-item-title">
		        				<input type="checkbox" class="menu-item-checkbox" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-object-id]" value="<?php echo esc_attr($i); ?>"> <?php _e('Title', 'maatwerkonline'); ?>
		        			</label>
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-attr-title]" value="<?php _e('Title', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-title]" value="<?php _e('Title', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-type" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-type]" value="custom">
		        			<input type="hidden" class="menu-item-classes" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-classes]" value="title">
		        		</li>
		        		<li>
		        			<label class="menu-item-title">
		        				<input type="checkbox" class="menu-item-checkbox" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-object-id]" value="<?php echo esc_attr($i); ?>"> <?php _e('Description', 'maatwerkonline'); ?>
		        			</label>
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-attr-title]" value="<?php _e('Description', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-attr-title" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-title]" value="<?php _e('Description', 'maatwerkonline'); ?>">
		        			<input type="hidden" class="menu-item-type" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-type]" value="custom">
		        			<input type="hidden" class="menu-item-classes" name="menu-item[<?php echo esc_attr($i); ?>][menu-item-classes]" value="description">
		        		</li>
		        		<?php
						$i --;
					?>
        		</ul>
        	</div>
        	<p class="button-controls">
        		<span class="list-controls">
        			<a href="<?php echo admin_url( 'nav-menus.php?page-tab=all&amp;selectall=1#posttype-bootstrap' ); ?>" class="select-all"><?php _e('Select All', 'maatwerkonline'); ?></a>
        		</span>
        		<span class="add-to-menu">
        			<input type="submit" class="button-secondary submit-add-to-menu right" value="<?php _e('Add to Menu', 'maatwerkonline');?>" name="add-post-type-menu-item" id="submit-posttype-bootstrap">
					<span class="spinner"></span>
				</span>
			</p>
		</div>
   <?php }
}
$custom_nav = new bootstrap_custom_menu;
add_action('admin_init', array($custom_nav, 'add_nav_menu_meta_boxes'));

// Allow HTML descriptions in WordPress Menu
remove_filter( 'nav_menu_description', 'strip_tags' );
function nav_menu_description_item( $menu_item ) {
    if ( isset( $menu_item->post_type ) ) {
        if ( 'nav_menu_item' == $menu_item->post_type ) {
            $menu_item->description = apply_filters( 'nav_menu_description', $menu_item->post_content );
        }
    }

    return $menu_item;
}
add_filter( 'wp_setup_nav_menu_item', 'nav_menu_description_item' );

add_action( 'init', 'on_init' );
add_action( 'admin_init', 'on_admin_init' );
	
/*
* Code to run at init.
* This doesn't really need to be here but I have it in every plugin.
*
	 */
	function on_init(){
		add_filter( 'walker_nav_menu_start_el', 'walker_nav_menu_start_el', 1, 4 );
	}
	
	/*
	 * Code to run at admin_init.
	 *
	 */
	function on_admin_init(){
		add_nav_menu_meta_box();
	}
	
	/*
	 * Add the search meta box to the left on the nav-menus.php page.
	 *
	 */
	function add_nav_menu_meta_box(){
		global $pagenow;
		if ( 'nav-menus.php' !== $pagenow ){
            return;
		}
		
		add_meta_box(
			'bop_nav_search_box_item_meta_box'
			,_x( 'Search Box', 'meta-box-title', 'maatwerkonline' )
			,'search_meta_box_render'
			,'nav-menus'
			,'side'
			,'low'
		);
	}
	
	/*
	 * Fill the meta box with the required html.
	 *
	 */
	function search_meta_box_render(){
		global $_nav_menu_placeholder, $nav_menu_selected_id;

		$_nav_menu_placeholder = 0 > $_nav_menu_placeholder ? $_nav_menu_placeholder - 1 : -1;

		?>
		<div class="customlinkdiv" id="searchboxitemdiv">
			<div class="tabs-panel-active">
				<ul class="categorychecklist">
					<li>
						<input type="hidden" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-type]" value="search">
						<input type="hidden" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-type_label]" value="<?php echo _x( 'Search Box', 'type-label', 'maatwerkonline' ) ?>">
						
						<input type="hidden" class="menu-item-title" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-title]" value="<?php echo _x( 'Search', 'default-title', 'maatwerkonline' ) ?>">
						<input type="hidden" class="menu-item-url" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-url]" value="<?php echo esc_attr( get_search_link() ); ?>">
						<input type="hidden" class="menu-item-classes" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-classes]" value="search">
						
						<input type="checkbox" class="menu-item-object-id" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-object-id]" value="<?php echo $_nav_menu_placeholder; ?>" checked="true">
					</li>
				</ul>
			</div>

			<p class="button-controls">
				<span class="add-to-menu">
					<input type="submit"<?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary right" value="<?php echo esc_attr_x( 'Add to menu', 'meta-box-submit', 'maatwerkonline' ); ?>" name="add-search-menu-item" id="submit-searchboxitemdiv">
					<span class="spinner"></span>
				</span>
			</p>
		</div>
		<script type="text/javascript">
			(function($){
				$(window).on('load', function(){
					$('#submit-searchboxitemdiv').on('click', function(e){
						e.preventDefault();
						$('#searchboxitemdiv').addSelectedToMenu();
					});
				});
			})(jQuery);
		</script>
		<style type="text/css">#searchboxitemdiv .tabs-panel-active{height:0;} #searchboxitemdiv .tabs-panel-active .menu-item-object-id{visibility:hidden;} #searchboxitemdiv p.howto{transition: max-height 2s;overflow: hidden;} #searchboxitemdiv p.howto{max-height: 0px;} #searchboxitemdiv p.howto.maatwerkonline-showing{max-height: 500px;}</style>
		<?php
	}
	
	/*
	 * Function to output the search form in a menu. Includes actions for site
	 * owners, themes, etc., to vary the html.
	 *
	 */
	function walker_nav_menu_start_el( $item_output, $item, $depth, $args ){
		if( $item->type != 'search' ){
			return $item_output;
		}
		
		/**
		 * This filter is defined in wp-includes/general-template.php
		 * 
		 * @since 1.0.0
		 */
		do_action( 'pre_get_search_form' );
		
		$format = current_theme_supports( 'html5', 'search-form' ) ? 'html5' : 'xhtml';
		
		/**
		 * This filter is defined in wp-includes/general-template.php
		 * 
		 * @since 1.0.0
		 */
		$format = apply_filters( 'search_form_format', $format );
		
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		
		/**
		 * This filter is defined in wp-includes/general-template.php
		 * 
		 * @since 1.0.0
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
		
		/**
		 * This filter is defined in wp-includes/general-template.php
		 * 
		 * @since 1.0.0
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
		
		$item_output = $args->before;
		
		ob_start();
		
		if( 'html5' == $format ):
		?>
		<form <?php echo $id . $class_names ?> role="search" method="get" action="<?php echo home_url( '/' ); ?>">
			<label>
				<?php 
				/**
				 * The screen reader text to show before the input box, if any.
				 *
				 * @since 1.4.0
				 *
				 * @param string	html 	The screen reader text to show. (Filtered)
				 * @param object 	$item 	Menu item data from db as std_object.
				 * @param int 		$depth	How many times menu should be prefixed with sub at this point.
				 * @param array		$args	Arguments passed to wp_nav_menu.
				 */
				echo apply_filters( 
					'bop_nav_search_screen_reader_text',
					'<span class="screen-reader-text">' . esc_attr_x( 'Search', 'form-submit-button', 'maatwerkonline' ) . '</span>',
					$item,
					$depth,
					$args
				) ?>
				<?php 
				/**
				 * Filter output of item attr_title/placeholder.
				 *
				 * @since 1.4.0
				 *
				 * @param bool	 	$item->title	Placeholder. (Filtered)
				 * @param object 	$item 			Menu item data from db as std_object.
				 * @param int 		$depth			How many times menu should be prefixed with sub at this point.
				 * @param array		$args			Arguments passed to wp_nav_menu.
				 */
				$attr_title = esc_attr( apply_filters( 'bop_nav_search_the_attr_title', $item->attr_title, $item, $depth, $args ) );
				?>
				<input type="search" class="search-field" placeholder="<?php echo $attr_title ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo $attr_title ?>" />
			</label>
			<?php 
			/**
			 * Determine whether to show the submit button.
			 *
			 * @since 1.4.0
			 *
			 * @param bool	 	true	Show the submit button? (Filtered)
			 * @param object 	$item 	Menu item data from db as std_object.
			 * @param int 		$depth	How many times menu should be prefixed with sub at this point.
			 * @param array		$args	Arguments passed to wp_nav_menu.
			 */
			if( apply_filters( 'bop_nav_search_show_submit_button', true, $item, $depth, $args ) ): ?>
				<input type="submit" class="search-submit" value="<?php 
					/**
					 * Filter output of item title.
					 *
					 * @since 1.4.0
					 *
					 * @param bool	 	$item->title	Navigation label (as in admin). (Filtered)
					 * @param object 	$item 			Menu item data from db as std_object.
					 * @param int 		$depth			How many times menu should be prefixed with sub at this point.
					 * @param array		$args			Arguments passed to wp_nav_menu.
					 */
					echo esc_attr( apply_filters( 'bop_nav_search_the_title', $item->title, $item, $depth, $args ) );
				?>" />
			<?php endif ?>
		</form>
		<?php else: ?>
		<div class="widget_search">
			<form <?php echo $id . $class_names ?> role="search" method="get" action="<?php echo home_url( '/' ); ?>">
				<?php 
				/**
				 * The screen reader text to show before the input box, if any.
				 *
				 * @since 1.4.0
				 *
				 * @param string	html 	The screen reader text to show. (Filtered)
				 * @param object 	$item 	Menu item data from db as std_object.
				 * @param int 		$depth	How many times menu should be prefixed with sub at this point.
				 * @param array		$args	Arguments passed to wp_nav_menu.
				 */
				echo apply_filters(
					'bop_nav_search_screen_reader_text',
					'',
					$item,
					$depth,
					$args
				) ?>
				<input name="s" id="s" type="text" placeholder="<?php esc_attr_e('Search', 'maatwerkonline'); ?>" value="<?php echo get_search_query() ?>"/>
				<?php 
				/**
				 * Determine whether to show the submit button.
				 *
				 * @since 1.4.0
				 *
				 * @param bool	 	true	Show the submit button? (Filtered)
				 * @param object 	$item 	Menu item data from db as std_object.
				 * @param int 		$depth	How many times menu should be prefixed with sub at this point.
				 * @param array		$args	Arguments passed to wp_nav_menu.
				 */
				if( apply_filters( 'bop_nav_search_show_submit_button', true, $item, $depth, $args ) ): ?>
					<a class="submit"><i class="ci_icon-search"></i></a>
				<?php endif ?>
			</form>
		</div>
		<?php
		endif;
		
		$item_output .= ob_get_clean();
		
		$item_output .= $args->after;
		
		/**
		 * Filter the HTML output of the search form.
		 *
		 * @since 1.0.0
		 *
		 * @param string 	$item_output	The search form HTML output. (Filtered)
		 * @param object 	$item 			Menu item data from db as std_object.
		 * @param int 		$depth			How many times menu should be prefixed with sub at this point.
		 * @param array		$args			Arguments passed to wp_nav_menu.
		 */
		return apply_filters( 'get_nav_search_box_form', $item_output, $item, $depth, $args );
	}

/*
 * This function calls the class singleton defined above.
 * This means the class above behaves like a module or
 * namespace for procedural programming purposes and runs or
 * queues a number of once only pieces of code on
 * instantiation.
 *
 */
$bop_nav_search_box_item;
function bop_nav_search_box_item(){
	global $bop_nav_search_box_item;
	if( ! ( $bop_nav_search_box_item instanceof Bop_Nav_Search_Box_Item ) ){
		$bop_nav_search_box_item = new Bop_Nav_Search_Box_Item();
	}
	return $bop_nav_search_box_item;
}