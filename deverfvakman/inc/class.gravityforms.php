<?php

// Change Gravityform Loader
function spinner_url( $image_src, $form ) {
    return IMAGES.'/loader-dark.svg';
}
add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );

add_filter( 'gform_confirmation_anchor', '__return_false' );

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

function gf_change_main_validation_ui($form_settings, $form){

    $form_settings["Validation Message"]["validation"] = '<tr>
		<th><label for="validation_message_validation">' .__( 'Validation Message', 'maatwerkonline' ) . '</label> ' .gform_tooltip( 'validation_message_validation', '', true ) .'</th>
		<td><input type="text" id="validation_message_validation" name="validation_message_validation" class="fieldwidth-3" value="' . esc_attr( rgars( $form, 'validation_message/validation' ) ) . '" /></td>
	</tr>';

    return $form_settings;
}
add_filter("gform_form_settings", "gf_change_main_validation_ui", 10, 2);

function gf_change_main_validation_process($updated_form){
  $updated_form['validation_message']['validation'] = rgpost( 'validation_message_validation' );
  return $updated_form;
}
add_filter( 'gform_pre_form_settings_save', "gf_change_main_validation_process", 10, 1);

function gf_change_main_validation_front_end($button_input, $form) {
	if($form['validation_message']['validation']){
    	$return .= $form['validation_message']['validation'];
    } else {
		$return .= '';
 	}

	return $return;
}
add_filter("gform_validation_message", "gf_change_main_validation_front_end", 10, 2);

function gf_add_class_to_button_ui($form_settings, $form){

    $form_settings[__('Form Button', 'gravityforms')]["button_css_class"] = '<tr>
		<th><label for="button_css_class">' .__( 'Button CSS-class', 'maatwerkonline' ) . '</label> ' .gform_tooltip( 'button_css_class', '', true ) .'</th>
		<td><input type="text" id="button_css_class" name="button_css_class" class="fieldwidth-3" value="' . esc_attr( rgars( $form, 'button/css_class' ) ) . '" /></td>
  </tr>';
  
  $form_settings[__('Form Button', 'gravityforms')]["previous_button_css_class"] = '<tr>
		<th><label for="previous_button_css_class">' .__( 'Previous Button CSS-class', 'maatwerkonline' ) . '</label> ' .gform_tooltip( 'previous_button_css_class', '', true ) .'</th>
		<td><input type="text" id="previous_button_css_class" name="previous_button_css_class" class="fieldwidth-3" value="' . esc_attr( rgars( $form, 'button/previous_css_class' ) ) . '" /></td>
  </tr>';
  
  $form_settings[__('Form Button', 'gravityforms')]["next_button_css_class"] = '<tr>
		<th><label for="next_button_css_class">' .__( 'Next Button CSS-class', 'maatwerkonline' ) . '</label> ' .gform_tooltip( 'next_button_css_class', '', true ) .'</th>
		<td><input type="text" id="next_button_css_class" name="next_button_css_class" class="fieldwidth-3" value="' . esc_attr( rgars( $form, 'button/next_css_class' ) ) . '" /></td>
	</tr>';

    return $form_settings;
}
add_filter("gform_form_settings", "gf_add_class_to_button_ui", 10, 2);

 function gf_add_class_to_button_process($updated_form){
  $updated_form['button']['css_class'] = rgpost( 'button_css_class' );
  $updated_form['button']['previous_css_class'] = rgpost( 'previous_button_css_class' );
  $updated_form['button']['next_css_class'] = rgpost( 'next_button_css_class' );
  return $updated_form;
}
add_filter( 'gform_pre_form_settings_save', "gf_add_class_to_button_process", 10, 1);

function gf_add_class_to_button_front_end($button, $form){

     preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
     $classes[0] = substr($classes[0], 0, -1);
     $classes[0] .= ' ';
     $classes[0] .= esc_attr($form['button']['css_class']);
     $classes[0] .= "'";

    $button_pieces = preg_split(
              "/class='[\.a-zA-Z_ -]+'/",
              $button,
              -1,
              PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
    );

    return $button_pieces[0] . $classes[0] . $button_pieces[1];

}
add_filter("gform_submit_button", "gf_add_class_to_button_front_end", 10, 2);

function gf_add_class_to_previous_button_front_end($button, $form){

  preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
  $classes[0] = substr($classes[0], 0, -1);
  $classes[0] .= ' ';
  $classes[0] .= esc_attr($form['button']['previous_css_class']);
  $classes[0] .= "'";

 $button_pieces = preg_split(
           "/class='[\.a-zA-Z_ -]+'/",
           $button,
           -1,
           PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
 );

 return $button_pieces[0] . $classes[0] . $button_pieces[1];

}
add_filter("gform_previous_button", "gf_add_class_to_previous_button_front_end", 10, 2);

function gf_add_class_to_next_button_front_end($button, $form){

  preg_match("/class='[\.a-zA-Z_ -]+'/", $button, $classes);
  $classes[0] = substr($classes[0], 0, -1);
  $classes[0] .= ' ';
  $classes[0] .= esc_attr($form['button']['next_css_class']);
  $classes[0] .= "'";

 $button_pieces = preg_split(
           "/class='[\.a-zA-Z_ -]+'/",
           $button,
           -1,
           PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
 );

 return $button_pieces[0] . $classes[0] . $button_pieces[1];

}
add_filter("gform_next_button", "gf_add_class_to_next_button_front_end", 10, 2);

function gf_add_text_after_button_ui($form_settings, $form){

    $form_settings[__('Form Button', 'gravityforms')]["button_text_after"] = '<tr>
		<th><label for="button_text_after">' .__( 'Button text after', 'maatwerkonline' ) . '</label> ' .gform_tooltip( 'button_text_after', '', true ) .'</th>
		<td><input type="text" id="button_text_after" name="button_text_after" class="fieldwidth-3" value="' . esc_attr( rgars( $form, 'button/text_after' ) ) . '" /></td>
	</tr>';

    return $form_settings;
}
add_filter("gform_form_settings", "gf_add_text_after_button_ui", 10, 2);

 function gf_add_text_after_button_process($updated_form){
  $updated_form['button']['text_after'] = rgpost( 'button_text_after' );
  return $updated_form;
}
add_filter( 'gform_pre_form_settings_save', "gf_add_text_after_button_process", 10, 1);

function gf_add_text_after_button_front_end($button_input, $form) {
	preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match); //save attribute string to $button_match[1]
	$button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]); //remove value attribute

	$return = '<button '.$button_atts.'>'.$form['button']['text'].'</button>'.$form['button']['text_after'];

	return $return;
}
add_filter("gform_submit_button", "gf_add_text_after_button_front_end", 10, 2);

// Bootstrapify
function bootstrap_styles_gravityforms_containers( $field_container, $field, $form, $css_class, $style, $field_content ) {
	$id = $field->id;
	$field_id = is_admin() || empty( $form ) ? "field_{$id}" : 'field_' . $form['id'] . "_$id";

	if (strpos($css_class, 'gform_validation_container') !== false) {
		$content = '<li id="' . $field_id . '" class="' . $css_class . ' sr-only">{FIELD_CONTENT}</li>';
	} elseif (strpos($css_class, 'gfield_error') !== false) {
		$content = '<li id="' . $field_id . '" class="' . $css_class . ' form-group has-danger">{FIELD_CONTENT}</li>';
	} elseif (strpos($css_class, 'gfield_visibility_hidden') !== false) {
		$content = '<li id="' . $field_id . '" class="' . $css_class . ' hidden-xs-up">{FIELD_CONTENT}</li>';
	} else {
		$content = '<li id="' . $field_id . '" class="' . $css_class . ' form-group">{FIELD_CONTENT}</li>';
	}
	return $content;
}
add_filter( 'gform_field_container', 'bootstrap_styles_gravityforms_containers', 10, 6 );

function bootstrap_styles_gravityforms_fields($content, $field, $value, $lead_id, $form_id){

    if($field["type"] != 'hidden' && $field["type"] != 'list' && $field["type"] != 'multiselect' && $field["type"] != 'checkbox' && $field["type"] != 'fileupload' && $field["type"] != 'date' && $field["type"] != 'html' && $field["type"] != 'address') {
      $content = str_replace('class=\'medium', 'class=\'form-control large', $content);
    }

    if($field["type"] != 'hidden' && $field["type"] != 'list' && $field["type"] != 'multiselect' && $field["type"] != 'checkbox' && $field["type"] != 'fileupload' && $field["type"] != 'date' && $field["type"] != 'html' && $field["type"] != 'address') {
      $content = str_replace('class=\'large', 'class=\'form-control form-control-lg large', $content);
    }

    if($field["type"] == 'address') {
      $content = str_replace('<input ', '<input class=\'form-control\' ', $content);
    }

    if($field["type"] == 'name') {
      $content = str_replace('<input ', '<input class=\'form-control\' ', $content);
      $content = str_replace('gf_name_has_2', 'gf_name_has_2 row', $content);
      $content = str_replace('name_first', 'name_first col-md-6', $content);
      $content = str_replace('name_last', 'name_last col-md-6', $content);
    }

    if($field["type"] == 'textarea') {
      $content = str_replace('class=\'textarea', 'class=\'form-control textarea', $content);
    }

    if($field["type"] == 'checkbox') {
      $content = str_replace('li class=\'', 'li class=\'custom-control custom-checkbox ', $content);
      $content = str_replace('<input ', '<input class=\'custom-control-input\' ', $content);
      $content = str_replace('<label for', '<span class="custom-control-indicator"></span><label class=\'custom-control-description\' for', $content);
    }

    if($field["type"] == 'radio') {
      $content = str_replace('li class=\'', 'li class=\'custom-control custom-radio ', $content);
      $content = str_replace('<input ', '<input class=\'custom-control-input\' ', $content);
      $content = str_replace('<label for', '<span class="custom-control-indicator"></span><label class=\'custom-control-description\' for', $content);
    }

    if($field["type"] == 'fileupload') {
      $content = str_replace('gfield_description', 'gfield_description form-text text-muted small', $content);
      $content = str_replace('gform_button_select_files', 'gform_button_select_files btn btn-primary', $content);
      $content = str_replace('gform_drop_instructions', 'gform_drop_instructions small d-block text-muted', $content);
      $content = str_replace('gform_drop_area', 'gform_drop_area p-3 mb-2 text-center', $content);
    }

    if($field["type"] == 'password') {
      $content = str_replace('<input ', '<input class=\'form-control\' ', $content);
    }

    $content = str_replace('gfield_required', 'gfield_required ml-2', $content);

	return $content;

}
add_filter("gform_field_content", "bootstrap_styles_gravityforms_fields", 10, 5);

// Fix Gravity Form Tabindex Conflicts
function gform_tabindexer( $tab_index, $form = false ) {
    $starting_index = 3000; // if you need a higher tabindex, update this number
    if( $form )
        add_filter( 'gform_tabindex_' . $form['id'], 'gform_tabindexer' );
    return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
}
add_filter( 'gform_tabindex', 'gform_tabindexer', 10, 2 );


// Add custom merge tags
function add_merge_tags( $form ) {
    ?>
    <script type="text/javascript">
        gform.addFilter('gform_merge_tags', 'add_merge_tags');
        function add_merge_tags(mergeTags, elementId, hideAllFields, excludeFieldTypes, isPrepop, option){
            mergeTags["other"].tags.push({ tag: '{date_y}', label: 'Date (yyyy)' });
            mergeTags["other"].tags.push({ tag: '{domain_extension}', label: 'Domain Extension' });

            return mergeTags;
        }
    </script>
    <?php
    //return the form object from the php hook
    return $form;
}
add_action( 'gform_admin_pre_render', 'add_merge_tags' );

/**
* replace custom merge tags in notifications
*/
function custom_gform_replace_merge_tags($text, $form, $lead, $url_encode, $esc_html, $nl2br, $format) {
    $user_id = get_current_user_id();
	 $user_info = get_userdata($user_id);

    $year = date('Y');
    $text = str_replace('{date_y}', $year, $text);

    $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
    $text = str_replace('{domain_extension}', $extension, $text);

    return $text;
}
add_filter('gform_replace_merge_tags', 'custom_gform_replace_merge_tags', 10, 7);
