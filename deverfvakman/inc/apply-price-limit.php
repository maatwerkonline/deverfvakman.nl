<?php 

// Enqueue the training-query.js file
function apply_price_limit_scripts() {
	// WP Localize Scripts to use PHP values inside your training-query.js file. In this case 'objectL10n.adminurl' will give the value admin_url( 'admin-ajax.php' ).
	wp_localize_script( 'apply_price_limit', 'objectL10n', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	) );
}
add_action( 'wp_enqueue_scripts', 'apply_price_limit_scripts' );

/* Enforce lower price limit for cart items */
function apply_price_limit(){

    if(!is_null(WC()->cart)):
        
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $_SESSION['allow_order'] = "false";

        global $woocommerce;

        $subtotal = WC()->cart->subtotal;

        // If the ability to make an order and go the checkout page should be disabled
        $disable_order = true;

        // The array that will contain all terms present in the cart
        $all_terms = [];

        // The max price at which the order will be disabled (in cents)
        $max_limit_price = 45.95;

        // The ID's of all terms that disable the money limit
        $limit_disablers = [45, 41, 40, 92];

        if($subtotal >= $max_limit_price):
            $disable_order = false;
        else:
            //Get the terms of each item in the cart and add these to all_terms
            foreach ( WC()->cart->get_cart() as $cart_item ):
                $product_id = $cart_item['data']->get_id();
                $product_info = wc_get_product( $product_id );
                $terms = get_the_terms( $product_info->id , 'product_cat');

                foreach($terms as $term):
                    array_push($all_terms, $term->name);
                endforeach;
            endforeach;

            // Check if there are any limit disablers in the cart's product's terms
            foreach($limit_disablers as $limit_disabler):
                $term_name = get_term( $limit_disabler )->name;
                if(in_array($term_name, $all_terms)):
                    $disable_order = false;
                endif;
            endforeach;
        endif;


        if($_POST['ajax_request']):
            if($disable_order == true):
                $_SESSION['allow_order'] = "false";
                wp_send_json(1);
            else:
                $_SESSION['allow_order'] = "true";
                wp_send_json(0);
            endif;
        endif;
    
    endif;
    
};

add_action('wp_ajax_apply_price_limit', 'apply_price_limit'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_apply_price_limit', 'apply_price_limit'); // wp_ajax_nopriv_{action}