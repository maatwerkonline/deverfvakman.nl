<?php
if ( function_exists( 'register_sidebar' ) ) {

	register_sidebar( array (
		'name' => __( 'Blog Sidebar', 'maatwerkonline' ),
		'id' => 'blog-widget-area',
		'description' => __( 'Blog Sidebar', 'maatwerkonline'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="h5">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'Categories', 'maatwerkonline' ),
		'id' => 'categories',
		'description' => __( 'Categories', 'maatwerkonline'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="h5">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array (
		'name' => __( 'First Footer Widget', 'onlinemarketingnl'),
		'id' => 'first-footer-widget',
		'description' => __( 'First Footer Widget Area', 'onlinemarketingnl' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class=" mb-3 strong">',
		'after_title' => '</h5>',
	) );

	register_sidebar( array (
		'name' => __( 'Second Footer Widget', 'onlinemarketingnl'),
		'id' => 'second-footer-widget',
		'description' => __( 'Second Footer Widget Area', 'onlinemarketingnl' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="mb-3 strong">',
		'after_title' => '</h5>',
	) );

	register_sidebar( array (
		'name' => __( 'Third Footer Widget', 'onlinemarketingnl' ),
		'id' => 'third-footer-widget',
		'description' => __( 'Third Footer Widget Area', 'onlinemarketingnl' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="mb-3 strong">',
		'after_title' => '</h5>',
	) );

	register_sidebar( array (
		'name' => __( 'First Bottom Footer Widget', 'onlinemarketingnl'),
		'id' => 'first--bottom-footer-widget',
		'description' => __( 'First Bottom Footer Widget Area', 'onlinemarketingnl' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class=" mb-3 strong">',
		'after_title' => '</h5>',
	) );

	register_sidebar( array (
		'name' => __( 'Second Bottom Footer Widget', 'onlinemarketingnl'),
		'id' => 'second-bottom-footer-widget',
		'description' => __( 'Second Bottom Footer Widget Area', 'onlinemarketingnl' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="mb-3 strong">',
		'after_title' => '</h5>',
	) );

	register_sidebar( array (
		'name' => __( 'Third Bottom Footer Widget', 'onlinemarketingnl' ),
		'id' => 'third-bottom-footer-widget',
		'description' => __( 'Third Bottom Footer Widget Area', 'onlinemarketingnl' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="mb-3 strong">',
		'after_title' => '</h5>',
	) );
}
