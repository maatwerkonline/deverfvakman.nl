<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div>
        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
        <input id="search-focus" class="input-search search-field" type="text" placeholder="<?php _e( 'Search here for your product...', 'maatwerkonline' ); ?>" value="<?php echo get_search_query(); ?>" name="s" id="s" />
        <button type="submit" id="searchsubmit"><i class="dvv dvv-magnifier"></i></button>
    </div>
</form>