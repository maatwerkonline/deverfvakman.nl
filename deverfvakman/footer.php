<footer id="footer">
    <?php if (!is_checkout() && !is_cart()) : ?>
        <section class="top-footer">
            <div class="container">

                <div class="row text-center text-xl-left text-white">
                    <div class="col-xl-3 col-md-3 col-sm-6 col-12">
                        <?php dynamic_sidebar(__('First Footer Widget', 'maatwerkonline')); ?>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-6 col-12 mt-3 mt-md-0">
                        <?php dynamic_sidebar(__('Second Footer Widget', 'maatwerkonline')); ?>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-6 col-12 mt-3 mt-md-0">
                        <?php dynamic_sidebar(__('Third Footer Widget', 'maatwerkonline')); ?>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-6 col-12 mt-3 mt-md-0 d-flex justify-content-left">
                        <img class="align-self-center footer-country" src="<?php echo get_site_url(); ?>/wp-content/uploads/2020/09/NLkopie.png" >
                        <div class="footer-socials">
                        <h4 class="strong"><?php _e('Follow us', 'maatwerkonline')?></h4>
                            <ul class="social-network social-circle">
                                 <li><a href="https://www.facebook.com/DeVerfVakman" target="_blank" class="icoFacebook mr-2" title="Facebook"><i class="dvv dvv-facebook"></i></a></li>
                                 <li><a href="https://www.linkedin.com/company/3147681/" target="_blank" class="icoLinkedin mr-2" title="Linkedin"><i class="dvv dvv-linkedin"></i></a></li>
                                 <li><a href="https://nl.pinterest.com/bobwoutersen0453/" target="_blank" class="icoPinterest" title="Pinterest"><i class="dvv dvv-pinterest"></i></a></li>
                            </ul>		
                        </div>		
                    </div>
                </div>
            </div>	
        </section>
    <?php endif; ?>
    <section class="bottom-footer">
        <div class="container pt-5 pb-5">
            <div class="row text-center text-xl-left">
                <div class="col-12">
                    <?php dynamic_sidebar(__('First Bottom Footer Widget', 'maatwerkonline')); ?>
                </div>
            </div>
            <div class="row text-center text-xl-left">
                <div class="col-12">
                    <?php dynamic_sidebar(__('Second Bottom Footer Widget', 'maatwerkonline')); ?>
                </div>
            </div>
            <div class="row text-xl-left ">
                <div class="col-12 text-center mb-0">
                    <?php dynamic_sidebar(__('Third Bottom Footer Widget', 'maatwerkonline')); ?>
                </div>
            </div>
        </div>	
    </section>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

</footer>

</div> <!-- end #page-content-wrapper -->

</div> <!-- end #wrapper -->

<?php wp_footer(); ?>

</body>
</html>
