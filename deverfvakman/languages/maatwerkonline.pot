#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Roodenburg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-31 14:51+0000\n"
"POT-Revision-Date: Fri Sep 09 2016 14:04:47 GMT+0200 (CEST)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco https://localise.biz/"

#: woocommerce/emails/vanilla/customer-completed-order.php:35
msgid "$order-completed-local-pickup-sentence-1"
msgstr ""

#: woocommerce/emails/vanilla/customer-completed-order.php:37
msgid "$order-completed-local-pickup-sentence-2"
msgstr ""

#: woocommerce/emails/vanilla/customer-completed-order.php:43
msgid "$order-completed-sentence-1"
msgstr ""

#: woocommerce/emails/vanilla/customer-completed-order.php:45
msgid "$order-completed-sentence-2"
msgstr ""

#. 1: comment author, 2: author badge, 3: date and time
#: template-parts/comments.php:42
#, php-format
msgid "%1$s  %2$s %3$s"
msgstr ""

#. %d: total results
#: woocommerce/loop/result-count.php:60
#, php-format
msgid "%d Result"
msgid_plural "%d Results"
msgstr[0] ""
msgstr[1] ""

#: template-parts/comments.php:45
#, php-format
msgctxt "%s = human-readable time difference"
msgid "<time pubdate class=\"small text-muted ml-2\">%s ago</time>"
msgstr ""

#: template-parts/comments.php:79
#, php-format
msgid "%s comment"
msgid_plural "%s comments"
msgstr[0] ""
msgstr[1] ""

#: template-parts/comments.php:84
msgid "&larr; Older Comments"
msgstr ""

#: inc/admin/theme-options.php:67
msgid "404 Warning"
msgstr ""

#: woocommerce/single-product/tabs/description.php:59
msgid "[Your browser does not support the video tag]"
msgstr ""

#: woocommerce/myaccount/form-edit-account.php:31
msgid "Account details change"
msgstr ""

#: inc/class.menu-walker.php:269
msgid "Add Bootstrap Items"
msgstr ""

#: inc/admin/post-type-example.php:12
msgid "Add New"
msgstr ""

#: inc/admin/post-type-example.php:11
msgid "Add New Example"
msgstr ""

#: inc/class.menu-walker.php:220
msgid "Add Sidebar"
msgstr ""

#: inc/class.menu-walker.php:256 inc/class.menu-walker.php:325
msgid "Add to Menu"
msgstr ""

#: inc/admin/theme-options.php:85
msgid "Address"
msgstr ""

#: inc/admin/theme-options.php:200
msgid "Advantage"
msgstr ""

#: template-parts/loop-single-post-slider.php:31
msgid "ago"
msgstr ""

#: inc/admin/post-type-example.php:10
msgid "All Examples"
msgstr ""

#: woocommerce/order/order-details.php:49
msgid "Amount"
msgstr ""

#: woocommerce/checkout/form-shipping.php:63
msgid "Any additional information?"
msgstr ""

#: inc/admin/theme-options.php:11
msgid "Appearance"
msgstr ""

#: template-parts/comments.php:37
msgid "Author"
msgstr ""

#: inc/admin/theme-options.php:316
msgid "Bank Account number"
msgstr ""

#: woocommerce/single-product-reviews.php:113
#: woocommerce/single-product-reviews.php:132
msgid "Be the first to review"
msgstr ""

#: woocommerce/emails/vanilla/email-addresses.php:22
msgid "Billing Address"
msgstr ""

#: inc/sidebars.php:5 inc/sidebars.php:7
msgid "Blog Sidebar"
msgstr ""

#: inc/admin/theme-options.php:54
msgid "Breadcrumbs"
msgstr ""

#: inc/class.gravityforms.php:44
msgid "Button CSS-class"
msgstr ""

#: inc/admin/theme-options.php:241
msgid "Button Text"
msgstr ""

#: inc/class.gravityforms.php:133
msgid "Button text after"
msgstr ""

#: header.php:89
msgid "Call for advice"
msgstr ""

#: template-parts/comments.php:132
msgid "Cancel Reply"
msgstr ""

#: woocommerce/cart/cart-totals.php:30
#: woocommerce/checkout/form-checkout.php:70
msgid "Cart"
msgstr ""

#: inc/sidebars.php:15 inc/sidebars.php:17
msgid "Categories"
msgstr ""

#: inc/admin/theme-options.php:306
msgid "Chamber of Commerce number"
msgstr ""

#: single.php:41 single.php:51
msgid "Check this out:"
msgstr ""

#: inc/admin/theme-options.php:288
msgid ""
"Choose the page which will be displayed when people click the contact button."
msgstr ""

#: inc/admin/theme-options.php:279
msgid "Choose the page which will be displayed when people got the 404-error."
msgstr ""

#: inc/class.woocommerce.php:957 inc/class.woocommerce-old.php:957
#: inc/class.woocommerce-oud.php:479
msgid "City"
msgstr ""

#: inc/admin/theme-options.php:475
msgid "Closing Hour"
msgstr ""

#: template-parts/comments.php:110 template-parts/comments.php:111
msgid "Comment"
msgstr ""

#: template-parts/comments.php:37
msgid "Comment by the author"
msgstr ""

#: template-parts/comments.php:83
msgid "Comment navigation"
msgstr ""

#: template-parts/comments.php:99
msgid "Comments are closed."
msgstr ""

#: inc/class.woocommerce.php:923 inc/class.woocommerce-old.php:923
#: inc/class.woocommerce-oud.php:462
msgid "Company (optional)"
msgstr ""

#: header.php:62 header.php:66
msgid "Confirm"
msgstr ""

#: inc/admin/theme-options.php:23
msgid "Contact"
msgstr ""

#: inc/admin/theme-options.php:73 inc/admin/theme-options.php:287
msgid "Contact page"
msgstr ""

#: woocommerce/cart/cart.php:139
msgid "Continue shopping"
msgstr ""

#: inc/admin/theme-options.php:60
msgid "Cookie Notification"
msgstr ""

#: inc/class.woocommerce.php:929 inc/class.woocommerce-old.php:929
#: inc/class.woocommerce-oud.php:465
msgid "Country"
msgstr ""

#: functions.php:86
msgid "Create a dropdown menu on smaller screens"
msgstr ""

#: woocommerce/myaccount/form-edit-account.php:57
msgid "Current password"
msgstr ""

#: inc/admin/theme-options.php:408
msgid "Day"
msgstr ""

#: inc/admin/theme-options.php:231
msgid "Day&time"
msgstr ""

#: functions.php:78 functions.php:98
msgid "Default Menu"
msgstr ""

#: inc/class.woocommerce.php:199 inc/class.woocommerce-old.php:199
#: inc/class.woocommerce-oud.php:100
msgid "Default sorting"
msgstr ""

#: inc/class.menu-walker.php:405
msgctxt "default-title"
msgid "Search"
msgstr ""

#: woocommerce/cart/cart-totals.php:36
#: woocommerce/checkout/form-checkout.php:44
#: woocommerce/checkout/form-checkout.php:76
msgid "Delivery"
msgstr ""

#: woocommerce/checkout/thankyou.php:54
msgid "Delivery date:"
msgstr ""

#: inc/class.menu-walker.php:308 inc/class.menu-walker.php:310
#: inc/class.menu-walker.php:311
msgid "Description"
msgstr ""

#: woocommerce/checkout/form-shipping.php:27
msgid "Do you want to ship to a different address?"
msgstr ""

#: inc/class.woocommerce.php:1593 inc/class.woocommerce-old.php:1593
#: inc/class.woocommerce-oud.php:797
msgid "Documenten"
msgstr ""

#: inc/class.menu-walker.php:290 inc/class.menu-walker.php:292
#: inc/class.menu-walker.php:293
msgid "Dropdown Divider"
msgstr ""

#: template-parts/comments.php:18 template-parts/comments.php:57
#: woocommerce/single-product/review.php:70
msgid "Edit"
msgstr ""

#: woocommerce/myaccount/dashboard.php:78
#: woocommerce/myaccount/dashboard.php:82
msgid "Edit Account"
msgstr ""

#: woocommerce/myaccount/dashboard.php:64
#: woocommerce/myaccount/dashboard.php:68
msgid "Edit Address"
msgstr ""

#: inc/admin/post-type-example.php:14
msgid "Edit Example"
msgstr ""

#: woocommerce/myaccount/dashboard.php:79
msgid "Edit your Account Settings, this will be used on your certificate."
msgstr ""

#: woocommerce/myaccount/dashboard.php:65
msgid "Edit your Billing Address, this will be used on new orders."
msgstr ""

#: inc/admin/theme-options.php:17
msgid "Elements"
msgstr ""

#: inc/class.woocommerce.php:963 inc/class.woocommerce-old.php:963
#: inc/class.woocommerce-oud.php:482 inc/admin/theme-options.php:103
#: inc/admin/theme-options.php:553 inc/admin/theme-options.php:573
msgid "Email"
msgstr ""

#: template-parts/comments.php:125 template-parts/comments.php:126
msgid "Email address"
msgstr ""

#: woocommerce/order/order-details-customer.php:39
msgid "Email:"
msgstr ""

#: inc/admin/theme-options.php:242
msgid "Enter button text"
msgstr ""

#: inc/admin/theme-options.php:232
msgid "Enter Day time"
msgstr ""

#: inc/admin/theme-options.php:222
msgid "Enter telephone number"
msgstr ""

#: inc/admin/theme-options.php:327
msgid "Enter the name of the building you are working in"
msgstr ""

#: inc/admin/theme-options.php:392
msgid "Enter the text which will be displayed to the visitors."
msgstr ""

#: inc/admin/theme-options.php:212
msgid "Enter title"
msgstr ""

#: inc/admin/theme-options.php:317
msgid "Enter your Bank Account number here"
msgstr ""

#: inc/admin/theme-options.php:307
msgid "Enter your Chamber of Commerce number here"
msgstr ""

#: inc/admin/theme-options.php:354 inc/admin/theme-options.php:382
msgid "Enter your city here"
msgstr ""

#: inc/admin/theme-options.php:554
msgid "Enter your email here"
msgstr ""

#: inc/admin/theme-options.php:544
msgid "Enter your mobile number here"
msgstr ""

#: inc/admin/theme-options.php:534
msgid "Enter your phone number here"
msgstr ""

#: inc/admin/theme-options.php:336 inc/admin/theme-options.php:364
msgid "Enter your street address here"
msgstr ""

#: inc/admin/theme-options.php:297
msgid "Enter your VAT number here"
msgstr ""

#: inc/admin/theme-options.php:345 inc/admin/theme-options.php:373
msgid "Enter your zip-code here"
msgstr ""

#: inc/admin/theme-options.php:278
msgid "Error 404 page"
msgstr ""

#: inc/admin/post-type-example.php:16 inc/admin/post-type-example.php:28
msgid "Example"
msgstr ""

#: inc/admin/post-type-example.php:22
msgid "example"
msgstr ""

#: inc/admin/post-type-example.php:8 inc/admin/post-type-example.php:9
msgid "Examples"
msgstr ""

#: woocommerce/loop/price-old.php:41 woocommerce/loop/price.php:43
msgid "exc. VAT"
msgstr ""

#: inc/admin/theme-options.php:574
msgid "Facebook"
msgstr ""

#: woocommerce/loop/orderby.php:40
msgid "Filter"
msgstr ""

#: footer.php:35
msgid "First Bottom Footer Widget"
msgstr ""

#: footer.php:8
msgid "First Footer Widget"
msgstr ""

#: inc/class.woocommerce.php:911 inc/class.woocommerce-old.php:911
#: inc/class.woocommerce-oud.php:456
msgid "First Name"
msgstr ""

#: footer.php:19
msgid "Follow us"
msgstr ""

#: inc/class.menu-walker.php:501
msgctxt "form-submit-button"
msgid "Search"
msgstr ""

#: inc/admin/theme-options.php:414
msgid "Friday"
msgstr ""

#: inc/class.woocommerce.php:1755 inc/class.woocommerce-old.php:1753
#: woocommerce/loop/price-old.php:37 woocommerce/loop/price.php:39
msgid "From"
msgstr ""

#: woocommerce/myaccount/dashboard.php:36
#, php-format
msgid ""
"From your account dashboard you can view your <a href=\"%1$s\">recent "
"orders</a>, manage your <a href=\"%2$s\">shipping and billing addresses</a> "
"and <a href=\"%3$s\">edit your password and account details</a>."
msgstr ""

#: inc/class.woocommerce.php:805 inc/class.woocommerce-old.php:805
#: inc/class.woocommerce-oud.php:403
msgid "Go to your Cart"
msgstr ""

#: inc/admin/theme-options.php:35 inc/admin/theme-options.php:263
msgid "Header"
msgstr ""

#. 1
#: woocommerce/myaccount/dashboard.php:29
#, php-format
msgid "Hello %1$s"
msgstr ""

#: functions.php:88
msgid "Hide title"
msgstr ""

#: inc/class.woocommerce.php:939 inc/class.woocommerce-old.php:939
#: inc/class.woocommerce-oud.php:470
msgid "Housenumber"
msgstr ""

#: inc/admin/theme-options.php:264
msgid ""
"How would you like to make the Header look like? Would like the Logo and "
"Navigation to be Inline or Seperated?"
msgstr ""

#. URI of the theme
msgid "https://www.deverfvakman.nl"
msgstr ""

#. Author URI of the theme
msgid "https://www.maatwerkonline.nl"
msgstr ""

#: header.php:65
msgid "I am a business partner"
msgstr ""

#: header.php:61
msgid "I am a consumer"
msgstr ""

#: woocommerce/checkout/terms.php:27
#, php-format
msgid ""
"I&rsquo;ve read and accept the <a href=\"%s\" class=\"woocommerce-terms-and-"
"conditions-link\">terms &amp; conditions</a>"
msgstr ""

#: inc/admin/theme-options.php:175
msgid "Icon"
msgstr ""

#: inc/admin/theme-options.php:255
msgid "If you have your logo in .png, please upload it in .png."
msgstr ""

#: inc/admin/theme-options.php:213 inc/admin/theme-options.php:223
#: inc/admin/theme-options.php:233 inc/admin/theme-options.php:243
#: inc/admin/theme-options.php:298 inc/admin/theme-options.php:308
#: inc/admin/theme-options.php:318 inc/admin/theme-options.php:328
#: inc/admin/theme-options.php:337 inc/admin/theme-options.php:346
#: inc/admin/theme-options.php:355 inc/admin/theme-options.php:365
#: inc/admin/theme-options.php:374 inc/admin/theme-options.php:383
#: inc/admin/theme-options.php:393 inc/admin/theme-options.php:535
#: inc/admin/theme-options.php:545
msgid "If you leave this field empty it will not be shown on the frontend."
msgstr ""

#: inc/admin/theme-options.php:555
msgid "If you leave this field empty it will not be shown on your website."
msgstr ""

#: inc/admin/theme-options.php:254
msgid ""
"If you upload a Logo this will overwrite the Site Title and Tagline in the "
"header."
msgstr ""

#: inc/admin/theme-options.php:63
msgid ""
"If you want to show uses you use cookies you want to change these options."
msgstr ""

#: inc/admin/post-type-example.php:29
msgid "Individual Example"
msgstr ""

#: inc/admin/theme-options.php:270
msgid "Inline"
msgstr ""

#: inc/admin/theme-options.php:581
msgid "Instagram"
msgstr ""

#: index.php:12
msgid "Instructie Blogs"
msgstr ""

#: inc/class.woocommerce.php:917 inc/class.woocommerce-old.php:917
#: inc/class.woocommerce-oud.php:459
msgid "Last Name"
msgstr ""

#. Name of the template
msgid "Lead Page"
msgstr ""

#: template-parts/comments.php:103
msgid "Leave a Comment"
msgstr ""

#: template-parts/comments.php:103
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: woocommerce/myaccount/form-edit-account.php:58
#: woocommerce/myaccount/form-edit-account.php:65
msgid "Leave blank to leave unchanged"
msgstr ""

#: inc/admin/theme-options.php:183
msgid "Link"
msgstr ""

#: inc/admin/theme-options.php:587
msgid "Link URL"
msgstr ""

#: inc/admin/theme-options.php:575
msgid "LinkedIn"
msgstr ""

#: template-parts/comments.php:114
msgid "Log out"
msgstr ""

#: template-parts/comments.php:114
msgid "Log out of this account"
msgstr ""

#: inc/admin/theme-options.php:253
msgid "Logo"
msgstr ""

#. Name of the theme
#. Author of the theme
msgid "Maatwerk Online"
msgstr ""

#: functions.php:79
msgid "Mega Menu - 1 Column"
msgstr ""

#: functions.php:80
msgid "Mega Menu - 2 Columns"
msgstr ""

#: functions.php:81
msgid "Mega Menu - 3 Columns"
msgstr ""

#: functions.php:82
msgid "Mega Menu - 4 Columns"
msgstr ""

#: functions.php:83
msgid "Mega Menu - 5 Columns"
msgstr ""

#: functions.php:84
msgid "Mega Menu - 6 Columns"
msgstr ""

#: functions.php:85
msgid "Mega Menu - 7 Columns"
msgstr ""

#: header.php:156 template-parts/header/header-seperated.php:40
#: template-parts/header/header-inline.php:43
msgid "Menu"
msgstr ""

#: functions.php:77
msgid "Menu Type"
msgstr ""

#: inc/class.menu-walker.php:416
msgctxt "meta-box-submit"
msgid "Add to menu"
msgstr ""

#: inc/class.menu-walker.php:380
msgctxt "meta-box-title"
msgid "Search Box"
msgstr ""

#: header.php:161
msgid "Minimum order amount € 44,95"
msgstr ""

#: functions.php:100
msgid "Mobile Menu"
msgstr ""

#: inc/admin/theme-options.php:543
msgid "Mobile number"
msgstr ""

#: inc/admin/theme-options.php:410
msgid "Monday"
msgstr ""

#: template-parts/comments.php:118 template-parts/comments.php:119
msgid "Name"
msgstr ""

#: inc/class.menu-walker.php:281 inc/class.menu-walker.php:283
#: inc/class.menu-walker.php:284
msgid "Navbar Divider"
msgstr ""

#: header.php:131
msgid "Need help?"
msgstr ""

#: template-parts/loop-single-post-slider.php:30
msgid "New"
msgstr ""

#: inc/admin/post-type-example.php:13
msgid "New Example"
msgstr ""

#: template-parts/comments.php:85
msgid "Newer Comments &rarr;"
msgstr ""

#: inc/admin/theme-options.php:576
msgid "Newsletter"
msgstr ""

#: inc/class.pagination.php:7
msgid "Next"
msgstr ""

#: inc/class.gravityforms.php:54
msgid "Next Button CSS-class"
msgstr ""

#: index.php:71
msgid "No posts were found. Sorry!"
msgstr ""

#: inc/admin/post-type-example.php:18
msgid "Not found"
msgstr ""

#: inc/admin/post-type-example.php:19
msgid "Not found in Trash"
msgstr ""

#: woocommerce/order/order-details-customer.php:32
msgid "Note:"
msgstr ""

#: inc/class.woocommerce.php:1129 inc/class.woocommerce-old.php:1129
#: inc/class.woocommerce-oud.php:565
msgid "Offerte aanvragen"
msgstr ""

#: inc/admin/theme-options.php:421
msgid "Opening Hour"
msgstr ""

#: inc/admin/theme-options.php:91 inc/admin/theme-options.php:400
msgid "Opening Hours"
msgstr ""

#: inc/admin/theme-options.php:391
msgid "Opening Hours text"
msgstr ""

#: woocommerce/myaccount/form-login.php:76
msgid "or"
msgstr ""

#: woocommerce/checkout/thankyou.php:60
msgid "Order date:"
msgstr ""

#: woocommerce/checkout/thankyou.php:48
msgid "Order number:"
msgstr ""

#: inc/class.woocommerce.php:765 inc/class.woocommerce-old.php:765
#: inc/class.woocommerce-oud.php:383
msgid "Our Choice"
msgstr ""

#: woocommerce/cart/cart-totals.php:42
#: woocommerce/checkout/form-checkout.php:82
msgid "Payment"
msgstr ""

#: woocommerce/checkout/thankyou.php:71
msgid "Payment method:"
msgstr ""

#: inc/class.woocommerce.php:969 inc/class.woocommerce-old.php:969
#: inc/class.woocommerce-oud.php:485 inc/admin/theme-options.php:97
msgid "Phone"
msgstr ""

#: inc/admin/theme-options.php:533
msgid "Phone number"
msgstr ""

#: woocommerce/order/order-details-customer.php:46
msgid "Phone:"
msgstr ""

#: template-parts/comments.php:18
msgid "Pingback:"
msgstr ""

#: template-parts/comments.php:121
msgid "Please enter a valid email address"
msgstr ""

#: woocommerce/myaccount/form-lost-password.php:30
msgid ""
"Please enter your username or email address. You will receive a link to "
"create a new password via email."
msgstr ""

#: inc/admin/theme-options.php:363
msgid "Post Address"
msgstr ""

#: inc/admin/post-type-example.php:6
msgctxt "Post Type General Name"
msgid "Example"
msgstr ""

#: inc/admin/post-type-example.php:7
msgctxt "Post Type Singular Name"
msgid "Example"
msgstr ""

#: inc/class.woocommerce.php:951 inc/class.woocommerce-old.php:951
#: inc/class.woocommerce-oud.php:476
msgid "Postcode"
msgstr ""

#: index.php:76 inc/class.pagination.php:6
msgid "Previous"
msgstr ""

#: inc/class.gravityforms.php:49
msgid "Previous Button CSS-class"
msgstr ""

#: woocommerce/cart/proceed-to-checkout-button.php:27
msgid "Proceed"
msgstr ""

#: inc/admin/theme-options.php:192
msgid "Product Advantages"
msgstr ""

#: inc/admin/theme-options.php:121
msgid "Product Widgets"
msgstr ""

#: index.php:60 template-parts/loop-single-post.php:24
msgid "Read more"
msgstr ""

#: woocommerce/myaccount/dashboard.php:49
msgid "Recent Orders"
msgstr ""

#: woocommerce/myaccount/form-login.php:84
msgid "Register"
msgstr ""

#: inc/admin/theme-options.php:79
msgid "Registration numbers"
msgstr ""

#: header.php:154 template-parts/header/header-seperated.php:40
#: template-parts/header/header-inline.php:43
msgid "Reload"
msgstr ""

#: woocommerce/cart/cart.php:115 woocommerce/cart/cart.php:119
msgid "Remove"
msgstr ""

#: template-parts/comments.php:56
msgid "Reply"
msgstr ""

#: template-parts/comments.php:131
msgid "Respond"
msgstr ""

#: inc/admin/theme-options.php:577
msgid "RSS"
msgstr ""

#: inc/admin/theme-options.php:415
msgid "Saturday"
msgstr ""

#: inc/class.menu-walker.php:569
msgid "Search"
msgstr ""

#: inc/admin/post-type-example.php:17
msgid "Search Example"
msgstr ""

#: searchform.php:4
msgid "Search here for your product..."
msgstr ""

#: woocommerce/product-searchform.php:27
msgid "Search your products here&hellip;"
msgstr ""

#: footer.php:40
msgid "Second Bottom Footer Widget"
msgstr ""

#: footer.php:11
msgid "Second Footer Widget"
msgstr ""

#: inc/class.menu-walker.php:253 inc/class.menu-walker.php:322
msgid "Select All"
msgstr ""

#: inc/admin/theme-options.php:271
msgid "Seperated"
msgstr ""

#: woocommerce/emails/vanilla/email-addresses.php:36
msgid "Shipping Address"
msgstr ""

#: woocommerce/archive-product.php:55
#, php-format
msgid "Showing search results for: %s"
msgstr ""

#. %d: total results
#: woocommerce/loop/result-count.php:42
#, php-format
msgid "Showing the single result"
msgid_plural "Showing all %d results"
msgstr[0] ""
msgstr[1] ""

#: inc/admin/theme-options.php:29
msgid "Site Title, Tagline, Logo & Favicon"
msgstr ""

#: inc/admin/theme-options.php:109
msgid "Social Media Icons"
msgstr ""

#: inc/admin/theme-options.php:562
msgid "Social Media Links"
msgstr ""

#: inc/admin/theme-options.php:571
msgid "Social Network"
msgstr ""

#: woocommerce/loop/orderby.php:34
msgid "Sort by"
msgstr ""

#: inc/class.woocommerce.php:203 inc/class.woocommerce-old.php:203
#: inc/class.woocommerce-oud.php:102
msgid "Sort by average rating"
msgstr ""

#: inc/class.woocommerce.php:205 inc/class.woocommerce-old.php:205
#: inc/class.woocommerce-oud.php:103
msgid "Sort by latest"
msgstr ""

#: inc/class.woocommerce.php:201 inc/class.woocommerce-old.php:201
#: inc/class.woocommerce-oud.php:101
msgid "Sort by popularity"
msgstr ""

#: inc/class.woocommerce.php:209 inc/class.woocommerce-old.php:209
#: inc/class.woocommerce-oud.php:105
msgid "Sort by price: high to low"
msgstr ""

#: inc/class.woocommerce.php:207 inc/class.woocommerce-old.php:207
#: inc/class.woocommerce-oud.php:104
msgid "Sort by price: low to high"
msgstr ""

#: inc/class.woocommerce.php:1131 inc/class.woocommerce-old.php:1131
#: inc/class.woocommerce-oud.php:566
msgid "Speciale tarieven aanvragen"
msgstr ""

#: inc/admin/theme-options.php:41
msgid "Static Front Page"
msgstr ""

#: inc/class.woocommerce.php:933 inc/class.woocommerce-old.php:933
#: inc/class.woocommerce-oud.php:467
msgid "Street"
msgstr ""

#: inc/class.woocommerce.php:945 inc/class.woocommerce-old.php:945
#: inc/class.woocommerce-oud.php:473
msgid "Suffix"
msgstr ""

#: inc/admin/theme-options.php:416
msgid "Sunday"
msgstr ""

#: inc/admin/theme-options.php:221
msgid "Telephone"
msgstr ""

#: footer.php:45
msgid "Third Bottom Footer Widget"
msgstr ""

#: footer.php:14
msgid "Third Footer Widget"
msgstr ""

#: inc/admin/theme-options.php:12
msgid "This panel contains the Appearance options"
msgstr ""

#: inc/admin/theme-options.php:24
msgid "This panel contains the Contact options"
msgstr ""

#: inc/admin/theme-options.php:18
msgid "This panel contains the different elements of the site."
msgstr ""

#: template-parts/comments.php:68
msgid ""
"This post is password protected. Enter the password to view any comments."
msgstr ""

#: woocommerce/single-product/price-old.php:34
#: woocommerce/single-product/price.php:32
msgid "This product will be <strong>cheaper</strong> if you buy more"
msgstr ""

#: inc/admin/theme-options.php:413
msgid "Thursday"
msgstr ""

#: inc/class.menu-walker.php:299 inc/class.menu-walker.php:301
#: inc/class.menu-walker.php:302 inc/admin/theme-options.php:211
msgid "Title"
msgstr ""

#: functions.php:99
msgid "Topbar Menu"
msgstr ""

#: woocommerce/checkout/thankyou.php:65
msgid "Total:"
msgstr ""

#: inc/admin/theme-options.php:411
msgid "Tuesday"
msgstr ""

#: inc/admin/theme-options.php:578
msgid "Twitter"
msgstr ""

#: inc/class.menu-walker.php:403
msgctxt "type-label"
msgid "Search Box"
msgstr ""

#: inc/admin/post-type-example.php:15
msgid "Update Example"
msgstr ""

#: functions.php:87
msgid "Use description field as HTML content"
msgstr ""

#: inc/admin/theme-options.php:179
msgid "USP"
msgstr ""

#: inc/admin/theme-options.php:115 inc/admin/theme-options.php:167
msgid "USPs"
msgstr ""

#: inc/class.woocommerce.php:1933 inc/class.woocommerce-old.php:1933
#: inc/class.woocommerce-oud.php:989
msgid "Uw winkelwagen is momenteel leeg"
msgstr ""

#: inc/class.gravityforms.php:16
msgid "Validation Message"
msgstr ""

#: inc/admin/theme-options.php:296
msgid "VAT number"
msgstr ""

#: woocommerce/myaccount/dashboard.php:50
msgid "View your recent orders and contact us if you have any question."
msgstr ""

#: inc/admin/theme-options.php:326
msgid "Visiting Address"
msgstr ""

#: inc/admin/theme-options.php:265
msgid ""
"Want to adjust one of these? Look into the /template-parts/headers/folder "
"and add or edit your header."
msgstr ""

#. Description of the theme
msgid "Webdesign by Maatwerk Online"
msgstr ""

#: inc/admin/theme-options.php:412
msgid "Wednesday"
msgstr ""

#: woocommerce/checkout/form-billing.php:28
msgid "What are your details?"
msgstr ""

#: inc/admin/theme-options.php:579
msgid "Whatsapp"
msgstr ""

#: template-parts/comments.php:127
msgid "Will not be published."
msgstr ""

#. 1: first result 2: last result 3: total results
#: woocommerce/loop/result-count.php:63
#, php-format
msgctxt "with first and last result"
msgid "%3$d Result"
msgid_plural "%3$d Results"
msgstr[0] ""
msgstr[1] ""

#. 1: first result 2: last result 3: total results
#: woocommerce/loop/result-count.php:45
#, php-format
msgctxt "with first and last result"
msgid "Showing the single result"
msgid_plural "Showing %1$d&ndash;%2$d of %3$d results"
msgstr[0] ""
msgstr[1] ""

#: inc/admin/theme-options.php:580
msgid "Wordpress"
msgstr ""

#: woocommerce/checkout/form-shipping.php:33
msgid "Yes, please ship to a different address then my details above"
msgstr ""

#: template-parts/comments.php:106
#, php-format
msgid "You must be %1$s logged in %2$s to post a comment"
msgstr ""

#: woocommerce/cart/cart.php:32
msgid "Your Cart"
msgstr ""

#: template-parts/comments.php:50
msgid "Your comment is awaiting moderation."
msgstr ""

#: woocommerce/myaccount/dashboard.php:53
msgid "Your Orders"
msgstr ""

#: inc/admin/theme-options.php:44
msgid "Your theme supports a static front page."
msgstr ""

#: inc/admin/theme-options.php:582
msgid "Youtube"
msgstr ""
