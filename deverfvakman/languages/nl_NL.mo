��    �      D      l      l  (   m  (   �     �     �     �  ]   	     j	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     '
     7
     D
  
   I
     T
     o
     t
     |
     �
     �
     �
     �
     �
     �
  )   �
          *     7     G  +   P  
   |     �     �     �  B   �  ;   �     %     +  #   9  *   ]     �     �     �  
   �  	   �     �  �   �     �  
   �  
   �     �     �       t     	   �     �     �     �     �     �     �          #     9     O     e     {  	   �     �     �  
   �     �     �     �     �     �                    *  "   1  h   T     �     �     �  	   �     �     �     �                    &     -     <  !   \     ~     �  0   �     �     �     �                ;     V     ]  I   d     �  %   �  
   �  @   �     '     8     O  =   f  1   �  	   �     �  $   �          &     /  3   2  \   f  o  �  ^   3     �  W   �  S   �     F  =   a     �     �     �     �     �     	          (     /     C     ]     j     z  
   �     �     �     �     �     �     �     �       
   !     ,     4  2   D     w     �     �     �  )   �  
   �     �     �       F     D   `     �     �  #   �  +   �          #     7     V     _     h  �   n     B  
   X     c  
   q     |     �  v   �  
   "     -     E  /   S  	   �     �     �     �     �     �                  .   
   E      P      o      t      �      �      �      �      �      �   	   �      �      �      �   �   !  	   �!     �!     �!  	    "     
"     "     +"     ?"     K"     S"     ["     b"     p"     �"     �"     �"  K   �"      #  !   ,#     N#     b#      z#      �#  
   �#  
   �#  a   �#     4$  .   <$  
   k$  N   v$     �$     �$     �$  F    %  4   G%     |%     �%  ,   �%     �%  	   �%     �%     �%  f   �%   $order-completed-local-pickup-sentence-1 $order-completed-local-pickup-sentence-2 $order-completed-sentence-1 $order-completed-sentence-2 %d Result %d Results %s = human-readable time difference<time pubdate class="small text-muted ml-2">%s ago</time> %s comment %s comments &larr; Older Comments Account details change Add New Add Sidebar Amount Any additional information? Author Bank Account number Be the first to review Billing Address Call for advice Cancel Reply Cart Categories Chamber of Commerce number City Comment Comment by the author Comment navigation Comments are closed. Company (optional) Confirm Contact Continue shopping Create a dropdown menu on smaller screens Current password Default Menu Default sorting Delivery Do you want to ship to a different address? Documenten Edit Edit Account Edit Address Edit your Account Settings, this will be used on your certificate. Edit your Billing Address, this will be used on new orders. Email Email address Enter your Bank Account number here Enter your Chamber of Commerce number here Enter your VAT number here Enter your city here Enter your phone number here First Name Follow us From From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a> and <a href="%3$s">edit your password and account details</a>. Go to your Cart Hello %1$s Hide title Housenumber I am a business partner I am a consumer I&rsquo;ve read and accept the <a href="%s" class="woocommerce-terms-and-conditions-link">terms &amp; conditions</a> Last Name Leave a Comment Leave a Reply to %s Leave blank to leave unchanged Log out Log out of this account Mega Menu - 1 Column Mega Menu - 2 Columns Mega Menu - 3 Columns Mega Menu - 4 Columns Mega Menu - 5 Columns Mega Menu - 6 Columns Mega Menu - 7 Columns Menu Type Minimum order amount € 44,95 Name Need help? Newer Comments &rarr; Next Order date: Order number: Payment Payment method: Phone Phone number Phone: Please enter a valid email address Please enter your username or email address. You will receive a link to create a new password via email. Post Address Previous Proceed Read more Recent Orders Register Registration numbers Remove Reply Respond Search Search Example Search here for your product... Search your products here&hellip; Shipping Address Showing search results for: %s Showing the single result Showing all %d results Sort by Sort by average rating Sort by latest Sort by popularity Sort by price: high to low Sort by price: low to high Street Suffix This post is password protected. Enter the password to view any comments. Total: Use description field as HTML content VAT number View your recent orders and contact us if you have any question. Visiting Address What are your details? Will not be published. Yes, please ship to a different address then my details above You must be %1$s logged in %2$s to post a comment Your Cart Your Orders Your comment is awaiting moderation. default-titleSearch exc. VAT or with first and last result%3$d Result %3$d Results with first and last resultShowing the single result Showing %1$d&ndash;%2$d of %3$d results Project-Id-Version: Roodenburg
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-11-12 14:33+0000
PO-Revision-Date: 2022-02-18 13:34+0000
Last-Translator: Maatwerk Online
Language-Team: Nederlands
Language: nl_NL
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Generator: Loco https://localise.biz/
X-Loco-Target-Locale: nl_NL
X-Loco-Version: 2.5.8; wp-5.8.3 Jouw bestelling [ec_order] bij DeVerfVakman is compleet en staat klaar om afgehaald te worden. [empty] Jouw bestelling [ec_order] bij DeVerfVakman is compleet en staat klaar voor verzending. Dat wilden we je gewoon even laten weten. Je hoeft verder geen actie te ondernemen. %d Resultaat %d Resultaten <time pubdate class="small text-muted ml-2">%s geleden</time> %s reactie %s reacties &larr; Oudere reacties Accountgegevens bewerken Nieuwe Toevoegen Zijbalk toevoegen Aantal Aanvullende informatie? Auteur Bankrekening nummer Schrijf de eerste review  Factuuradres Bel voor advies Annuleer reactie Winkelmand Categorieën Kamer van Koophandel nummer Stad Reactie Reactie door de auteur Reactie navigatie Reacties zijn gesloten. Bedrijf (optioneel) Bevestigen Contact Verder winkelen Verander in een dropdown menu op kleinere schermen Huidig wachtwoord Standaard Menu Standaardsortering Levering Wil je op een ander adres laten bezorgen? Documenten Bewerken Bewerk account Adresgegevens aanpassen Bewerk je accountinstellingen, deze worden op je certificaat gebruikt. Bewerk je factuuradres, dit wordt gebruikt voor nieuwe bestellingen. E-mail E-mailadres Voer uw bankrekening nummer hier in Voer uw Kamer van Koophandel nummer hier in Voer uw BTW nummer hier in Vul uw stad hier in Vul hier uw telefoon nummer in Voornaam Volg ons Vanaf Vanaf je dashboard kun je je<a href="%1$s"> recente bestellingen </a> bekijken, je <a href="%2$s"> verzend- en factuuradressen </a> beheren en <a href = "% 3 $ s"> je wachtwoord en accountgegevens aanpassen</a>. Ga naar uw winkelmand Hallo %1$s Verberg titel Huisnummer Ik ben een professional Ik ben een particulier Ik heb de <a href="%s" class="woocommerce-terms-and-conditions-link">algemene voorwaarden</a> gelezen en geaccepteerd  Achternaam Laat een reactie achter Reageer op %s Laat het veld leeg om deze ongewijzigd te laten Uitloggen Uitloggen van dit account Mega Menu - 1 Kolom Mega Menu - 2 Kolommen Mega Menu - 3 Kolommen Mega Menu - 4 Kolommen Mega Menu - 5 Kolommen Mega Menu - 6 Kolommen Mega Menu - 7 Kolommen Menu Soort Minimum bestelbedrag € 44,95 Naam Hulp nodig? Nieuwere Reacties &rarr; Volgende Besteldatum: Bestelnummer: Betaling Betaalmethode: Telefoon: Telefoon nummer Telefoonnummer: Vul een geldig e-mailadres in We hebben een wachtwoord reset-email is naar je e-mailadres gestuurd. Het kan enkele minuten duren voor deze in je inbox verschijnt. Wacht minimaal 10 minuten voor je nog een poging tot resetten waagt.
 Postadres Vorige Verder Lees meer Recente bestellingen Registreren Registratie nummers Verwijderen Reageer Reageer Zoeken Zoekvoorbeeld Zoek hier naar uw product... Zoek hier naar uw product... Afleveradres Zoekresultaat tonen voor: %s Het enige resultaat wordt weergegeven Alle %d resultaten worden weergegeven Sorteren op Sorteer op gemiddelde beoordeling Sorteer op nieuwste Sorteer op populariteit Sorteer op prijs: hoog naar laag Sorteer op prijs: laag naar hoog Straatnaam Toevoeging Dit bericht is beveiligd met een wachtwoord. Voer het wachtwoord in om de reacties weer te geven. Totaal: Gebruik het beschrijving veld als HTML content BTW nummer Bekijk je recente bestellingen en neem contact met ons op als je vragen heeft. Bezoekadres Wat zijn uw gegevens? Zal niet getoond worden. Ja, lever alsjeblieft op een ander adres dan ik hierboven ingevuld heb U moet %1$s inloggen %2$s om een reactie te plaatsen Uw winkelmand Jouw bestellingen Je reactie is in afwachting van goedkeuring. Zoeken excl. BTW of %3$d Resultaat %3$d Resultaten Het enige resultaat wordt weergegeven De eerste %1$d&ndash;%2$d van %3$d resultaten worden weergegeven 