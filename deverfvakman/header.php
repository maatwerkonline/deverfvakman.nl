<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="google-site-verification" content="lolg808pgeNRwj83BhiOg_ihWDS0Mt1UDgAnGv8y9P4" />
        <meta name="facebook-domain-verification" content="f58upmg4nvx7bs9fv6t72y38ogu3fo" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <?php wp_head(); ?>

    </head>

    <?php
    /*
    if (is_user_logged_in()) {
        $class = 'logged-in';
    } else {
        $class = 'logged-out';
    };

    $current_user = wp_get_current_user();
    if (get_user_meta($current_user->ID, 'billing_kvk', true)) {
        $class = 'business-user';
    };

    if (is_product() && get_post_meta(get_the_ID(), '_wc_pb_bundle_sell_ids')) {
        $class = 'product-has-bundles';
    };
    */
    ?>

    <body id="frm-body" <?php body_class($class); ?>>
        <?php do_action('after_body_open_tag'); ?>
        
        <?php
        if (function_exists('gtm4wp_the_gtm_tag')) {
            gtm4wp_the_gtm_tag();
        }
        ?>
        <script>
            // Decide what homepage the user will be shown. Consumer of Business
            var homepageType = localStorage.getItem("homepageType");
            
            // If on the consumer homepage, but the business homepage has previously been selected
            if(jQuery('body.home').length && homepageType == "business"){
                window.location.replace("/home-zakelijk/");

            // If on the business homepage, but the consumer homepage has previously been selected
            }else if(window.location.href.indexOf("/home-zakelijk/") > -1 && homepageType == "consumer") {
                window.location.replace(window.location.origin);

            }else if(homepageType === null){
                var homepage_prompt_overlay = '<div class="homepage-prompt-overlay">\
                                                    <div class="homepage-prompt-close">&cross;</div>\
                                                    <div class="homepage-prompt">\
                                                        <div class="homepage-prompt-choice">\
                                                            <h2 class="prompt-title"><strong><?php _e('I am a consumer','maatwerkonline'); ?></strong></h2>\
                                                            <a class="prompt-button btn btn-primary choose-reg-homepage close-homepage-prompt" href="#"><?php _e('Confirm','maatwerkonline'); ?></a>\
                                                        </div>\
                                                        <div class="homepage-prompt-choice">\
                                                            <h2 class="prompt-title"><strong><?php _e('I am a business partner','maatwerkonline'); ?></strong></h2>\
                                                            <a class="prompt-button btn btn-primary choose-alt-homepage" href="<?php echo get_site_url(); ?>/home-zakelijk/"><?php _e('Confirm','maatwerkonline'); ?></a>\
                                                        </div>\
                                                    </div>\
                                                </div>';
                document.write(homepage_prompt_overlay);
            }
            
        </script>

        <?php if (!is_page_template('page-leadpage.php')): ?>
            <div id="wrapper" class="d-flex">

                <?php wp_nav_menu(array('menu' => 'mobile', 'theme_location' => 'mobile', 'container_id' => 'sidebar-wrapper', 'container_class' => 'bg-dark border-right', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu())); ?>

                <div id="page-content-wrapper">
                    <header id="header">
                        <?php if (!is_checkout() && !is_cart()) : ?>
                            <div class="border-bottom">
                                <div class="container">
                                    <div class="top-bar row mr-2 ml-2 mr-md-0 mr-lg-0 ml-md-0 ml-lg-0">
                                        <div class="menu-top w-100">
                                            <div class="float-left">
                                                <p class="mb-0 call-us">
                                                    <a href="tel:<?php echo get_theme_mod('phone_number'); ?>"><i class="dvv dvv-telephone2 mr-2"></i><u style="text-decoration:none;"><?php _e('Call for advice', 'maatwerkonline'); ?> <?php echo get_theme_mod('phone_number'); ?></u></a>
                                                </p>
                                            </div>
                                            <?php wp_nav_menu(array('menu' => 'topbar', 'theme_location' => 'topbar', 'container_id' => 'default-topbar', 'menu_class' => 'd-flex float-right pl-0', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu())); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="menu-holder top-menu-holder">
                            <nav class="navbar navbar-light header-top" role="navigation" id="slide-nav">
                                <div class="container">
                                    <?php
                                    $logo = get_theme_mod('logo');
                                    $events_logo = get_theme_mod('events_logo');
                                    $events_page = get_theme_mod('events_page');
                                    $phone_number = get_theme_mod('phone_number');
                                    ?>
                                    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                        <img src="<?php echo esc_attr($logo); ?>" data-no-lazy="1" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="mb-0">
                                    </a>
                                    <?php if (!is_checkout() && !is_cart()) : ?>
                                        <?php get_product_search_form(); ?>
                                        <img class="image-home" src="<?php get_site_url(); ?>/wp-content/uploads/2020/07/Thuiswinkel_Waarborg_Kleur_Horizontaal.png" width="115" height="35">
                                        <div class="kiyoh-container">
                                            <a href="https://www.kiyoh.com/reviews/1066237/de_verfvakman_nl?from=widget&lang=nl" target="_blank">
                                                <img src="/wp-content/uploads/2021/11/kiyoh-rating-11-12-21.png" alt="" class="h-100 py-1">
                                            </a>
                                            <?php
                                            /* Disabled for speed management, uncomment to see current Kiyoh stats
                                                <iframe style="height: 100%; width: 250px; border: none;" allowtransparency="true" src="https://www.kiyoh.com/retrieve-widget.html? color=white&allowTransparency=false&button=false&lang=nl&tenantId=98&locationId=1066237"></iframe>
                                            */
                                            ?>
                                        </div>
                                        
                                        <img class="image-ratings" src="<?php get_site_url(); ?>/wp-content/uploads/2020/07/Kiyoh.png" width="120" height="35">
                                        <a href="<?php get_site_url(); ?>/winkelwagen/" title="Ga naar je winkelwagen" class="nav-link cart-contents">
                                            <i class="dvv dvv-cart"></i>
                                        </a>
                                        <button type="button" id="searchVisible" class="d-lg-none"><i class="dvv dvv-magnifier"></i></button>
                                    <?php else : ?>
                                        <div class="need-help my-2">
                                            <h4 class="h6 text-primary mb-0"><?php _e('Need help?', 'maatwerkonline'); ?></h4>
                                            <h3><a class="text-primary mb-0" href="tel:<?php echo get_theme_mod('phone_number'); ?>"><i class="dvv dvv-phone mr-2"></i> <?php echo get_theme_mod('phone_number'); ?></a></h3>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </nav>
                        </div>
                        <?php if (!is_checkout() && !is_cart()) : ?>
                            <div class="menu-holder">
                                <nav class="navbar navbar-default navbar-light main-menu" role="navigation" id="slide-nav">
                                    <div class="container">
                                        <div id="menu-toggle" class="d-block d-lg-none <?php if (is_page('offline')) { ?> inactive <?php }; ?>">
                                            <div class="hamburger">
                                                <div class="hamburger-box">
                                                    <?php if (is_page('offline')) { ?>
                                                        <i class="dvv dvv-undo2"></i>
                                                    <?php } else { ?>
                                                        <div class="hamburger-inner"></div>
                                                    <?php }; ?>
                                                </div>
                                            </div>
                                            <span class="mobile-menu-label"><?php
                                                if (is_page('offline')) {
                                                    _e('Reload', 'maatwerkonline');
                                                } else {
                                                    _e('Menu', 'maatwerkonline');
                                                };
                                                ?></span>
                                        </div>
                                        <?php wp_nav_menu(array('menu' => 'default', 'theme_location' => 'default', 'container_id' => 'default-menu', 'container_class' => 'd-none d-lg-block ', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu())); ?>
                                        
                                        <span class="header-notice"><?php _e('Minimum order amount € 44,95', 'maatwerkonline'); ?></span>
                                        
                                        <?php
                                        /* Price tax removal toggle button - wip */
                                        /* if(mo_price_tax_toggle()): ?>
                                            <div class="price-tax-toggle">
                                                <div class="content-container">
                                                    <?php _e('Show prices without tax','maatwerkonline'); ?>
                                                    <label class="switch">
                                                        <input type="checkbox" id="toggle-price-tax">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                                <div id="price-tax-toggle-close"></div>
                                            </div>
                                        <?php endif; */
                                        ?>
                                    </div>
                                </nav>

                                <nav class="navbar navbar-default navbar-light" role="subnavigation" id="sub-slide-nav show-sub-mobile">
                                    <div class="container">
                                        <?php $usps = get_theme_mod('uspbox', array()); ?>
                                        <div id="usps" class="d-none d-lg-block w-100">
                                            <div class="usps d-flex justify-content-between">
                                                <?php foreach ($usps as $usp) : ?>


                                                <a href="<?php echo esc_attr($usp['link']);?>">
                                                    <div class="usp">
                                                        <span class="icon-text px-2">
                                                            <?php if (strpos($usp['icon'], 'lnr') !== false || strpos($usp['icon'], 'fz') !== false || strpos($usp['icon'], 'dvv') !== false) { ?>
                                                                <i class="mr-2 <?php echo esc_attr($usp['icon']); ?>"></i>
                                                            <?php } else { ?>
                                                                <i><?php echo esc_attr($usp['icon']); ?></i>
                                                            <?php }; ?>
                                                            <?php echo esc_attr($usp['text']); ?>
                                                        </span>
                                                    </div>
                                                </a>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <?php wp_nav_menu(array('menu' => 'buttons', 'theme_location' => 'buttons', 'container_id' => 'buttons-menu', 'container_class' => 'ml-auto', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => false, 'walker' => new bootstrap_default_walker_nav_menu())); ?>
                                    </div>
                                </nav>
                            </div>
                        <?php endif; ?>
                    </header>

                <?php endif; ?>
